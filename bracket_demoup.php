<?
	require_once "bracket_ccode.php";
	require_once "bracket_connect.php";
    require_once "bracket_cfg.php";


	$state = "get_info";
	$here = $_SERVER['PHP_SELF'];

	extract ($_GET);
	extract ($_POST);
	extract ($_FILES);
	
	if (!isset ($RoundID) && !isset ($DemoUploaded))
	{
		echo "Please specify the RoundID for the upload.<br>";
		exit;
	}

	$allow_characters	= array (']', '[', '(', ')', '-', '_', '|', '&', '{', '}', '!', ' ' ,'.');
	$allow_extensions	= array ("zip");
	
	// for testing at home (specify your own path in bracket_connect.php)
	#if ($_SERVER['SERVER_NAME'] == "putty-server")
		#$destinationPath	= "c:/Demos/";

	function addToLog ($filePath, $info)
	{
		global $demoLogName;
		$date = date ("m-d-Y h:i:s A");
		$dbid = mysql_insert_id ();
		$browser = $_SERVER['HTTP_USER_AGENT'];
		$ipaddy = $_SERVER['REMOTE_ADDR'];
		$string= "time: $date\n"
				."file: $filePath\n"
				."browser: $browser\n"
				."database id: $dbid\n"
				."uploader's ip: $ipaddy\n"
				."further info:\n$info\n";
		$fp = fopen ($demoLogName, "a");
		fwrite ($fp, $string);
		fwrite ($fp, "-----------------------------\n");
		fclose ($fp);
	}

	function checkString ($string, $allow_string)
	{
		for ($i = 0; $i < strlen ($string); $i ++)
		{
			$char = $string[$i];
			$cord = ord ($string[$i]);
			
			if ($cord > 47 && $cord < 58)
				continue;	// 0-9
			if ($cord > 64 && $cord < 91)
				continue;	// A-Z
			if ($cord > 96 && $cord < 123)
				continue;	// a-z
			if (in_array ($char, $allow_string))
				continue;	// special allowed characters
			return 0;	// if ur here ur bad!
		}
		return 1;
	}

	if ($state == "handle_upload")
	{	// check the upload data/file
		$bFile = ($iFile["size"] < 10) ? false : true;
		$allGood = true;	// innocent till proven guilty
		
		if (!$bFile)		{ echo "A zip file is required.<br>"; $allGood = false; }
		
		if ($allGood)
		{
			$srcPath	= $iFile["tmp_name"];
			$fileName	= $iFile["name"];
			$extension	= end(explode(".", $fileName));
			$bAllowFile	= (in_array (strtolower($extension), $allow_extensions)) ? true : false;
			
			if (!$bAllowFile) {
				echo "The file type you uploaded ($extension) is not allowed.<br>"
					."File must be: " . implode (", ", $allow_extensions) . ".<br>";
				$allGood = false;
			}
		}

		$bIsUploaded = is_uploaded_file($iFile["tmp_name"]);
		if (!$bIsUploaded)
		{
			echo "the file was not properly uploaded<br>this is usually a problem on the client side.<br>check your browser/net configuration and try again.<br>";
			addToLog ($srcPath, "bIsUploaded = false\ntmp_name: $iFile[tmp_name]\nsize: $iFile[size]");
		}
	
		if ($allGood)
		{	// requirements met, lets upload this biznatch
			$query = "select p1.Nick as p1Nick, p2.Nick as p2Nick, WinnerScore as p1Score, LoserScore as p2Score, MapName, r.BracketID as MatchID "
					."from pbs_rounds r left join pbs_players p1 on (p1.ID = r.WinnerPID) "
									  ."left join pbs_players p2 on (p2.ID = r.LoserPID) "
									  ."left join pbs_mappool m on (m.ID = r.MapID) "
					."where r.ID = $RoundID";
			$rRoundInfo	= db_query ($query);
			if (mysql_error()) {
				echo "error while reading round info for filename<br>\n";
				echo mysql_error();
				exit;
			}
			$roundInfo	= mysql_fetch_assoc ($rRoundInfo);

			$destFilename = getTourneyName()."_".$roundInfo["p1Nick"]."_vs_".$roundInfo["p2Nick"]."_on_".$roundInfo["MapName"].".$extension";
			//$destFilename = "CHTV_$iTeam1-".$sCtr1."_".$iTeam2."-".$sCtr2."_".$iPov."_".$iMap."_".$sGame."_".$sType.".$extension";
			//echo "destFilename: $destFilename<br>";
			// strip off bad things
			$destFilename = str_replace(" ","_",$destFilename);
			$destFilename = str_replace("'","",$destFilename);
			$destFilename = str_replace("\"","",$destFilename);
			//echo "destFilename: $destFilename<br>";
			$destDir = $destinationPath . "/";
			
			if (!checkString ($destFilename, $allow_characters))
			{
				echo "Unallowed characters were used ($destFilename), check your inputs and try again.<br>";
				addToLog($destFilename, "checkString($destFilename, ..) failed\nallow_chars = [".(implode ($allow_characters, ", "))."]\n");
			} else
			if (!($destFilename = checkFilename ($destDir, $destFilename)))
			{
				echo "A destination path could not be stablished.<br>"
					."Usually this means too many similar filenames exist.<br>"
					."Contact putty about this.<br>";
				$fsize = $iFile["size"];
				$browser = $_SERVER['HTTP_USER_AGENT'];
				addToLog($destFilename,"checkFilename() returned 0\ndestDir: $destDir\nfilesize: $fsize\n");
			} else
			{
				$destPath = $destDir . $destFilename;
				//echo "destPath: $destPath<br>";
				$bMoved = move_uploaded_file ($iFile["tmp_name"], $destPath);
				if ($bMoved)
				{
					db_query ("update pbs_rounds set DemoURL = '$destFilename' where ID = $RoundID");
					if (mysql_error())
					{
						echo "error while inserting demo into database:<br>".mysql_error()."<br>";
						$bDeleted = unlink ($destPath);
						addToLog($destPath, "error inserting into database\nfile deleted: $bDeleted\nsql error:".mysql_error());
					} else
					{
						$date = date ("m-d-Y h:i:s A");
						$userip = $_SERVER['REMOTE_ADDR'];
						//echo "newID = $newID<br>";
						$browser = $_SERVER['HTTP_USER_AGENT'];
						$fsize = number_format (filesize ($destPath) / 1024);
						$string = "time: $date\tfilesize: $fsize KB\n"
								 ."file: $destFilename\n"
								 ."browser: $browser\n"
								 ."pbs_rounds id: $RoundID\n"
								 ."uploader's ip: $userip\n";
						$fp = fopen ($demoLogName, "a");
						fwrite ($fp, $string);
						fwrite ($fp, "-----------------------------\n");
						fclose ($fp);
						header ("Location: $here?DemoUploaded=".$roundInfo["MatchID"]);
						//echo "<meta http-equiv=\"refresh\" content=\"0; url=$here?DemoUploaded=$RoundID\">";
					}
				} else
				{
					echo "Problem while moving uploaded file (folder doesnt exist?).<br>";
					addToLog($destPath, "bMoved = false\ntmp_name: $iFile[tmp_name]\nfilesize: $iFile[size]");
				}
			}
		}
	} else
	{
?>
<html>
<head>
  <title>Demo Upload</title>
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>

<body bgcolor="#232E41">
<br>
<?
		if (isset ($DemoUploaded))
		{
?>
<br><br><br><br><br>

    <table border="0" width="200" cellpadding="2" cellspacing="0" align="center">
	<tr>
  	  <td align="center" class="uploadtext">
  	    <br>
  	    Demo Uploaded.<br>
	    You can see it <a href="bracket_view.php?show=match&matchid=<? echo $DemoUploaded; ?>">here</a>.
	    <br><br>
      </td>
	</tr>
    </table>
<?
		} else
		{	
			$query = "select p1.Nick as p1Nick, p2.Nick as p2Nick, WinnerScore as p1Score, LoserScore as p2Score, MapName, r.BracketID as MatchID "
					."from pbs_rounds r left join pbs_players p1 on (p1.ID = r.WinnerPID) "
									  ."left join pbs_players p2 on (p2.ID = r.LoserPID) "
									  ."left join pbs_mappool m on (m.ID = r.MapID) "
					."where r.ID = $RoundID";
			$rDemoInfo = db_query ($query);
			if (mysql_error()) {
				echo "error while getting round information, invalid roundid?<br>\n";
				exit;
			}
			$demoInfo = mysql_fetch_assoc ($rDemoInfo);
?>
<form method="POST" enctype="multipart/form-data">
<input type="hidden" name="state" value="handle_upload">
<table border="0" cellpadding="1" cellspacing="1" align="center" width="320">
<tr>
  <td colspan="2" align="center" class="td_sched">Demo Uploader</td>
</tr>
<tr>
  <td class="td_unsched" width="30%">Player1:</td><td class="td_unsched"><? echo $demoInfo["p1Nick"]; ?></td>
</tr>
<tr>
  <td class="td_unsched">Player2:</td><td class="td_unsched"><? echo $demoInfo["p2Nick"]; ?></td>
</tr>
<tr>
  <td class="td_unsched">Map:</td><td class="td_unsched"><? echo $demoInfo["MapName"]; ?></td>
</tr>
<tr>
  <td colspan="2" align="center" class="td_normal">
    the demo: (remember to zip it up!)<br>
    <input type="file" name="iFile" size="30" class="text">
  </td>
</tr>
<tr>
  <td colspan="2" align="center" class="td_unsched"><input type="submit" class="button" value="Upload"></td>
</tr>
</table>
</form>
<?
		} // else from if (isset (demouploaded))
?>
</body>
</html>
<?
	} // else from if (state == handle_upload)
?>