<?
$gameTermS = "Player";
$gameTermP = "Players";

function getGameMode()
{
    global $gameMode, $gameTermS, $gameTermP;
    if (file_exists("bracket_cfg.xml"))
    {
        $xml = simplexml_load_file("bracket_cfg.xml");
        $gameMode = $xml->game;
    } else {
        $gameMode = "1v1";
    }
    $gameTermS = ($$gameMode == "1v1" || $$gameMode == "HM" || $$gameMode == "FFA") ? "Player" : "Team";	// sad but true
    $gameTermP = ($$gameMode == "1v1" || $$gameMode == "HM" || $$gameMode == "FFA") ? "Players" : "Teams";	// even sadder :<
    return $gameMode;
}

function getTourneyName()
{
    global $tourneyName,$gameMode, $gameTermS, $gameTermP;
    if (file_exists("bracket_cfg.xml"))
    {
        $xml = simplexml_load_file("bracket_cfg.xml");
        $tourneyName = $xml->tourney;
        $gameMode = $xml->game;
    } else {
        $tourneyName = "CPMA";
    }
    $gameTermS = ($$gameMode == "1v1" || $$gameMode == "HM" || $$gameMode == "FFA") ? "Player" : "Team";	// sad but true
    $gameTermP = ($$gameMode == "1v1" || $$gameMode == "HM" || $$gameMode == "FFA") ? "Players" : "Teams";	// even sadder :<
    return $tourneyName;
}

$gm = getGameMode();
$gameTermS = ($gm == "1v1" || $gm == "HM" || $gm == "FFA") ? "Player" : "Team";	// sad but true
$gameTermP = ($gm == "1v1" || $gm == "HM" || $gm == "FFA") ? "Players" : "Teams";	// even sadder :<
?>