<?
	require_once ("bracket_connect.php");
	require_once ("bracket_ccode.php");
	require_once ("bracket_login.php");

	extract ($_GET);
	extract ($_POST);
	extract ($_FILES);
	
	if (isset ($iMatchResult)) {
		//echo "iDropInID: $iDropInID<br>";
		//echo "iSpotID: $iSpotID<br>";
		//echo "iMatchResult: $iMatchResult<br>";
		list ($WinnerPID, $LoserPID) = split ("/", $iMatchResult);
		//echo "WinnerPID = $WinnerPID | LoserPID = $LoserPID<br>";
		
		$sqlresult = db_query ("select Value from pbs_config where Label = 'MPG'");
		$numMPG = mysql_result ($sqlresult, 0);
		
		db_query ("delete from pbs_rounds where BracketID = $iSpotID");
		
		db_query ("update pbs_bracket set PlayerID = $WinnerPID where ID = $iSpotID");
		if (mysql_error()) {
			showHint("Error Updating Winner Bracket", mysql_error());
			exit;
		}
		
		for ($i = 1; $i <= $numMPG; $i ++) {
			db_query ("insert into pbs_rounds (BracketID, WinnerPID, LoserPID, SequenceNum) "
						."values ($iSpotID, $WinnerPID, $LoserPID, $i)");
			if (mysql_error()) {
				showHint("Error Updating Rounds Info", mysql_error());
				exit;			
			}
		}
		
		if ($iDropInID) {
			db_query ("update pbs_bracket set PlayerID = $LoserPID where ID = $iDropInID");
			if (mysql_error()) {
				showHint("Error Updating Loser Bracket", mysql_error());
				exit;
			}
		}
		header ("Location: bracket_referee.php");
	}
	
?>
<html>
<head>
<title>Bracket: Referee</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<script src="bracket_cjscript.js" language="JavaScript" type="text/javascript"></script>
<?	
	$nColor1	= "#02132F";	// blank
	$nColor2	= "#838862";	// players
	$nColor3	= "#9D7785";	// drop in players
	$nColor4	= "#C4A879";	// connector

	if (isset ($editTime)) {
		extract (editTimeData( array_shift( split( ";", $editTime ) ) ));
?>
<body bgcolor="<? echo $nColor1; ?>" onLoad="javascript: changeMonth (<? echo "$nMonth, $nYear"; ?>); setMinHour (<? echo "$nMin, $nHour"; ?>); dateSelect (<? echo "$nMonth, $nDay, $nYear"; ?>);">
<?
	} else {
?>
<body bgcolor="<? echo $nColor1; ?>">
<?
	}
	//timeCode (1, "entire page");
	
	if ($_SESSION["refAuth"] == "logged in")
	{
		$refName = $_SESSION["refName"];
		$refID = $_SESSION["refID"];
	
		$query = "select r.RefName, p1.Nick as p1Nick, p2.Nick as p2Nick, b.Opponent1, b.Opponent2, b.PlayerID, "
				."if(b.ID is null, 0, b.ID) as BID, if(b.Timestamp is null, 0, date_format(b.Timestamp, '%m/%d/%Y @ %H:%i:%s')) as Time "
				."from pbs_bracket b left join pbs_referees r on (r.ID = b.RefID) "
				."left join pbs_players p1 on p1.ID = b.Opponent1 "
				."left join pbs_players p2 on p2.ID = b.Opponent2 where r.ID = $refID";

		$rAssignments = db_query ($query);
?>
<table align="center" class="data_list">
<tr>
  <td colspan="4" class="headerText" align="center">Referee Assignments for <? echo $refName; ?> [<a href="?action=Logout">logout</a>]</td>
</tr>
<tr>
  <td class="header" width="180px">Match</td>
  <td class="header">Scheduled</td>
  <td class="header" width="50px">Played</td>
  <td class="header" width="">Action</td>
</tr>
<?
		if (!mysql_num_rows ($rAssignments))
		{
?>
<tr>
  <td colspan="4" class="headerText" align="center">You have no assignments at the moment ;]</td>
</tr>
<?
		} else
		while ($assignment = mysql_fetch_assoc ($rAssignments))
		{
			//echo "<pre>";
			//print_r ($assignment);
			$sMatch = "<a href=\"?editTime=$assignment[BID];$assignment[Opponent1];$assignment[Opponent2]\">".$assignment["p1Nick"] . " vs " . $assignment["p2Nick"] . "</a>";
			$sSched = ($assignment["Time"] > 0) ? $assignment["Time"] : "No";
			$sPlayed = ($assignment["PlayerID"]) ? "Yes" : "No";
?>
<tr>
  <td class="entry"><? echo $sMatch; ?></td>
  <td class="entry"><? echo $sSched; ?></td>
  <td class="entry"><? echo $sPlayed; ?></td>
  <td class="entry">
    [<a href="?editTime=<? echo "$assignment[BID];$assignment[Opponent1];$assignment[Opponent2]"; ?>">Schedule</a>]
	[<a href="?editResult=<? echo $assignment["BID"]; ?>">Result</a>]
	<? if ($assignment["PlayerID"]) echo "[<a href=\"?editScores=$assignment[BID]\">Score</a>]"; else echo "--------"; ?>
	[<a href="bracket_view.php?show=match&matchid=<? echo $assignment["BID"]; ?>">View</a>]
  </td>
</tr>
<?
		}
?>
</table>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
  <td>
<?
		if (isset ($editTime)) {
			list ($nSpotID, $nPlayer1, $nPlayer2) = split (";", $editTime);
			editTime ($nSpotID, $nPlayer1, $nPlayer2);
			showHint ("How To Schedule", "1. Select a day and a month from the calendar.<br>"
										."2. Then an hour and minute from the drop down menus.<br>"
										."3. The timestamp input field allows you to override and use your own time. "
										."However please maintain the format used within the timestamp");
		} else
		if (isset ($editScores)) {
			editScores ($editScores);
		} else
		if (isset ($editResult))
		{
			// \o/ $$$$ \o/ \\
			$query = "select b.ID, b.RefID, b.DropInID, b.Opponent1 as PlayerID1, b.Opponent2 as PlayerID2, p1.Nick as p1Nick, p2.Nick as p2Nick "
					."from pbs_bracket b left join pbs_players p1 on (b.Opponent1 = p1.ID) ".
										"left join pbs_players p2 on (b.Opponent2 = p2.ID) "
					."where b.ID = $editResult";
			$sqlresult = db_query ($query);
			$match = mysql_fetch_assoc ($sqlresult);
			
			//echo "<pre>";
			//print_r ($match);
			if ($match["RefID"] != $refID) {
				echo "<div align=\"center\">You are not the referee for this match, fuck off.</div><br>\n";
			} else
			{
?>
	<form action="bracket_referee.php" method="post">
	<input type="hidden" name="iSpotID" value="<? echo $match["ID"]; ?>">
	<input type="hidden" name="iDropInID" value="<? echo $match["DropInID"]; ?>">
	<table border="0" cellpadding="1" cellspacing="1" align="center">
	<tr>
	  <td align="center"><? echo $match["p1Nick"]; ?></td>
	  <td align="center">vs</td>
	  <td align="center"><? echo $match["p2Nick"]; ?></td>
	</tr>
	<tr>
	  <td colspan="3">
	  <select name="iMatchResult" size="1">
		<option value="0">-- Choose Winner --</option>
		<option value="<? echo $match["PlayerID1"] . "/" . $match["PlayerID2"]; ?>"><? echo $match["p1Nick"]; ?></option>
		<option value="<? echo $match["PlayerID2"] . "/" . $match["PlayerID1"]; ?>"><? echo $match["p2Nick"]; ?></option>
	  </select>
	  </td>
	</tr>
	<tr>
	  <td align="center" colspan="3"><input class="button" type="submit" value="Submit"></td>
	</tr>
	</table>
	</form>
<?
			}
		} // if isset editResult
		else {
			//echo "<div class=\"headerText2\" align=\"center\">no action</div>";
			showHint ("HowTo", "Step 1: [Schedule] the match.<br>Step 2: Set the [Result] of the match (who won).<br>Step 3: Set the [Scores] of the match.<br>* Please skip step 3 if it was a forfeit-win. We dont need fake scores.");
		}
?>
  </td>
</tr>
</table>
<?
	} // if session auth
	//timeCode (1);
?>
</body>
</html>
