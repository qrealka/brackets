<?
	$CTables = array();
	$CTables[] = array ("Name" => "pbs_signup", "Code" =>
		"CREATE TABLE pbs_signup (
			ID		int auto_increment NOT NULL,
			Nick		varchar (64) NOT NULL unique,
			Location	varchar (64),
			Email		varchar (64),
			IP		varchar (16),
			Date		timestamp,
			Roster		text,
			SeededNum	int NOT NULL DEFAULT 0,
			PRIMARY KEY	(ID)
		);"
	);
	$CTables[] = array ("Name" => "pbs_bracket", "Code" =>
		"CREATE TABLE pbs_bracket (
			ID		int AUTO_INCREMENT NOT NULL,
			PlayerID	int NOT NULL DEFAULT 0,
			RefID		int NOT NULL DEFAULT 0,
			Opponent1	int NOT NULL DEFAULT 0,
			Opponent2	int NOT NULL DEFAULT 0,
			DropInID	int NOT NULL DEFAULT 0,
			Timestamp	timestamp,
			PRIMARY KEY	(ID)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_config", "Code" =>
		"CREATE TABLE pbs_config (
			ID		int AUTO_INCREMENT NOT NULL,
			Label		varchar (64) NOT NULL UNIQUE,
			Value		varchar (64) NULL,
			PRIMARY	KEY	(ID, Label)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_players", "Code" =>
		"CREATE TABLE pbs_players (
			ID		int AUTO_INCREMENT NOT NULL,
			Nick		varchar (64),
			Location	varchar (64),
			PRIMARY KEY	(ID)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_mappool", "Code" =>
		"CREATE TABLE pbs_mappool (
			ID		int AUTO_INCREMENT NOT NULL,
			MapName		varchar (64),
			MapImage	varchar (64),
			URL		varchar (128),
			Description	tinytext,
			PRIMARY KEY	(ID)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_rounds", "Code" =>
		"CREATE TABLE pbs_rounds (
			ID		int AUTO_INCREMENT NOT NULL,
			BracketID	int NOT NULL DEFAULT 0,
			DropInID	int NOT NULL DEFAULT 0,
			MapID		int NOT NULL DEFAULT 0,
			LoserPID	int NOT NULL DEFAULT 0,
			WinnerPID	int NOT NULL DEFAULT 0,
			LoserScore	int NOT NULL DEFAULT 0,
			WinnerScore	int NOT NULL DEFAULT 0,
			SequenceNum	int NOT NULL DEFAULT 0,
			Screenshot	tinytext,
			DemoURL		tinytext,
			DemoID		int NOT NULL DEFAULT 0,
			PRIMARY KEY	(ID)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_referees", "Code" =>
		"CREATE TABLE pbs_referees (
			ID		int AUTO_INCREMENT NOT NULL,
			RefName		varchar (64) NOT NULL,
			Login		varchar (32) NOT NULL UNIQUE,
			Passwd		varchar (32) NOT NULL,
			bIsAdmin	enum ('N', 'Y') NOT NULL DEFAULT 'N',
			PRIMARY KEY	(ID, Login)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_blockedips", "Code" =>
		"CREATE TABLE pbs_blockedips (
			ID		int AUTO_INCREMENT NOT NULL,
			IP		varchar (16) NOT NULL,
			FailedAuths	int NOT NULL DEFAULT 0,
			Time		timestamp,
			PRIMARY KEY	(ID, IP)	
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_teamdata", "Code" =>
		"CREATE TABLE pbs_teamdata (
			ID		int AUTO_INCREMENT NOT NULL,
			TeamID		int NOT NULL DEFAULT 0,
			Captain		enum ('N', 'Y') NOT NULL DEFAULT 'N',
			Name		varchar (64),
			Location	varchar (64),
			PRIMARY KEY	(ID, TeamID)
		);"
	);

	$CTables[] = array ("Name" => "pbs_comments", "Code" =>
		"CREATE TABLE pbs_comments (
			ID		int AUTO_INCREMENT NOT NULL,
			BracketID	int NOT NULL DEFAULT 0,
			Author		varchar (32) NOT NULL,
			Body		text NOT NULL,
			IP		varchar (16),
			Timestamp	timestamp,
			PRIMARY KEY	(ID)
		);"
	);
	
	$CTables[] = array ("Name" => "pbs_textdata", "Code" =>
		"CREATE TABLE pbs_textdata (
			ID		int AUTO_INCREMENT NOT NULL,
			TextType	enum( 'News', 'Rules' ),
			Title		varchar (32) NOT NULL,
			Author		varchar (32) NOT NULL,
			Body		text NOT NULL,
			Created		datetime,
			LastModified	timestamp,
			PRIMARY KEY	(ID)
		);"
	);
?>