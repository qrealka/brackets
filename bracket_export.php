<?
	require_once "bracket_connect.php";
    require_once "bracket_cfg.php";

	echo "<pre>\n";
	echo "-------------------\npbs_bracket data\n-------------------\n";
	$sqlresult = db_query ("select ID, PlayerID, RefID, Opponent1, Opponent2, DropInID, Timestamp from pbs_bracket");
	while ($data = mysql_fetch_assoc ($sqlresult))
	{
		extract ($data);
		$line =  "insert into pbs_bracket (ID, PlayerID, RefID, Opponent1, Opponent2, DropInID, Timestamp) values "
				."($ID, $PlayerID, $RefID, $Opponent1, $Opponent2, $DropInID, $Timestamp);\n";
		echo $line;
	}
	echo "-------------------\npbs_players data\n-------------------\n";
	$sqlresult = db_query ("select ID, Nick, Location from pbs_players");
	while ($data = mysql_fetch_assoc ($sqlresult))
	{
		extract ($data);
		$Location = addslashes(htmlentities($Location));
		$Nick = addslashes(htmlentities($Nick));
		$line =  "insert into pbs_players (ID, Nick, Location) values ($ID, '$Nick', '$Location');\n";
		echo $line;
	}
	echo "-------------------\npbs_rounds data\n-------------------\n";
	$sqlresult = db_query ("select ID, BracketID, DropInID, MapID, LoserPID, WinnerPID, LoserScore, WinnerScore, SequenceNum from pbs_rounds");
	while ($data = mysql_fetch_assoc ($sqlresult))
	{
		extract ($data);
		$line =  "insert into pbs_rounds (ID, BracketID, DropInID, MapID, LoserPID, WinnerPID, LoserScore, WinnerScore, SequenceNum) values "
				."($ID, $BracketID, $DropInID, $MapID, $LoserPID, $WinnerPID, $LoserScore, $WinnerScore, $SequenceNum);\n";
		echo $line;
	}
	echo "-------------------\npbs_mappool data\n-------------------\n";
	$sqlresult = db_query ("select ID, TeamID, Captain, Name, Location from pbs_teamdata");
	while ($data = mysql_fetch_assoc ($sqlresult))
	{
		extract ($data);
		$Location = addslashes(htmlentities($Location));
		$Captain = addslashes(htmlentities($Captain));
		$Name = addslashes(htmlentities($Name));
		$line =  "insert into pbs_teamdata (ID, TeamID, Captain, Name, Location) values "
				."($ID, $TeamID, '$Captain', '$Name', '$Location');\n";
		echo $line;
	}
	echo "</pre>\n";
?>