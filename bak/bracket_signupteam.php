<?
	require_once ("bracket_connect.php");
	require_once ("bracket_ccode.php");
	
	$bShowOldList = false;

	// the configuration
	$res = db_query("select Value from pbs_config where Label = 'MaxSignups'");
	if (!mysql_num_rows($res))
		$wanted = 0;
	else
		$wanted = mysql_result($res, 0);

	extract ($_GET);
	extract ($_POST);
	
	
	$res = db_query ("select count(*) from pbs_signup");
	$signedup = mysql_result ($res, 0);

	if (count ($_POST))
	if (isset ($Submit) && ($signedup < $wanted))
	{
		if (strlen ($nick) < 3)
			echo "<div align=\"center\" class=\"td_sched\">your team name is too short..<br>has to be at least 3 letters long!</div>\n";
		else if (strlen ($email) < 3)
			echo "<div align=\"center\" class=\"td_sched\">please enter a valid email</div>\n";
		else if (strlen ($location) < 2)
			echo "<div align=\"center\" class=\"td_sched\">please enter your clan's irc channel/network or NA if not applicable</div>\n";
		else
		{
			$nick		= safeString($nick);
			$location	= safeString($location);
			$email		= safeString($email);
			$ip			= $_SERVER['REMOTE_ADDR'];
			$roster		= safeString ( safeNL2Br ( htmlentities ( trim($roster) ) ) );
						
			db_query ("insert into pbs_signup (Nick, Location, Email, IP, Roster) values ('$nick', '$location', '$email', '$ip', '$roster')");
			if (mysql_error()) echo mysql_error();			
			//else echo "all ok<br>";
			else header ("Location: bracket_signupteam.php");
			//else echo "<meta http-equiv=\"Refresh\" content=\"0; url=$_SERVER[PHP_SELF]\">";
			exit;
		}
	}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
<title>Tournament Signup</title>
</head>
<body bgcolor="#354662" text="#FFFFFF" link="#E3CC7E" alink="#E3CC7E" vlink="#E3CC7E">
<br>
<br>
<?
	
	if (!isset ($list))
		$list = 0;
	if (!isset ($Do))
		$Do = 0;

	if (!$wanted)
	{	// signup list disabled
?>
<table width="450" border="0" align="center" cellpadding="3" cellspacing="0">
<tr>
  <td>
    <table width="250" border="0" cellspacing="0" cellpadding="2" align="center">
    <tr>
      <td bgcolor="#2C446B" class="td_sched" align="center">
        <? echo $tourneyName; ?> Sign Up List Is Closed
      </td>
    </tr>
    </table>
  </td>
</tr>
</table>
<?
	} else		
	if ($Do == "View List")
	{
?>
<table border="0" align="center" cellpadding="3" cellspacing="0">
<tr>
  <td>
    <table width="250" border="0" cellspacing="0" cellpadding="2" align="center">
    <tr>
      <td class="" align="center">
        <? echo $tourneyName; ?> Sign Up List 
      </td>
    </tr>
    </table>
  </td>
</tr>
<tr>
  <td align="center">
<?
	if ($bShowOldList)
	{
?>
    <table width="450" border="0" cellpadding="1" cellspacing="2" bgcolor="#215B62" class="td_dropin" align="center">
<?
  		$res = db_query ("select * from pbs_signup order by ID asc");
		$entrynumb = 1;
		if (!mysql_num_rows($res))
		{
?>
	<tr>
	  <td>
	  	The list is currenty empty!<br>
	  	You can be the first to sign up!
	  </td>
	</tr>
<?
		} else
		while ($entry = mysql_fetch_assoc ($res))
		{
?>
        <tr align="center">
          <td>
            <? echo $entrynumb; ?> 
          </td>
          <td>
            <? echo $entry["Nick"]; ?> 
          </td>
          <td>
            <? echo $entry["Location"]; ?> 
          </td>
          <td>
            <? echo $entry["Email"]; ?> 
          </td>
        </tr>
<?
  			$entrynumb ++;
  		}
?>
    </table>
<?
	} else
	{ // else show new list
		$query = "select ID, Nick, date_format(Date, '%m/%d/%y %r') as Date, Location, Email, IP, Roster, SeededNum from pbs_signup";
		if (isset ($Sort))
			$query .= " order by $Sort";
		$rSignUps = db_query ($query);
		$entries = mysql_num_rows ($rSignUps);
		$entrynumb = 0;
		if (!isset ($all))
			$all = "Hide";
		else if ($all != "Hide" && $all != "Show") $all = "Hide";
		if (!isset ($expand))
			$expand = 0;
		if ($all == "Show" || $expand)
			$newall = "Hide";
		else
			$newall = "Show";
?>
	<table border="0" cellpadding="2" cellspacing="2" align="center" style="table-layout: fixed">
	<tr align="center">
	  <td width="150"><a href="?Sort=Date">Date</a></td>
	  <td width="150"><a href="?Sort=Nick"><? echo $gameTermS; ?></a></td>
	  <td width="120"><a href="?Sort=Location">Location</a></td>
	  <td width="160"><a href="?Sort=Email">Captain's Email</a></td>
	  <td width="150">Roster [<a href="?all=<? echo $newall; ?>"><? echo ($newall == "Show") ? "Expand All" : "Collapse All"; ?></a>]</td>
	</tr>
	<?
			if (!$entries)
			{
	?>
	<tr>
	  <td class="td_normal" colspan="5">there are currently none signed up.</td>
	</tr>
	<?
			} else
			while ($entry = mysql_fetch_assoc ($rSignUps))
			{
				$entrynumb ++;
				$arrRoster = split ("<br>", $entry["Roster"]);

				echo "<tr>\n";
	?>
	  <td class="td_normal"><? echo $entry["Date"]; ?></td>
	  <td class="td_normal"><? echo $entry["Nick"]; ?></td>
	  <td class="td_normal"><? echo $entry["Location"]; ?></td>
	  <td class="td_normal"><? echo $entry["Email"]; ?></td>
	<?
				$count = (!strlen ($arrRoster[0])) ? 0 : count ($arrRoster);
			  	if ($count && ($expand == $entry["ID"] || $all == "Show"))
			  	{
			  		echo "<td class=\"td_unsched\">";
			  		echo array_pop ($arrRoster);
			  		echo "</td>\n";
			  		$count --;
			  	} else if (!$count)
			  		echo "<td bgcolor=\"#990000\" align=\"center\">No Roster</td>\n";
			  	else
			  		echo "<td class=\"td_normal\"><a href=\"?expand=$entry[ID]\">+ Show</a></td>\n";
				
			  	echo "</tr>\n";
				
			  	if ($count && ($expand == $entry["ID"] || $all == "Show"))
				{
					echo "<tr>\n";
	            	echo "<td colspan=\"4\">&nbsp;</td>";
					if (count ($arrRoster))
					{
						$Player = array_pop ($arrRoster);
						echo "<td class=\"td_unsched\">$Player</td></tr>";
					} else echo "<td>&nbsp;</td></tr>";
					
					if (count ($arrRoster))
					foreach ($arrRoster as $Player)
					{
						echo "<tr><td colspan=\"4\">&nbsp;</td><td class=\"td_unsched\">$Player</td></tr>\n";
					}
				}
				if ($all == "Show")
				{	// if we are in condensed view...
	?>
	          <tr>
	            <td colspan="5">&nbsp;</td>
	          </tr>
	<?
				}
			}
	?>
	</table>
<?
	}
?>
  </td>
</tr>
<tr>
  <td align="center">
  Signup Status: <? echo ($entrynumb)."/$wanted"; if ($entrynumb == $wanted) echo " FULL!"; ?>
  </td>
</tr>
<tr>
  <td align="center">
  <? if ($entrynumb < $wanted) { ?>
  <form method="GET">
    <input type="submit" name="Do" class="button" value="Sign Up">
  </form>
  <? } ?>
  </td>
</tr>
</table>
<br>
<?
	} else
	if ($signedup < $wanted)
	{
?>
<br><br><br><br><br><br>
<form action="<? echo "$_SERVER[PHP_SELF]" ?>" method="post" style="margin-bottom:0;">
  <table width="250" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td align="center">
      <? echo $tourneyName; ?> Sign Up 
    </td>
  </tr>
  <tr>
    <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="2" align="center">
          <tr>
            <td>
              <? echo $gameTermS; ?>: 
            </td>
            <td class="">
              <input name="nick" type="text" class="text" size="32" maxlength="64"> 
            </td>
          </tr>
          <tr>
            <td>
              IRC Chan/Net: 
            </td>
            <td class="">
              <input name="location" type="text" class="text" size="32" maxlength="64"> 
            </td>
          </tr>
          <tr>
            <td>
              Captain's Email: 
            </td>
            <td class="">
              <input name="email" type="text" class="text" size="32" maxlength="64"> 
            </td>
          </tr>
          <tr>
            <td>
              Roster:
            </td>
            <td>
              <textarea rows="8" cols="40" name="roster" class="text"></textarea>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center">(1 player per line)</td>
          </tr>
          </table>
    </td>
  </tr>
  <tr>
    <td align="center">
      <input type="submit" name="Submit" class="button" value="Submit">
    </td>
  </tr>
  </table>
</form>
<br>
<?
	} else
	if ($signedup >= $wanted)
	{
?>
<table width="300" border="0" cellspacing="0" cellpadding="1" align="center">
<tr>
	<td bgcolor="#0E203D">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  <tr>
		<td bgcolor="#2C446B" class="td_sched" align="center">
		  The <? echo $tourneyName; ?> signup list is currently at full capacity<br>
		  Your too late jack!
		</td>
	  </tr>
	  </table>
	</td>
</tr>
<tr>
  <td align="center">
  <form method="GET">
    <input type="submit" name="Do" class="button" value="View List">
  </form>
  </td>
</tr>
</table>
<?
	}
?>
</body>
</html>
