<?
	function safeUnset ($var)
	{
		unset ($_SESSION[$var]);
	}

	$check = session_start();
	echo "<pre>\n";
	if (!$check) {
		"Error Starting Session, Test Aborted.\n";
	} else
	{
		echo "Session Started.\n";
		echo "Setting Session Array Variables..\n";
		{
			$_SESSION["Variable1"] = "Variable 1 Data";
			echo "+ \$_SESSION[\"Variable1\"] = \"$_SESSION[Variable1]\"\n";
			
			$_SESSION["Variable2"] = "Variable 2 Data";
			echo "+ \$_SESSION[\"Variable2\"] = \"$_SESSION[Variable2]\"\n";

			$_SESSION["Variable3"] = "Variable 3 Data";
			echo "+ \$_SESSION[\"Variable3\"] = \"$_SESSION[Variable3]\"\n";
			
			$_SESSION["Variable4"] = "Variable 4 Data";
			echo "+ \$_SESSION[\"Variable4\"] = \"$_SESSION[Variable4]\"\n";
		}
		echo "Printing Session ";
		print_r ($_SESSION);
		echo "Checking Session -> Gloval variable displacement..\n";
		{
			for ($i = 1; $i <= 4; $i ++)
				echo (isset (${"Variable$i"})) ? "- global variable \$Variable$i exists.\n" : "+ global variable \$Variable$i doesnt exist.\n";
		}
		echo "Unsetting Session Variables..\n";
		{
			echo "? Test 1: unset (\$_SESSION[\"Variable1\"])\n";
			unset ($_SESSION["Variable1"]);
			
			echo "? Test 2: unset (\$_SESSION[\"Variable2\"], \$Variable2)\n";
			unset ($_SESSION["Variable2"], $Variable2);
			
			echo "? Test 3: session_unregister (\"Variable3\")\n";
			session_unregister("Variable3");
			
			echo "? Test 4: safeUnset (\"Variable4\")\n";
			safeUnset ("Variable4");
		}
		echo "Checking Session Variables..\n";
		{
			for ($i = 1; $i <= 4; $i ++)
				echo (isset ($_SESSION["Variable$i"])) ? "- test $i failed: \$_SESSION[\"Variable$i\"] = ".$_SESSION["Variable$i"]."\n" : "+ test $i passed: \$_SESSION[\"Variable$i\"] doesnt exist.\n";
		}
		echo "Checking Post-Unset Session -> Gloval variable displacement..\n";
		{
			for ($i = 1; $i <= 4; $i ++)
				echo (isset (${"Variable$i"})) ? "- global variable \$Variable$i exists.\n" : "+ global variable \$Variable$i doesnt exist.\n";
		}
		session_destroy();
		echo "Session Ended.\n\n";
		{	// important info printout
			$outputBuffer = ini_get ("output_buffering");
				if (!$outputBuffer) $outputBuffer = "none";
			$registerGlobals = ini_get ("register_globals") ? "on" : "off";
			$displayErrors = ini_get("display_errors") ? "on" : "off";
			$uploadFilesize = ini_get("upload_max_filesize");
			$postFilesize = ini_get("post_max_size");
			$sessionTransid = ini_get("session.use_trans_sid") ? "on" : "off";
			$magicQuotesGpc = ini_get("magic_quotes_gpc") ? "on" : "off";
			$magicQuotesRuntime = ini_get("magic_quotes_runtime") ? "on" : "off";
			$magicQuotesSybase = ini_get("magic_quotes_sybase") ? "on" : "off";
			$mysqlPersistent = ini_get("mysql.allow_persistent") ? "on" : "off";
			
			echo "<hr width=\"600\" align=\"left\">\n";
			echo "PHP Version Info: " . phpversion() . "\n";
			echo "Relevant PHP Variable Dump:\n";
			echo "<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\n";
			echo "<tr><td width=\"200\">Variable Name</td><td width=\"50\" align=\"center\">Value</td><td width=\"60\" align=\"center\">Wanted</td></tr>\n";
			echo "<tr><td>PHP Register Globals</td><td align=\"center\">$registerGlobals</td><td align=\"center\">off</td></tr>\n";
			echo "<tr><td>PHP Output Buffer</td><td align=\"center\">$outputBuffer</td><td align=\"center\">none</td></tr>\n";
			echo "<tr><td>PHP Display Errors</td><td align=\"center\">$displayErrors</td><td align=\"center\">on</td></tr>\n";
			echo "<tr><td>PHP Max Upload Size</td><td align=\"center\">$uploadFilesize</td><td align=\"center\">--</td></tr>\n";
			echo "<tr><td>PHP Max Post Size</td><td align=\"center\">$postFilesize</td><td align=\"center\">--</td></tr>\n";
			echo "<tr><td>PHP Session TransID</td><td align=\"center\">$sessionTransid</td><td align=\"center\">off</td></tr>\n";
			echo "<tr><td>PHP Magic Quotes GPC</td><td align=\"center\">$magicQuotesGpc</td><td align=\"center\">off</td></tr>\n";
			echo "<tr><td>PHP Magic Quotes Runtime</td><td align=\"center\">$magicQuotesRuntime</td><td align=\"center\">off</td></tr>\n";
			echo "<tr><td>PHP Magic Quotes Sybase</td><td align=\"center\">$magicQuotesSybase</td><td align=\"center\">off</td></tr>\n";
			echo "<tr><td>PHP MYSQL Prsst. Connect</td><td align=\"center\">$mysqlPersistent</td><td align=\"center\">on</td></tr>\n";
			echo "</table>\n\n";
			echo "<hr width=\"600\" align=\"left\">\n";
		}
		echo "PHP Config Dump:\n";
		$arrVarData = ini_get_all();
		echo "<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\n";
		echo "<tr><td>Variable Name</td><td>Global Value</td><td>Local Value</td></tr>\n";
		foreach ($arrVarData as $key => $arrValue)
		{
			// print variable name
			echo "<tr><td>$key</td>";
			// print global value
			$value = array_shift ($arrValue);
			echo "<td>".((!$value) ? "Off" : (($value == 1) ? "On" : $value))."</td>";
			// print local value
			$value = array_shift ($arrValue);
			echo "<td>".((!$value) ? "Off" : (($value == 1) ? "On" : $value))."</td>";
			// next!
			echo "</tr>\n";
		}
		echo "</table>\n";
	}

	echo "</pre>\n";
?>