var lastTD = 0;
var nDate = 0;
var nHour = 17;
var nMinute = 30;

function setMinHour (Minute, Hour)
{
	nMinute = Minute;
	// haha this pads the 0 to 00 :D
	if (!nMinute) nMinute = "00";
	nHour = Hour;
}

function minSelect (that)
{
	var dateString = document.getElementById("dateString");
	//dateString.value = that[that.selectedIndex].value;
	nMinute = that[that.selectedIndex].value;
	if (nDate && nHour)
		dateString.value = nDate + " " + nHour + ":" + nMinute + ":00";
}

function hourSelect (that)
{
	var dateString = document.getElementById("dateString");
	//dateString.value = that[that.selectedIndex].value;
	nHour = that[that.selectedIndex].value;
	if (nDate && nHour)
		dateString.value = nDate + " " + nHour + ":" + nMinute + ":00";
}

function leapYear (Year)
{
	if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {
		return (true);
	} else {
		return (false);
	}
}

function getDays(month, year)
{
  // create array to hold number of days in each month
  var ar = new Array(12);
  ar[0] = 31; // January
  ar[1] = (leapYear(year)) ? 29 : 28; // February
  ar[2] = 31; // March
  ar[3] = 30; // April
  ar[4] = 31; // May
  ar[5] = 30; // June
  ar[6] = 31; // July
  ar[7] = 31; // August
  ar[8] = 30; // September
  ar[9] = 31; // October
  ar[10] = 30; // November
  ar[11] = 31; // December

  // return number of days in the specified month (parameter)
  return ar[month];
}

function changeMonth (month, year) {
    var store = new Array();
    var monthName = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    document.getElementById('writeroot').innerHTML = '';
    store.push('<table border="0" cellpadding="1" cellspacing="1" class="calendar">');
    store.push('<tr align="center"><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Th</th><th>Fri</th><th>Sat</th></tr>');
    var days = getDays(month - 1, year);
    store.push('<tr>');
    var firstOfMonth = new Date(year, month - 1, 1);
    var startingPos = firstOfMonth.getDay() + 1;
    var i;
    for (i = 1; i < startingPos; i++)
        store.push('<td>&nbsp;</td>');

    var day;
    for (i = startingPos; i < days + startingPos; i++) {
        day = i - startingPos + 1;
        store.push('<td id="td_' + day + '" class="regular" onclick="javascript:dateSelect(' + month + ', ' + day + ', ' + year + ');"><a href="javascript:dateSelect(' + month + ', ' + day + ', ' + year + ');">' + day + '</a></td>');
        if (i % 7 == 0) store.push('</tr><tr>');
    }
    store.push('</tr>');
    store.push('<tr>');
    var prevYear = year;
    var prevMonth = month - 1;
    if (prevMonth < 1) {
        prevMonth = 12;
        prevYear--;
    }
    var nextYear = year;
    var nextMonth = month + 1;
    if (nextMonth > 12) {
        nextMonth = 1;
        nextYear++;
    }
    store.push('<td colspan="1"><a href="javascript:changeMonth(' + prevMonth + ', ' + prevYear + ')">&lt;&lt;</a></td>');
    store.push('<td colspan="5">' + monthName[month - 1] + ', ' + year + '</td>');
    store.push('<td colspan="1"><a href="javascript:changeMonth(' + nextMonth + ', ' + nextYear + ')">&gt;&gt;</a></td>');
    store.push('</tr>');
    /*
     for (var i = 1; i < days; i += 7)
     {
     store.push('<tr>');
     for (var j = i; j < i + 7; j ++)
     {
     store.push('<td>');
     store.push(j)
     store.push('</td>');
     }
     store.push('</tr>');
     }
     */
    store.push('</table>');
    document.getElementById('writeroot').innerHTML = store.join('');
}

function dateSelect (month, day, year)
{
	if (!month || !day) return false;
	var td = document.getElementById("td_"+day);
	var dateString = document.getElementById("dateString");
	
	if (lastTD)
		lastTD.className = "regular";
	td.className = "chosen";
	nDate = year + "-" + month + "-" + day;
	//dateString.value = month + "/" + day + "/" + year;
	
	lastTD = td;
	if (nDate && nHour)
		dateString.value = nDate + " " + nHour + ":" + nMinute + ":00";
}