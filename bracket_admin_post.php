<?
	require_once "bracket_connect.php";
    require_once "bracket_cfg.php";
	require_once "bracket_ccode.php";
	require_once "bracket_login.php";		// plug-in security ;)
	
	//var_dump($_POST);
	
	$DEBUG = 0;
	function decho ($string) {
		global $DEBUG;
		if ($DEBUG == 1)
			echo "debug: " . $string . "<br>\n";
	}
	
	function checkSqlErrors($line)
	{
		if (mysql_error())
			echo "error php line $line:<br>" . mysql_error() . "<br>\n";
	}
	
	extract ($_GET);
	extract ($_POST, EXTR_OVERWRITE);

	if (isset ($delSID))
	{
		$nSpot1 = 0;
		$nSpot2 = 0;
		if (strstr ($delSID, "/"))
			list ($nSpot1, $nSpot2) = split ("/", $delSID);
		else
			$nSpot1 = $delSID;
		//echo "nSpot1 = $nSpot1<br>nSpot2 = $nSpot2<br>";
		if ($nSpot1) {
			db_query ("update pbs_bracket set PlayerID = 0, Timestamp = Timestamp where ID = $nSpot1");
			db_query ("delete from pbs_rounds where BracketID = $nSpot1");
			db_query ("delete from pbs_comments where BracketID = $nSpot1");
		}
		if ($nSpot2) {
			db_query ("update pbs_bracket set PlayerID = 0, Timestamp = Timestamp where ID = $nSpot2");
			//db_query ("delete from pbs_rounds where BracketID = $nSpot2 or DropInID = $nSpot2");
		}
		header ("Location: bracket_admin.php");
	} else
	if (isset ($signupDel))
	{
		db_query ("delete from pbs_signup where ID = $signupDel");		
		header ("Location: bracket_admin.php?ViewMode=Signups");
	} else
	if (isset ($setSignups) && $setSignups == "ClearList")
	{
		if (isset ($Confirmed))
		{
			if ($Confirmed == "Yes")
			{	// confirmed the signups clearing
				db_query ("truncate pbs_signup");
				safeUnset("Confirm");
			}
			header("Location: bracket_admin.php?ViewMode=Signups");
			exit;		
		} else
		{	// make a signups clear confirmation request
			$_SESSION["Confirm"] =
				array
				(
					"return"	=>	$_SERVER['REQUEST_URI'],
					"variable"	=>	"setSignups",
					"value"		=>	"ClearList",
					"body"		=>	"Are you sure you want to clear the signups list?",
					"cfname"	=>	"Confirmed",
				);
			header("Location: bracket_confirm.php");
			exit;
		}
	} else
	if (isset ($remove))
	{
		if (isset ($Confirmed))
		{
			if ($Confirmed == "Yes")
			{	// confirmation granted
				if ($remove == "DBOnly")
				{	// clean out the database
					// read in the table array
					$errors = 0;
					require_once "bracket_install.inc";
					foreach ($CTables as $table)
					{
						mysql_query ("drop table $table[Name]");
						if (mysql_error())
						{
							echo "Error Dropping Table [$table[Name]]: " . mysql_error() . "<br>\n";
							$errors ++;
						}
					}
					if (!$errors)
					{
						if (mysql_num_rows(mysql_query ("show tables")) == 0)
						{
							$res = mysql_query ("select database()");
							if (mysql_error())
							{
								echo "Error getting database name. Database could not be dropped.<br>Details: " . mysql_error() . "<br>\n";
								$errors ++;
							} else
							{
								$dbname = mysql_result ($res, 0);
								mysql_query ("drop database $dbname");
								if (mysql_error())
								{
									echo "Error dropping database. Maybe you lack permissions?<br>Details: " . mysql_error() . "<br>\n";
									$errors ++;
								} else echo "Database '$dbname' has been dropped successfully.<br>\n";
							}
						} else echo "Database contains table entries other than our own, it will not be dropped.<br>\n";
					}
					
					@unlink ("bracket_connect.php");
					
					if (!$errors)
						echo "<br>Bracket SQL data successfully removed.<br>Demos and Screenshots have been left untouched.<br>\n";
					else
						echo "<br>Some errors occured during uninstall.<br>\n";
										
					exit;
				} else
				if ($remove == "Full")
				{	// clean out the database and remove demos/screenshots
					$errors = 0;
					require_once "bracket_install.inc";
					
					$arrFiles = array();
					$res = mysql_query ("select DemoURL from pbs_rounds where DemoURL is not null");
					while ($file = mysql_fetch_assoc ($res))
						$arrFiles [] = $file["DemoURL"];
					
					foreach ($arrFiles as $file)
					{
						if (!unlink ($destinationPath."/".$file))
						{
							echo "Error Deleting Demo: $destinationPath/$file<br>\n";
							$errors ++;
						}// echo "deleted $destinationPath/$file?<br>\n";
					}
					
					if ($errors)
					{	// oh no errors!
						echo "<br>Errors Occured while deleting Demos... database drop will not occur.<br>\n";
						echo "If you still want to drop database, use the db-only remove option.<br>\n";
					} else
					{
						echo "Deleted " . count ($arrFiles) . " demos successfully.<br>\n";
						
						// clear screenshots (ez pie)
						$handle = opendir ("screenshots");
						while ($file = readdir ($handle))
						{
							if (is_file ("screenshots/".$file))
								if (!unlink ("screenshots/".$file))
								{
									echo "Error deleting screenshot $file<br>\n";
									$errors++;
								}// else echo "deleted $file?<br>\n";
						}
						closedir ($handle);
						
						if (!$errors)
						{	// if no errors occured thus far, drop the tables!
						
							echo "Screenshots deleted successfully.<br>\n";
						
							foreach ($CTables as $table)
							{
								mysql_query ("drop table $table[Name]");
								if (mysql_error())
								{
									echo "Error Dropping Table [$table[Name]]: " . mysql_error() . "<br>\n";
									$errors ++;
								} else echo "Table $table[Name] has been dropped successfully.<br>\n";
							}
						}
						
						if (!$errors)
						{	// if no errors occured while droppin tables, see if database is empty, if it is drop it too
							if (mysql_num_rows(mysql_query ("show tables")) == 0)
							{
								$res = mysql_query ("select database()");
								if (mysql_error())
								{
									echo "Error getting database name. Database could not be dropped.<br>Details: " . mysql_error() . "<br>\n";
									$errors ++;
								} else
								{
									$dbname = mysql_result ($res, 0);
									mysql_query ("drop database $dbname");
									if (mysql_error())
									{
										echo "Error dropping database. Maybe you lack permissions?<br>Details: " . mysql_error() . "<br>\n";
										$errors ++;
									} else echo "Database '$dbname' has been dropped successfully.<br>\n";
								}
							} else echo "Database contains table entries other than our own, it will not be dropped.<br>\n";
						}
					}
					
					@unlink ("bracket_connect.php");
										
					if (!$errors)
						echo "<br>Bracket has been successfully uninstalled.<br>SQL data, demos, and screenshots were all removed!.<br>\n";
					else
						echo "<br>Some errors occured during uninstall, page will not be redirected.<br>\n";
						
					exit;
				}
				else
					echo "Confirmed = [$Confirmed] stuck<br>\n";
			}
		} else
		{
			$desc = ($remove == "DBOnly") ? "database-only" : "full-bracket";
			$_SESSION["Confirm"] =
				array
				(
					"return"	=>	"bracket_admin_post.php",
					"variable"	=>	"remove",
					"value"		=>	"$remove",
					"body"		=>	"Are you sure you want to proceed with the $desc erasure?",
					"cfname"	=>	"Confirmed",
				);
			header("Location: bracket_confirm.php");
			exit;
		}
	}
	
	if (isset ($delMap))
	{
		db_query ("delete from pbs_mappool where ID = $delMap");
		header ("Location: bracket_admin.php?ViewMode=MapPool");
		exit;
	}
	
	if (isset ($addReferee) && isset ($iRefName))
	{
		$iRefAdmin = (isset ($iRefAdmin)) ? 'Y' : 'N';
		db_query ("insert into pbs_referees (RefName, Login, Passwd, bIsAdmin) values ('$iRefName', '$iRefLogin', md5('$iRefPasswd'), '$iRefAdmin')");
		if (mysql_error()) echo mysql_error();
		else header ("Location: bracket_admin.php?ViewMode=Referees");
	}
	
	if (isset ($updateReferee) && isset ($iRefName))
	{
		$iRefAdmin = (isset ($iRefAdmin)) ? 'Y' : 'N';
		if (isset($iRefPasswd) && strlen($iRefPasswd) > 1)
			db_query ("update pbs_referees set RefName = '$iRefName', Login = '$iRefLogin', Passwd = md5('$iRefPasswd'), bIsAdmin = '$iRefAdmin' where ID = $iRefID");
		else
			db_query ("update pbs_referees set RefName = '$iRefName', Login = '$iRefLogin', bIsAdmin = '$iRefAdmin' where ID = $iRefID");

		if (mysql_error()) echo mysql_error();
		else header ("Location: bracket_admin.php?ViewMode=Referees");
	}
	
	if (isset ($delReferee))
	{
		db_query ("delete from pbs_referees where ID = $delReferee");
		if (mysql_error()) echo mysql_error();
		else header ("Location: bracket_admin.php?ViewMode=Referees");
	}
	
	if (isset ($delIpBan))
	{
		if (get_magic_quotes_gpc()) $delIpBan = stripslashes($delIpBan);
		db_query ("delete from pbs_blockedips where IP = $delIpBan");
		if (mysql_error()) echo mysql_error();
		else header ("Location: bracket_admin.php?ViewMode=Referees");
	}
	
	if (isset ($addIpBan))
	{	// -1 indicates a manually added ip block
		db_query ("insert into pbs_blockedips (IP, FailedAuths) values ('$iIpAddy', -1)");
		if (mysql_error()) echo mysql_error();
		else header ("Location: bracket_admin.php?ViewMode=Referees");
		exit;
	}
	
	if (isset($setMaxSignups))
	{
		if ($setMaxSignups == "Update")
		{
			if ($iMaxSignups < 0 || $iMaxSignups > 1000)
			{
				echo "invalid maxentries range, please pick something between 0 and 1000...<br>\n";
				exit;
			}
			db_query ("update pbs_config set Value = $iMaxSignups where Label = 'MaxSignups'");
			if (mysql_error()) echo mysql_error();
			else header ("Location: bracket_admin.php?ViewMode=Signups");
		}
	}
	
	if (isset ($seedAction))
	{
		switch ($seedAction)
		{
			case "Build List":
			{
				$rawPlayerNames = trim ($rawPlayerNames);
				$aRawPlayers = split ("\n", $rawPlayerNames);
				$filter = "/\d*\.?\s*(.+)/";
				if (strstr ($aRawPlayers[0], "from mysql")) {
					array_shift ($aRawPlayers);
					// 
					$filter = "/[\W\s+\d+\s+]*\W\s+(.+)\s+\W/";
				}
				$nNumPlayers = sizeof ($aRawPlayers);
				/*
				// build seeds list
				$aSeeds = buildSeedList ($nNumPlayers);
				// build bracket to player link
				for ($i = 0; $i < $nNumPlayers; $i ++)
				{
					$nID = $i * 2 + 1;
					$nSeededID = $aSeeds[$i];
					//db_query ("insert into pbs_bracket (ID, PlayerID, Timestamp) values ($nID, $nSeededID, Timestamp)");
					db_query ("update pbs_bracket set PlayerID = $nSeededID, Timestamp = Timestamp where ID = $nID");
				}
				*/
				
				// build player list that bracket refers to
				$nPlayerID = 0;
				foreach ($aRawPlayers as $sRawPlayer)
				{
					preg_match ($filter, $sRawPlayer, $aMatch);
					if (!isset ($aMatch[1])){
						echo "invalid data supplied, filters unable to process it, exiting.<br>";
						exit;
					}
					$newPlayer = trim ($aMatch[1]);
					$nPlayerID ++;
					db_query ("update pbs_players set Nick = '$newPlayer' where ID = $nPlayerID");
					//echo "Nick = '$newPlayer' ID = $nPlayerID<br>\n";
				}
				header ("Location: bracket_admin.php");
			} break;
			
			case "Update":
			{
				for ($i = 1; $i <= $nPlayers; $i ++)
				{
					$sNewPlayer = safeString (${"sPlayer_$i"});
					db_query ("update pbs_players set Nick = '$sNewPlayer' where ID = $i");					
				}
				//db_query ("truncate pbs_players");
				//db_query ($query);
				header ("Location: bracket_admin.php");
			} break;
			
			default:
				echo "warning: switch($seedAction) default case.<br>\n";			
		}
	}
	
	if (isset ($detailAction))
	{
		$location = "bracket_referee.php";
		if ($_SESSION["bIsAdmin"] == 'Y')
			$location = "bracket_admin.php";
			
		switch ($detailAction)
		{
			case "Set Detail":
			{
				for ($i = 1; $i <= $nMPG; $i ++)
				{
					$nWScore = ${"nWScore_$i"};
					$nLScore = ${"nLScore_$i"};
					$nID = ${"nRoundID_$i"};
					$nMapID = ${"nMap_$i"};
					db_query ("update pbs_rounds set WinnerScore = $nWScore, LoserScore = $nLScore, MapID = $nMapID, SequenceNum = $i where ID = $nID");
					//checkSqlErrors (__LINE__);
					if (mysql_error()) echo mysql_error();
				}
				header ("Location: $location");
			} break;
			case "Set Date":
			{
				//echo "nSpotID = $nSpotID<br>";
				$extra = (isset ($iReferee)) ? " RefID = $iReferee," : "";
				$query = "update pbs_bracket set Timestamp = '$dateString',$extra Opponent1 = $iOpponent1, Opponent2 = $iOpponent2 where ID = $nSpotID";
				db_query ($query);
				if (mysql_error()) echo mysql_error();
				else header ("Location: $location");
			} break;
			case "Clear":
			{
				$query = "update pbs_bracket set Timestamp = 0, RefID = 0 where ID = $nSpotID";
				db_query ($query);
				if (mysql_error()) echo mysql_error();
				else header ("Location: $location");				
			}
			case "Cancel":
			{
				header ("Location: $location");
			} break;
			default:
				echo "warning: switch($detailAction) default case.<br>\n";
		}
	}

function saveConfig($br_config)
{
    $fconfig = @fopen("bracket_cfg.xml", "wb"); // prevents \r\n translations and shit
    if ($fconfig) {
        fwrite($fconfig, $br_config, strlen($br_config)); // specifying size bypasses magic quotes
        fclose($fconfig);
        chmod("bracket_cfg.xml", 0666);
    } else
    {
        die("Could not write config to bracket_cfg.xml<br>Check file permissions. Install aborted cleanly.");
    }
}

if (isset ($Action))
	{
		switch ($Action)
		{
			case "Set Names":
			{
				//decho ("switch() set names case.");
				$nPlayerID = 0;
				for ($i = 1; $i <= $nSEPlayers; $i ++)
				{
					$nPlayerID ++;
					if (isset (${"sNames_$i"}))
					{
						$Nick = ${"sNames_$i"};
						db_query ("update pbs_players set Nick = '$Nick' where ID = $nPlayerID");
					}
				}
				header ("Location: bracket_admin.php");
			} break;
			
			case "Set Standings":
			{
				$editSID = 0;
				$changes = 0;
				$lastsid = 0;
				//decho ("switch() set names case.");
				$newStandings = split (",", $WBStandingString);
				//echo "<pre>";
				//print_r ($newStandings);
				if (strlen ($WBStandingString))
				//for ($i = 0; $i < sizeof ($newStandings); $i ++)
				foreach ($newStandings as $newStanding)
				{
					// break down winner/loser standing ids
					list ($nW_SID, $nL_SID) = split ("/", $newStanding);
					// if we have a set winner position id
					if (!${"sWBracket_".$nW_SID}) continue;
					list ($nW_PID, $nL_PID) = split ("/", ${"sWBracket_" . $nW_SID});
					// if we have a set loser position id
					if ($nL_SID)
					{	// have loser bracket position to insert to
						//echo "tossing player $nL_PID into loser bracket on spot $nL_SID<br>\n";
						db_query ("update pbs_bracket set PlayerID = $nL_PID, Timestamp = Timestamp where ID = $nL_SID");
						checkSqlErrors(__LINE__);
					}
					// each match has nMPG rounds/maps in it (for ch3 there will be 3 maps per match)
					for ($i = 1; $i <= $nMPG; $i ++)
						db_query ("insert into pbs_rounds (BracketID, DropInID, WinnerPID, LoserPID, SequenceNum) "
									."values ($nW_SID, $nL_SID, $nW_PID, $nL_PID, $i)");
									//echo mysql_error();
					//echo "new standing $nW_PID = " . ${"sWBracket_" . $nW_SID} . "<br>\n";
					db_query ("update pbs_bracket set PlayerID = $nW_PID, Timestamp = Timestamp where ID = $nW_SID");
					if (mysql_error()) echo mysql_error();
					unset (${"sWBracket_".$nW_SID});
					$changes ++;
					$editSID = $nW_SID;
				}

				$newStandings = split (",", $LBStandingString);
				//foreach ($newStandings as $nSID)
				if (strlen ($LBStandingString))
				//for ($i = 0; $i < sizeof ($newStandings); $i ++)
				foreach ($newStandings as $newStanding)
				{
					// break down winner/loser standing ids
					$nW_SID = $newStanding;
					// if we have a set winner position id
					//echo "handling sLBracket_$nW_SID..<br>";
					if (!$nW_SID) continue;
					if (!${"sLBracket_" . $nW_SID}) continue;
					list ($nW_PID, $nL_PID) = split ("/", ${"sLBracket_" . $nW_SID});
					//echo "nW_SID = $nW_SID<br>";
					//echo "nW_PID = $nW_PID<br>";
					//echo "nL_PID = $nL_PID<br>";
					//$nL_PID = ${"sLBracket_" . $nL_SID};
					// each match has nMPG rounds/maps in it (for ch3 there will be 3 maps per match)
					for ($i = 1; $i <= $nMPG; $i ++)
						db_query ("insert into pbs_rounds (BracketID, DropInID, WinnerPID, LoserPID, SequenceNum) "
									."values ($nW_SID, 0, $nW_PID, $nL_PID, $i)");
					db_query ("update pbs_bracket set PlayerID = $nW_PID, Timestamp = Timestamp where ID = $nW_SID");
					if (mysql_error()) echo mysql_error();
					$changes ++;
					$editSID = $nW_SID;
				}
				if ($changes == 1 && $editSID)
				{	// we only set 1 result... so why not go directly to scores editor?
					header( "Location: bracket_admin.php?editSID=$editSID" );
				} else
					header ("Location: bracket_admin.php");
				exit;
			} break;
			case "Rename":
            {
                // save $tourneyName only
                $gm = getGameMode();
                $br_config = <<<XML
<?xml version='1.0' encoding="ISO-8859-1"?>
<config>
    <tourney>$tourneyName</tourney>
    <game>$gm</game>
</config>
XML;
                saveConfig($br_config);
            } break;
			case "Build Bracket":
			{
			    if( strlen($gameMode)<2 ) die( "You must specify a game mode for tourney bracket" );
				if( $nSEPlayers % 2 ) die( "You must specify a 2^x number of players for winner's bracket" );
				if( $nDEPlayers % 2 ) die( "You must specify a 2^x number of players for loser's bracket" );
				
				if( strlen($gameMode)>4 ) die( "The maximum length of gamo mode name is 4" );
				if( $nSEPlayers > 128 ) die("The maximum number of players the winner's bracket may have is 128" );
				if( $nDEPlayers > 64 ) die("The maximum number of players the loser's bracket may have is 64" );
				if( $nDEPlayers >= $nSEPlayers ) die( "The maximum size of the loser's bracket has to be 1/2 of the winner's bracket" ); 
				if( $nMPG < 1 || $nMPG>10 ) die( "Wrong number maps per game" ); 
				
				//decho ("switch() build bracket case.");
				db_query ("truncate pbs_players");
				db_query ("truncate pbs_bracket");
				db_query ("truncate pbs_rounds");
				db_query ("truncate pbs_comments");
				
				// save $gameMode $tourneyName
		        $br_config = <<<XML
<?xml version='1.0' encoding="ISO-8859-1"?>
<config>
    <tourney>$tourneyName</tourney>
    <game>$gameMode</game>
</config>
XML;
                saveConfig($br_config);

				$nDERows	= log10 ($nDEPlayers) / log10(2) * 4 + 1;
				$nSERows	= log10 ($nSEPlayers) / log10(2) * 2 + 1;
				
				/*
					pbs_bracket size for loser bracket sizes:
					2	-	5
					4	-	13
					8	-	29
					16	-	61
					32	-	125
					64	-	253
					128	-	509
					
					f(x) = (5 * 2 + 3)
					n(x) = log(x)/log(2) * 2 + 1)
					
					pbs_bracket size for winner bracket sizes:
					2	-	4
					4	-	8
					8	-	16
					16	-	32
					32	-	64
					64	-	128
					128	-	256
				*/
				
				// number of rows required in pbs_bracket for each table
				$numWB = $nSEPlayers * 2;
				$numLB = getLBSize($nDEPlayers);
				if ($numLB) $numLB += 1;			// 1 more for final round
				$numRows = $numWB + $numLB;
				
				// proper filler :)
				for ($i = 1; $i <= $numRows; $i ++)
					db_query ("insert into pbs_bracket (ID, PlayerID, Timestamp) values ($i, 0, 0)");
				// player list
				for ($i = 1; $i <= $nSEPlayers; $i ++)
					db_query ("insert into pbs_players (ID, Nick) values ($i, '-TBA-')");
				//echo "inserted player names into player list.<br>";
				
				db_query ("update pbs_config set Value = '$nSEPlayers' where Label = 'SEPlayers'");
				db_query ("update pbs_config set Value = '$nDEPlayers' where Label = 'DEPlayers'");
				db_query ("update pbs_config set Value = '$nMPG' where Label = 'MPG'");
				
				require_once ("bracket_link.php");
				
				$aSeeds = buildSeedList ($nSEPlayers);
				for ($i = 0; $i < $nSEPlayers; $i ++)
				{
					$nID = $i * 2 + 1;
					$nSeededID = $aSeeds[$i];
					db_query ("update pbs_bracket set PlayerID = $nSeededID, Timestamp = Timestamp where ID = $nID");
				}
				//echo "inserted player links into main bracket.<br>";
				
				header ("Location: bracket_admin.php?ViewMode=Seeder");				
			} break;
			
			case "Resize":
			{
			    if( strlen($gameMode)<2 ) die( "You must specify a game mode for tourney bracket" );
				if( strlen($gameMode)>4 ) die( "The maximum length of gamo mode name is 4" );

            // save $gameMode $tourneyName
            $br_config = <<<XML
<?xml version='1.0' encoding="ISO-8859-1"?>
<config>
    <tourney>$tourneyName</tourney>
    <game>$gameMode</game>
</config>
XML;
                saveConfig($br_config);

			  // dont take nseplayers from the form for resize... simple mistake, bad consequences!
				$nSEPlayers = mysql_result( db_query("select Value from pbs_config where Label = 'SEPlayers'"), 0 );
				$numWB = $nSEPlayers * 2;
				$numLB = getLBSize($nDEPlayers);
				if ($numLB) $numLB += 1;			// 1 more for final round
				$numRows = $numWB + $numLB;
				// erase loser bracket to shrink table
				db_query ("delete from pbs_bracket where ID > $numWB");
				db_query ("delete from pbs_comments where BracketID > $numWB");
				// proper loser bracket filler, thanks to jwalk for findin the resize bug!
				for ($i = $numWB + 1; $i <= $numRows; $i ++)
					db_query ("insert into pbs_bracket (ID, PlayerID, Timestamp) values ($i, 0, 0)");
				
				db_query ("update pbs_config set Value = '$nDEPlayers' where Label = 'DEPlayers'"); 
				db_query ("update pbs_config set Value = '$nMPG' where Label = 'MPG'");
				
				require_once ("bracket_link.php");
				header ("Location: bracket_admin.php");
			} break;
			
			default:
				echo "warning: switch($Action) default case.<br>\n";
		}
	}
		
	if (isset ($mapAction))
	{
		switch ($mapAction)
		{
			case "Add Map":
			{
				if (!isset ($iMapName) || !isset ($iMapImage)) {
					echo "mandatory fields: mapname and mapimage.<br>\n";
					exit;
				}
				if (!isset ($iDescription)) $iDescription = "No Description";
				if (!isset ($iMapURL)) $iMapURL = "";
				$iDescription = safeString (safeNL2Br($iDescription));
				$iMapURL = safeString ($iMapURL);
				$iMapName = safeString ($iMapName);
				$query = "insert into pbs_mappool (MapName, MapImage, URL, Description) values "
						."('$iMapName', '$iMapImage', '$iMapURL', '$iDescription')";
				db_query ($query);
				if (mysql_error()) echo mysql_error();
				else header ("Location: bracket_admin.php?ViewMode=MapPool");
			} break;
			
			case "Update Map":
			{
				if (!isset ($iMapName) || !isset ($iMapImage)) {
					echo "mandatory fields: mapname and mapimage.<br>\n";
					exit;
				}
				if (!isset ($iMapID)) {
					echo "MapID unspecified: missing hidden input element in Map Edit form?<br>\n";
					exit;
				}
				if (!isset ($iDescription) || strlen ($iDescription) < 2) $iDescription = "No Description";
				if (!isset ($iMapURL) || strlen ($iMapURL) < 2) $iMapURL = "";
				
				$iDescription = safeString (safeNL2Br ($iDescription));
				$iMapName = safeString ($iMapName);
				$iMapURL = safeString ($iMapURL);
				
				$query = "update pbs_mappool set MapName = '$iMapName', MapImage = '$iMapImage', "
						."URL = '$iMapURL', Description = '$iDescription' where ID = $iMapID";
				db_query ($query);
				if (mysql_error()) echo mysql_error();
				else header ("Location: bracket_admin.php?ViewMode=MapPool");
			} break;
			
			default:
				echo "warning: switch($mapAction) default case.<br>\n";
		}
	}
	
	if (isset ($doTeam))
	{
		if ($doTeam == "Submit")
		{
			$returnMode = "Teams";			
			if (isset($iTeamID))
			{
				db_query ("update pbs_signup set SeededNum = 0 where SeededNum = $iTeamID");
			}
			
			
			$iTeamData = safeNL2Br ( htmlentities ( trim($iTeamData) ) );
			$iTeamName = safeString ($iTeamName);
			$iTeamLocation = safeString ($iTeamLocation);
			$arrTeamData = split("<br>", $iTeamData);
			$numErrors = 0;
			db_query ("update pbs_players set Nick = '$iTeamName', Location = '$iTeamLocation' where ID = $iTeamID");
			if (mysql_error()) { echo "error updating team name: " . mysql_error() . "<br>\n"; $numErrors ++; exit; }
			db_query ("delete from pbs_teamdata where TeamID = $iTeamID");
			if (strlen($arrTeamData[0]))
			foreach ($arrTeamData as $Player)
			{
				$Captain = 'N';
				if (strstr ($Player, "+captain"))
					$Captain = 'Y';
				$Player = array_shift( explode("+captain", $Player) );
				//$arrPlayer = explode("+captain", $Player);
				//$Player = $arrPlayer[0];
				//if (strlen ($Player) < 2) continue;
				$Player = safeString ($Player);
				
				db_query ("insert into pbs_teamdata (TeamID, Captain, Name) values ($iTeamID, '$Captain', '$Player')");
				if (mysql_error()) { echo "error inserting player name: " . mysql_error() . "<br>\n"; $numErrors++; exit; }
			}
			
			if (isset ($iSignupID))
			{
				db_query ("update pbs_signup set SeededNum = $iTeamID where ID = $iSignupID");
				if (mysql_error()) { echo "error seeting signup seednum:" . mysql_error() . "<br>\n"; $numErrors++; exit; }
				$returnMode = "Signups";
			}
			
			if ($numErrors == 0)
				header ("Location: bracket_admin.php?ViewMode=$returnMode");
			else
				echo "Errors Occured :(<br>\n";
		} else
		if ($doTeam == "Delete")
		{
			db_query ("delete from pbs_teamdata where TeamID = $iTeamID");
			header ("Location: bracket_admin.php?ViewMode=Teams");
		} else
		if ($doTeam == "Cancel")
		{
			header ("Location: bracket_admin.php?ViewMode=Teams");
		} else
		if ($doTeam == "Swap")
		{	// to be honest, i find the following disgusting and disgraceful, but i wanna get this ready for yrim asap!
		
			$rTeamname = db_query ("select Nick, Location from pbs_players where ID = $iTeamID");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 1a", mysql_error()); exit; }
	    	$team1 = mysql_fetch_assoc ($rTeamname);
	    	
	    	$rTeamname = db_query ("select Nick, Location from pbs_players where ID = $iSeedNum");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 1b", mysql_error()); exit; }
	    	$team2 = mysql_fetch_assoc ($rTeamname);
	    	
	    	db_query ("update pbs_players set Nick = '$team2[Nick]', Location = '$team2[Location]' where ID = $iTeamID");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 2a", mysql_error()); exit; }
	    	db_query ("update pbs_players set Nick = '$team1[Nick]', Location = '$team1[Location]' where ID = $iSeedNum");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 2b", mysql_error()); exit; }
	    	
	    	db_query ("update pbs_signup set SeededNum = -1 where SeededNum = $iSeedNum");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 3a", mysql_error()); exit; }
	    	db_query ("update pbs_signup set SeededNum = $iSeedNum where SeededNum = $iTeamID");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 3b", mysql_error()); exit; }
	    	db_query ("update pbs_signup set SeededNum = $iTeamID where SeededNum = -1");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 3c", mysql_error()); exit; }
	    	

	    	db_query ("update pbs_teamdata set TeamID = -1 where TeamID = $iTeamID");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 4a", mysql_error()); exit; }
	    	db_query ("update pbs_teamdata set TeamID = $iTeamID where TeamID = $iSeedNum");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 4b", mysql_error()); exit; }
	    	db_query ("update pbs_teamdata set TeamID = $iSeedNum where TeamID = -1");
	    	if (mysql_error()) { showHint ("Error In doTeam=Swap Step 4c", mysql_error()); exit; }
	    	
	    	header ("Location: bracket_admin.php?ViewMode=Signups");
		} else
			echo "lost in switch() (doTeam = $doTeam) call putty for help :/<br>\n";
	}
	
	if( isset( $setRules ) )
	{
		$Rules = safeString( safeNL2Br( $Rules ) );
		$Author = safeString( $Author );
		
		db_query( "delete from pbs_textdata where TextType = 'Rules'" );
		db_query( "insert pbs_textdata set TextType = 'Rules', Author = '$Author', Body = '$Rules'" );
		
		header ("Location: bracket_admin.php?ViewMode=Rules");
	}
	
	if( isset( $setNews ) )
	{
		$News = safeString( safeNL2Br( $News ) );
		$Title = safeString( $Title );
		$Author = safeString( $Author );
		
		if( isset( $newsAction ) )
		{
			if( $newsAction == "Close" )
			{
				header( "Location: bracket_admin.php?ViewMode=News" );
				exit;
			} elseif( $newsAction == "Delete" )
			{
				db_query( "delete from pbs_textdata where ID = $NewsId" );
				header( "Location: bracket_admin.php?ViewMode=News" );
				exit;
			}
		}
		
		$query = "pbs_textdata set Title = '$Title', Author = '$Author', Body = '$News'";
		
		if( $NewsId ) {
			db_query( "update $query where ID = $NewsId" );
		} else {
			db_query( "insert $query, TextType = 'News', Created = now()" );
			$NewsId = mysql_insert_id();
		}
		
		header( "Location: bracket_admin.php?ViewMode=News&NewsId=$NewsId" );
	}

?>
