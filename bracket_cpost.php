<?
	require_once "bracket_connect.php";
    require_once "bracket_cfg.php";
	require_once "bracket_ccode.php";
	require_once "bracket_login.php";		// plug-in security ;)

	extract ($_GET);
	extract ($_POST);
	extract ($_FILES);
	
	if( isset( $setScores ) )
	{
		$allgood = true;
		for ($i = 1; $i <= $nMPG; $i ++)
		{
			$nWScore = ${"nWScore_$i"};
			$nLScore = ${"nLScore_$i"};
			$nID = ${"nRoundID_$i"};
			$nMapID = ${"nMap_$i"};
			if (!$nMapID) continue;		// if this round wasnt played, skip
			
			$ssFile = handleScreenshot ($nID, ${"iScreenshot_$i"});
			if (!$ssFile) $allgood = false;
			if (isset (${"iDemo_$i"}))
				$iDemo = ${"iDemo_$i"};
			else $iDemo = 0;
			db_query ("update pbs_rounds set WinnerScore = $nWScore, LoserScore = $nLScore, MapID = $nMapID, SequenceNum = $i, DemoID = $iDemo where ID = $nID");
		}
		if( $allgood )
			header ("Location: bracket_login.php?doRedirect=1");
		exit;
	}
	
	echo "no actions requested so... hit back and.. do somethin! \o/\n";
?>