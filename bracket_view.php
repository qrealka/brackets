<?
	require_once "bracket_connect.php";	// mysql connection
	require_once "bracket_ccode.php";	// shared bracket code
    require_once "bracket_cfg.php";

	
	db_init();
	# length name variables (to prevent super long team/player names from screwing shit up)
	$LONGEST_NAME_IN_MATCH_REVIEWS = 18;
?>
<html>
<head>
<title><? echo getTourneyName(); ?>: Bracket View</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<?	
	$ButtonMenu = array (
		0 => strtr($gameTermP, "tp", "TP"),
		1 => "Upcoming",
		2 => "History",
		3 => "MapList",
		4 => "Standings",
		5 => "Ranking",
		6 => "Rules",
		7 => "News",
		8 => "About",
	);
	
	$nWName = 170;
	$nWSpace = 8;
	
	//$noDataSign = "[<a title=\"No Info Available\">N/I</a>]";
	$noDataSign = "";
	
	$show = "Standings";
	
	extract ($_GET, EXTR_OVERWRITE);
	extract ($_POST, EXTR_OVERWRITE);
?>
<body>
<form method="get">
<table border="0" cellpadding="0" cellspacing="2" align="center" class="sexy_buttons">
<tr>
<?
	foreach ($ButtonMenu as $Button)
		echo "<td".(!strcasecmp($show, $Button) ? " id=\"selected\"" : "")."><a class=\"sexy_button\" href=\"?show=$Button\">$Button</a></td>\n";
?>
</tr>
</table>
</form>
<?
  if( $show == "Rules" )
  {
  	$resid = db_query( "select Author, Body from pbs_textdata where TextType = 'Rules'" );
  	$rules = mysql_fetch_assoc( $resid );
?>
<table border="0" width="700px" cellpadding="1" cellspacing="2" align="center">
<tr><td><h4>Tournament Rules</h4><br></td></tr>
<? if( $rules ) { ?>
<tr><td><h6>by <? echo $rules["Author"]; ?></h6></td></tr>
<tr>
  <td class="rules_cell"><br>
    <? echo $rules["Body"]; ?>
  </td>
</tr>
<? } else echo "<tr><td>Rules have not been posted yet.</td></tr>\n"; ?>
</table>
<?
  } else
  if( $show == "News" )
  {
  	$resid = db_query(
  		"select Author, Title, Body, unix_timestamp(Created) as Created, unix_timestamp(LastModified) as LastModified, ".
  		"date_format(Created, '%M %D \'%y %l:%i %p') as CreatedF, ".
  		"date_format(LastModified, '%M %D \'%y %l:%i %p') as LastModifiedF ".
  		"from pbs_textdata where TextType = 'News' order by LastModified desc" );
?>
<table class="news" cellspacing="2" align="center" width="700px">
<tr><td><h4>Tournament News</h4><br></td></tr>
<?
	if( ! mysql_num_rows( $resid ) )
		echo "<tr><td>No News have been Posted yet.</td></tr>\n";
	else
  	while( $news = mysql_fetch_assoc( $resid ) )
  	{
  		echo "<tr><td class=\"title\">{$news["Title"]} [posted: {$news["CreatedF"]}]";
  		// only show that the article was modified if it was edited a minute later or more
  		if( $news["LastModified"] - $news["Created"] > 60 )
  			echo " [updated: {$news["LastModifiedF"]}]";
  		echo "</td></tr>\n";
  		echo "<tr><td class=\"author\">by {$news["Author"]}</td></tr>\n";
  		echo "<tr><td class=\"body\">{$news["Body"]}</td></tr>\n";
  		echo "<tr><td class=\"break\">&nbsp;</td></tr>\n";
  	}
?>
</table>
<?
  } else
  if ($show == "Standings")
  {
  	
  	$nSEPlayers = mysql_result (db_query ("select Value from pbs_config where Label = 'SEPlayers'"), 0);
	$nDEPlayers	= mysql_result (db_query ("select Value from pbs_config where Label = 'DEPlayers'"), 0);
	$nMPG		= mysql_result (db_query ("select Value from pbs_config where Label = 'MPG'"), 0);
	$nDERows	= log10 ($nDEPlayers) / log10(2) * 4 + 1;
	$nSERows	= log10 ($nSEPlayers) / log10(2) * 2 + 1;
	$nDBTableOffset = $nSEPlayers * 2;
	$bDrawMenu	= true;
	$bAltView	= true;
	
	if (!$nDEPlayers) $nDERows = 0;  	
  	
  	if ($bDrawMenu)
  	{
?>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td class="td_unsched" width="100">Unscheduled</td>
  <td class="td_played" width="100" align="center">Played</td>
  <td rowspan="2" class="td_unsched" width="200">
    Single Elim <? echo $gameTermP; ?>: <? echo $nSEPlayers; ?><br>
    <? if ($nDEPlayers) { ?> Double Elim <? echo $gameTermP; ?>: <? echo $nDEPlayers; ?><br> <? } ?>
  </td>
</tr>
<tr>
  <td class="td_sched" width="100">Scheduled</td>
  <td class="td_dropin" width="100" align="center">Drop In</td>
</tr>
</table>
<br><br>
<?
  	}
  	$query = "select b.ID, count(r.ID) as Rounds, b.RefID, b.DropInID, if(b.Timestamp > 0, date_format(b.Timestamp, '%b %D %h:%i %p'), 0) as Timestamp, p.Nick, b.PlayerID "
  			."from pbs_bracket b "
  				."left join pbs_players p on (b.PlayerID = p.ID) "
  				."left join pbs_rounds r on (r.BracketID = b.ID and r.MapID) "
  			."group by b.ID order by b.ID";
	$rBracket = db_query ($query);
	if (!mysql_num_rows ($rBracket))
	{
		showHint ("Section Disabled", "bracket has not been seeded with players yet, section disabled.<br>", 200);
		exit;
	}
?>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td><hr noshade style="color: #eee;" size="1" width="120"></td>
  <td width="200px" align="center"><h4>The Winners Bracket</h4></td>
  <td><hr noshade style="color: #eee;" size="1" width="120"></td>
</tr>
</table>
<table class="bracket" align="center">
<?
	$aPlayers = array();
	
	$nColumns = $nSEPlayers * 2 - 1;
	$nRows = $nSERows;
	//$arrLoserToWinner = array();
	if (mysql_num_rows ($rBracket))
		while ($rPlayer = mysql_fetch_assoc ($rBracket))
		{
			$aPlayers[$rPlayer["ID"]] = $rPlayer;
			//$arrLoserToWinner[$rPlayer["DropInID"]] = $rPlayer["ID"];
		}

	//echo "<pre>";
	//print_r ($aPlayers);
	//echo "</pre>";
		
	$arrGraph = array();

	$bColor = array ();
	for ($i = 0; $i < $nSERows; $i ++)
		$bColor []= false;
		
?><tr><?
// CfYz added and modifyed block for maps for each WINNERS round START
//	$winnersmaps = array ("ztn3tourney1","hub3tourney1","q3tourney2","pro-q3dm6","pro-q3tourney4");
	$count = 0;
	for ($i = 0; $i < $nSERows; $i ++)
	{
		echo "<td ";
		if ($i % 2 == 0) {
			echo "width=\"$nWName\"><a><u>round " . ($i / 2 + 1) ."</u></a><br><b><a></a></b><br>&nbsp;";
			}
		else
			echo "width=\"$nWSpace\">&nbsp;";
		echo "</td>";
		$count++;
//		if ($count % (count($winnersmaps) * 2) == 0)
//			$count = 0;
	}
//END
?></tr><?

	$powlookup = array (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768);
	$bRowStep = array (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	$bBridgeFocus = array (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	if (isset ($compact))
		$powlookup[0] = 0;

	$nPlayerID = 0;
	for ($i = 0; $i < $nSEPlayers * 2 - 1; $i ++)
	{
		$m = 0;
		$sub = 0;
		$sub2 = 0;
		for ($j = 0; $j < $nSERows; $j ++)
		{
			$p = $powlookup[$j+1];
			$arrGraph[$i * $nSERows + $j] = 0;
			$bWas = false;
			if ($j % 2 == 0)
			{
				if (($i-$sub) % $powlookup[$j/2+1] == 0) {
					$nPlayerID ++;
					$arrGraph[$i * $nSERows + $j] = 1;
					if ($j > 1 && $aPlayers[$nPlayerID]["PlayerID"])
						$arrGraph[$i * $nSERows + ($j - 2)] = 3;
					else if( $aPlayers[$nPlayerID]["Timestamp"] )
						$arrGraph[$i * $nSERows + ($j - 2)] = 4;
				}
				$sub += $powlookup[$j/2];
			} else
			{
				if (($i-$sub2) % $powlookup[($j-1)/2+1] == 0)
				{
					$bWas = $bColor[$j];
					$bColor[$j] = !$bColor[$j];
				}
				if ($bColor[$j] || $bWas)
					$arrGraph[$i * $nSERows + $j] = 2;
				$sub2 += $powlookup[($j-1)/2];
			}
		}
	}
	$nPlayerID = 0;
	$WBStandingString = "";
	for ($i = 0; $i < $nColumns; $i ++)
	{
		echo "<tr>";
		for ($j = 0; $j < $nRows; $j ++)
		{
			$bHasText	= false;
			$bHasOption	= false;
			$bHasTime	= false;
			$bHasRef	= false;
			$bHasLink	= false;
			$sClass		= "";
			if ($arrGraph[$i * $nRows + $j] == 1)
			{
				$nPlayerID ++;
				$sClass = "td_played";
				$bHasText = true;
				if (!$aPlayers[$nPlayerID]["PlayerID"])
				{
					if (!$aPlayers[$nPlayerID]["Timestamp"])
						$sClass = "td_unsched";
					else
						$sClass = "td_sched";
						
					if ($aPlayers[$nPlayerID]["RefID"] > 0)
						$bHasRef = true;
						
					if ($bHasRef && $bHasTime)
						$sClass = "td_sched";
				}
			} else
			if ($arrGraph[$i * $nRows + $j] == 2)
			{
				if ($sClass != "td_unsched")
					$sClass = "td_br";
				
			} else
			if ($arrGraph[$i * $nRows + $j] == 3)
			{
				$bHasLink = true;
			} else
			if( $arrGraph[$i * $nRows + $j] == 4 )
			{
				$bHasTime = true;
			}
			
			$rowSpan = 0;
			$bDrawTD = false;
			if ($arrGraph[$i * $nRows + $j] == 2)
			{
				if ($bRowStep[($j+1)/2] == 0)
				{	# top bridge corner
					$bRowStep[($j+1)/2] = 1;
					$rowSpan = 1;
					$bDrawTD = true;
					$nrcoff = $j == 1 ? 1 : $j-1; # next row center offset from top bridge corner
					if( $arrGraph[($i+$nrcoff) * $nRows + ($j-1)] == 4 ) {
						$sClass = "td_br_es";
						$bBridgeFocus[($j+1)/2] = 2;
					} else{
						$bBridgeFocus[($j+1)/2] = $arrGraph[($i+$nrcoff) * $nRows + ($j-1)] == 3 ? 1 : 0;
						$sClass = $bBridgeFocus[($j+1)/2] ? "td_br_e_f" : "td_br_e_b";
					}
					
				} else
				if ($bRowStep[($j+1)/2] == 1)
				{	# center of bridge
					$bRowStep[($j+1)/2] = -($powlookup[(($j-1)/2)+1]-1);
					$rowSpan = ($powlookup[(($j-1)/2)+1]-1);
					$bDrawTD = true;
					# is the center focused or not?
					$sClass = $bBridgeFocus[($j+1)/2] ? "td_br_c_f" : "td_br_c_b";
					$sClass = $bBridgeFocus[($j+1)/2] == 2 ? "td_br_cs" : $sClass;
				} else
				if ($bRowStep[($j+1)/2] == -1)
				{	# bottom bridge corner
					$bRowStep[($j+1)/2] = 0;
					$rowSpan = 1;
					$bDrawTD = true;
					$sClass = $bBridgeFocus[($j+1)/2] ? "td_br_e_f" : "td_br_e_b";
					$sClass = $bBridgeFocus[($j+1)/2] == 2 ? "td_br_es" : $sClass;
					$bBridgeFocus[($j+1)/2] = 0;
				} else
					$bRowStep[($j+1)/2] += 1;
				
				if ($bDrawTD)
				{
					echo "<td class=\"$sClass\" rowspan=\"$rowSpan\">";
				}
			} else if ($bHasLink && $bAltView)
			{
				echo "<td class=\"td_view\">";
				if ($aPlayers[$nPlayerID+1]["Rounds"])
					echo "&uarr;[<a href=\"?show=match&matchid=".($nPlayerID+1)."\">Match Details</a>]&darr;";
				else echo "$noDataSign";
			} elseif( $bHasTime )
			{
				echo "<td class=\"td_view\">";
				echo "Scheduled For &rarr;";
			} else
			{
				echo "<td";
				if ($sClass != "") echo " class=\"$sClass\"";
				echo ">";
				$bDrawTD = true;
			}
			
			
			if ($bDrawTD)
			if ($bHasText)
			{
				if ($aPlayers[$nPlayerID]["PlayerID"])
				{
					if ($j && !$bAltView && $aPlayers[$nPlayerID]["Rounds"])
					{
						echo "<ul>";
	    					echo "<li id=\"l\">";
	    					echo "(" . $aPlayers[$nPlayerID]["PlayerID"] . ") <a href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\" title=\"{$aPlayers[$nPlayerID]["Nick"]}\">" . ShortenString( $aPlayers[$nPlayerID]["Nick"], 20 ) . "</a>";
	    					echo "</li>";			
	      					echo "<li id=\"r\">[<a href=\"?show=match&matchid=$nPlayerID\">View</a>]</li>";
						echo "</ul>";

					} else echo "(" . $aPlayers[$nPlayerID]["PlayerID"] . ") <a href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\" title=\"{$aPlayers[$nPlayerID]["Nick"]}\">" . ShortenString( $aPlayers[$nPlayerID]["Nick"], 20 ) . "</a>";
				} else
				{
					if ($aPlayers[$nPlayerID]["Timestamp"])
						echo $aPlayers[$nPlayerID]["Timestamp"];
					else
						echo "&nbsp;";
				}
			} else echo "&nbsp;";
			
			// place anchor
			echo "<a name=\"match$nPlayerID\"></a>";
			
			echo "</td>";
		} // for (j)
		echo "</tr>";
	}// for (i)
?>
</table>
<br>
<?
	if ($nDEPlayers)
	{
		list ($arrGraph) = buildDoubleElim2Small ($nDEPlayers);	
		$nColumns = $nDEPlayers * 2 + 1;
		$nRows = $nDERows;
		$abCon = array ();
		$abWas = array ();
	
		for ($i = 0; $i < $nRows; $i ++) {
			$abCon[$i] = false;
			$abWas[$i] = false;
		}
	
		$aDoubleElimID = array();
	
		$nPlayerID = $nDBTableOffset;
		$arrIDtoPlayerID = array();
		for ($i = 0; $i < $nColumns; $i ++)
		{
			for ($j = 0; $j < $nRows; $j ++)
			{
				$id = $j * $nColumns + $i;
				
				if ($arrGraph[$id] == 1) {	// normal winner
					$nPlayerID ++;
					$arrIDtoPlayerID[$id] = $nPlayerID;
				} else if ($arrGraph[$id] == 2) {	// drop in from loser bracket
					$nPlayerID ++;
					$arrIDtoPlayerID[$id] = $nPlayerID;
				}
			}
		}
		
		$nPlayerID = $nDBTableOffset;
		$LBStandingString = "";
		//$nTableWidth = ($nWName + $nWSpace + 2) * $nDERows/2 - $nWSpace;
		$bRowStep = array (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
?>
<br>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td><hr noshade size="1" width="120"></td>
  <td width="360px" align="center"><h4>The Losers Bracket: Losers vs New Losers</h4></td>
  <td><hr noshade size="1" width="120"></td>
</tr>
</table>
<table class="bracket" align="center" border="0">
<?
	echo "<tr>";
// CfYz added and modifyed block for maps for each LOSERS round START
//	$losersmaps = array ("pro-q3tourney7","pro-q3dm6","q3tourney2","pro-q3tourney4","ztn3tourney1","hub3tourney1","cpm1a");
	$count = 0;
	for ($i = 0; $i < $nDERows; $i ++)
	{
		echo "<td ";
		if ($i % 2 == 0)
			echo "width=\"$nWName\" align=\"center\"><a><u>round " . (($i % 4) ? "W" : "L") . ($i / 2 + 1) . "</a></u><br><b><a></a></b><br>&nbsp;";
		else
			echo "width=\"$nWSpace\">&nbsp;";
		echo "</td>";
		$count ++;
//		if ($count % (count($losersmaps) * 2) == 0)
//			$count = 0;
//END
	}
	echo "</tr>";
	for ($i = 0; $i < $nColumns; $i ++)
	{
		echo "<tr>";
		for ($j = 0; $j < $nRows; $j ++)
		{
			$id = $j * $nColumns + $i;

			$bHasText	= false;
			$bHasTime	= false;
			$bHasRef	= false;
			$bHasLink	= false;
			$bIsTBA		= false;
			$sClass		= "";
			$bWas[$j]	= $abCon[$j];
			
			if ($aPlayers[$nPlayerID]["Timestamp"])
				$Nick = $aPlayers[$nPlayerID]["Timestamp"];
			else
				$Nick = "Not Scheduled";
			
			if ($arrGraph[$id] == 1) {	// normal winner
				$nPlayerID ++;
				$bHasText = true;		
				$abCon[$j] = !$abCon[$j];
				$sClass = "td_played";
				if ($aPlayers[$nPlayerID]["PlayerID"])
				{
					$Nick = "(".$aPlayers[$nPlayerID]["PlayerID"].") <a title=\"{$aPlayers[$nPlayerID]["Nick"]}\" href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\">". ShortenString( $aPlayers[$nPlayerID]["Nick"], 20 ) . "</a>";
				} else
				{
					if (!$aPlayers[$nPlayerID]["Timestamp"])
						$sClass = "td_unsched";
					else
						$sClass = "td_sched";
						
					if ($aPlayers[$nPlayerID]["Timestamp"])
						$bHasTime = true;
					else
						$bHasTime = false;
						
					if ($aPlayers[$nPlayerID]["RefID"] > 0)
						$bHasRef = true;
						
					if ($bHasRef && $bHasTime)
						$sClass = "td_sched";
				}				
				if ($j == $nRows - 1) $nSpotIDLoser = $nPlayerID;
			} else if ($arrGraph[$id] == 2) {	// drop in from loser bracket
				$nPlayerID ++;
				$sClass = "td_dropin";
				$bHasText = true;
				$abCon[$j] = !$abCon[$j];
				
				if ($aPlayers[$nPlayerID]["PlayerID"]) {
					$Nick = "(".$aPlayers[$nPlayerID]["PlayerID"].") <a title=\"{$aPlayers[$nPlayerID]["Nick"]}\" href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\">". ShortenString( $aPlayers[$nPlayerID]["Nick"], 20 ) . "</a>";
				} else
				{
					if ($aPlayers[$nPlayerID]["Timestamp"])
						$bHasTime = true;
					else
						$bHasTime = false;
						
					if ($aPlayers[$nPlayerID]["RefID"] > 0)
						$bHasRef = true;
						
					if ($bHasRef && $bHasTime)
						$sClass = "td_sched";
				}
			} else if ($arrGraph[$id] == 3) {	// altview thing
				$bHasLink = true;
				if( $bAltView && $bHasLink && $aPlayers[$nPlayerID + 1]["PlayerID"] )
					$sClass = "td_view";
				elseif( $bAltView && $bHasLink && $aPlayers[$nPlayerID + 1]["Timestamp"] )
					$sClass = "td_view";
			}

			if ($j % 2 == 1) {
				if ($bWas[$j - 1] || $abCon[$j - 1]) {
					$sClass = "td_br";
					$bWas[$j - 1] = false;			
				}
			}
			
			
			$rowSpan = 0;
			$bDrawTD = false;

			echo "<td";
			if ($sClass != "") echo " class=\"$sClass\"";
			//if ($bAltView && $bHasLink) echo " align=\"right\"";
			echo ">";
			$bDrawTD = true;
			
			
			if ($bDrawTD)
			if ($bHasText)
			{
				if ($aPlayers[$nPlayerID]["PlayerID"])
				{
					if (!$bIsTBA)
					{
       					if (!$bAltView)
       					{
       						echo "<ul>";
    						echo "<li id=\"l\">$Nick</li>";
      						echo "<li id=\"r\">";
       						if ($aPlayers[$nPlayerID]["Rounds"])
       						{
	       						echo "[<a href=\"bracket_view.php?show=match&matchid=$nPlayerID\">View</a>]";
       						} else echo $noDataSign;
       						echo "</li></ul>";
       					} else echo $Nick;
					}
				} else
				{
					if ($bHasTime)
						echo $aPlayers[$nPlayerID]["Timestamp"];	
					else
					if ($bIsTBA)
						echo "Not Scheduled";
					else
						echo "&nbsp;";
				}
			} else
			if ($bAltView && $bHasLink && $aPlayers[$nPlayerID + 1]["PlayerID"])
			{
				if ($aPlayers[$nPlayerID + 1]["Rounds"])
					echo "&uarr;[<a href=\"?show=match&matchid=".($nPlayerID+1)."\">Match Details</a>]&darr;";
				else echo $noDataSign;
			} elseif( $bHasLink &&  $aPlayers[$nPlayerID + 1]["Timestamp"] )
			{
				echo "Scheduled For &rarr;";
			} else echo "&nbsp;";
  			echo "</td>";
		} // for j
		echo "</tr>";
	} // for i
?>
</table>
<?
	} // if (nDEPlayers)
	
	$nSpotIDWinner = $nSEPlayers;
	$nSpotIDFinal = $nPlayerID + 1;
	if (isset ($nSpotIDLoser))
	if ($aPlayers[$nSpotIDWinner]["PlayerID"] && $aPlayers[$nSpotIDLoser]["PlayerID"])
	{
		//$nTableWidth = ($nWName + $nWSpace + 2) - $nWSpace;

		// im usin loser bracket handling to add final winner to winner bracket
		// in order to avoid creating a loser bracket entry (im lazy)
		$nPlayerID0 = $aPlayers[$nSpotIDWinner]["PlayerID"];
		$nPlayerID1 = $aPlayers[$nSpotIDLoser]["PlayerID"];
		$sNick0		= "<a title=\"{$aPlayers[$nSpotIDWinner]["Nick"]}\" href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\">" . ShortenString( $aPlayers[$nSpotIDWinner]["Nick"], 20 ) . "</a>";
		$sNick1		= "<a title=\"{$aPlayers[$nSpotIDLoser]["Nick"]}\" href=\"?show=".strtolower($gameTermP)."&id=$nPlayerID\">" . ShortenString( $aPlayers[$nSpotIDLoser]["Nick"], 20 ) . "</a>";
		
		$nWinnerID	= $nSpotIDFinal;
		
		$bHasTime = ($aPlayers[$nWinnerID]["Timestamp"]) ? true : false;
		$bHasRef = ($aPlayers[$nWinnerID]["RefID"]) ? true : false;
		
		if ($aPlayers[$nWinnerID]["PlayerID"])
			$class = "td_played";
		else if ($bHasTime)
			$class = "td_sched";
		else
			$class = "td_unsched";
	
		if( $aPlayers[$nWinnerID]["PlayerID"] )
		{ // played
			$br_e ="td_br_e_f";
			$br_c ="td_br_c_f";
		} elseif( $aPlayers[$nWinnerID]["Timestamp"] )
		{ // scheduled
			$br_e ="td_br_es";
			$br_c ="td_br_cs";
		} else
		{ // unscheduled
			$br_e ="td_br_e_b";
			$br_c ="td_br_c_b";
		}
		
?>
<br><br>
<table border="0" cellpadding="1" cellspacing="1" width="" align="center">
<tr>
  <td><hr noshade size="1" width="120"></td>
  <td><h4>Final Match: Winner Bracket vs Loser Bracket</h4></td>
  <td><hr noshade size="1" width="120"></td>
</tr>
</table>
<br>
<table cellpadding="1" cellspacing="1" align="center" border="0" width="<? echo ($nWSpace+$nWName*2) ?>">
<!-- START MAPS FOR GF CfYz-->
<!-- END MAPS FOR GF -->
<tr>
  <td width="<? echo $nWName; ?>" class="td_played">
  <?
		echo "($nPlayerID0) " . $sNick0;
  ?>
  </td>
  <td width="<? echo $nWSpace; ?>" class="<? echo $br_e; ?>">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>">&nbsp;
  </td>
</tr>
<tr>
  <td width="<? echo $nWName; ?>" class="td_view" align="right">
<?
  	if ($bAltView && $aPlayers[$nWinnerID]["PlayerID"])
  	{
  		if ($aPlayers[$nWinnerID]["Rounds"])
  		{
  			echo "&uarr;[<a href=\"?show=match&matchid=$nWinnerID\">Match Details</a>]&darr;";
  		} else echo $noDataSign;
  	} else echo "&nbsp;";
?>
  </td>
  <td width="<? echo $nWSpace; ?>" class="<? echo $br_c; ?>">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>" class="<? echo $class; ?>">
  <?
	if ($aPlayers[$nWinnerID]["PlayerID"])
	{
?>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td class="td_played_text"><? echo "(".$aPlayers[$nWinnerID]["PlayerID"].") <a href=\"?show=".strtolower($gameTermP)."&id=$nWinnerID\" title=\"{$aPlayers[$nWinnerID]["Nick"]}\">" . $aPlayers[$nWinnerID]["Nick"] . "</a>"; ?></td>
      <td align="right">
        <? if (!$bAltView) { ?>
        [<a href="bracket_view.php?show=match&matchid=<? echo $nWinnerID; ?>" title="View Match Info">View</a>]
        <? } ?>
      </td>
    </tr>
    </table>
<?
	} else
	{
		if ($aPlayers[$nWinnerID]["Timestamp"])
			echo $aPlayers[$nWinnerID]["Timestamp"];
		else
			echo "Not Scheduled";
	}
  ?>
  </td>
</tr>
<tr>
  <td width="<? echo $nWName; ?>" class="td_played">
  <?
		echo "($nPlayerID1) " . $sNick1;
  ?>
  </td>
  <td width="<? echo $nWSpace; ?>" class="<? echo $br_e; ?>">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>">&nbsp;
  </td>
</tr>
</table>
<br><br>
<?
		if (0)
		{
?>
<div align="center">
Putty's Bracket System<small><sup>TM</sup></small><br>
&copy; 2002-2003 chk-putty<br>
All rights reserved.<br>
</div>
<?
		}
	}
  } // end of if (show = standing)
  else if ($show == "The Seeds" || strtolower($show) == "players" || strtolower($show) == "teams") {
	$result = db_query ("select ID, Nick from pbs_players order by ID");
?>
<br><br>
<table border="0" cellpadding="0" cellspacing="0" align="left" width="800px">
<tr>
  <td colspan="2" width="250px" align="center"><h4><? echo $gameTermS; ?> Matches Review System</h4></td>
  <td colspan="1" width="550px"></td>
</tr>
<tr>
  <td valign="top" align="center">
  <hr noshade size="1">
	<table border="0" cellpadding="1" cellspacing="2">
	<tr>
	  <td class="td_seed_header">Seed</td>
	  <td class="td_seed_header" width="150px">Team Name</td>
	  <td class="td_seed_header" width="50px">History</td>
	</tr>
	<?
		while ($arrPlayer = mysql_fetch_assoc ($result))
		{
	?>
	<tr>
	  <td class="td_seed_entry"><? echo $arrPlayer["ID"]; ?></td>
	  <td class="td_seed_entry"><? echo "<a title=\"{$arrPlayer["Nick"]}\">" . ShortenString( $arrPlayer["Nick"], 20 ) . "</a>"; ?></td>
	  <td class="td_seed_entry">[<a href="?show=<? echo strtolower($gameTermP)."&amp;id"; ?>=<? echo $arrPlayer["ID"]; ?>" title="Access Match Records">Access</a>]</td>
	</tr>
	<?
		}
	?>
	</table>
  <hr noshade size="1">
</td>
<td width="20px" valign="top"><hr noshade size="1" width="100%"></td>
<td valign="top">
<?
	if (isset ($id))
	{    
		$query = "select MapName, r.BracketID, p1.ID as WinID, p1.Nick as WinNick, p2.ID as LoseID, p2.Nick as LoseNick, WinnerScore, LoserScore, SequenceNum, Screenshot, DemoID, DemoURL "
				."from pbs_rounds r, pbs_players p1, pbs_players p2, pbs_mappool m "
				."where (p1.ID = r.WinnerPID) and (p2.ID = r.LoserPID) and (r.WinnerPID = $id or r.LoserPID = $id) and (m.ID = MapID or MapID = 0) "
				."group by r.BracketID, r.SequenceNum";
		$result = db_query ($query);
		$arrMatches = array();
		while ($round = mysql_fetch_assoc ($result)) {
			
			if (!isset ($arrMatches[$round["BracketID"]]) || !is_array ($arrMatches[$round["BracketID"]]))
				$arrMatches[$round["BracketID"]] = array();
			$arrMatches[$round["BracketID"]][] = $round;
		}
		// get win/lose record
		$nWins = mysql_num_rows (db_query ("select ID from pbs_rounds where WinnerPID = $id group by BracketID"));
		$nLoses = mysql_num_rows (db_query ("select ID from pbs_rounds where LoserPID = $id group by BracketID"));

        $gm = getGameMode();
		if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
		{	// get the team members
		    $rTeams = db_query ("select Name, Captain from pbs_teamdata where TeamID = $id order by Name");
		    if (mysql_error()) showHint ("Error Fetching Team Player List", mysql_error());
		    // get the team name
			$rTeamname = db_query ("select Nick, Location from pbs_players where ID = $id");
			if (mysql_error()) showHint ("Error Fetching Team Name", mysql_error());
			$res = mysql_fetch_assoc ($rTeamname);
			$teamName = $res["Nick"];
			$teamChan = $res["Location"];
		} else {
			$rTeamname = db_query ("select Nick from pbs_players where ID = $id");
			if (mysql_error()) showHint ("Error Fetching Player Name", mysql_error());
			$res = mysql_fetch_assoc ($rTeamname);
			$teamName = $res["Nick"];
		}
		
		if (!isset ($perRow) || ($perRow < 1 || $perRow > 10))
			$perRow = 1;
		//echo "<pre>";
		//print_r ($arrMatches);
		//echo "</pre>";
?>
<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
<tr>
  <td align="right"><hr noshade size="1" width="100%"></td>
  <td align="center" colspan="2" class="td_sched">
    Accessing <? echo "records on $teamName.."; ?>
  </td>
</tr>
<tr>
  <td colspan="3">&nbsp;</td>
</tr>
<tr>
<?
    $gm = getGameMode();
    if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
	{
?>
  <td width="150" valign="top" align="center">
<?
	  if (mysql_num_rows ($rTeams))
	  {
?>
    <table cellspacing="1" style="members_table" align="center">
    <tr>
	  <td colspan="<? echo $perRow; ?>" class="td_played">
	    Members
	  </td>
	</tr>
<?
		while ($teamEntry = mysql_fetch_assoc ($rTeams))
		{
			echo "<tr><td class=\"td_unsched\">" . $teamEntry["Name"];
		   	if ($teamEntry["Captain"] == 'Y')
		    	echo " <a class=\"red\">*</a>";		    	
		    echo "</td>";
		}
?>
    <tr><td>&nbsp;</td></tr>
    <tr><td class="td_normal"><a class="red">*</a> = team captain</td></tr>
    </table>
<?
	  } else echo "<br><br><br>Roster N/A.";
?>
  </td>
<?
    }
    getGameMode();
?>
  <td valign="top" align="center" colspan="<? echo ($gameMode != "1v1") ? 2 : 3; ?>">
<?
		if (mysql_num_rows ($result))
		{
?>
	<table border="0" cellpadding="1" cellspacing="1" align="center">
	
	<?
		$numCol = 0;
		foreach ($arrMatches as $arrMatch)
		{
			// get info from first round
			$WinInfo = "(".$arrMatch[0]["WinID"].") <a title=\"{$arrMatch[0]["WinNick"]}\">" . ShortenString( $arrMatch[0]["WinNick"], $LONGEST_NAME_IN_MATCH_REVIEWS ) . "</a>";
			$LoseInfo = "<a title=\"{$arrMatch[0]["LoseNick"]}\">".ShortenString( $arrMatch[0]["LoseNick"], $LONGEST_NAME_IN_MATCH_REVIEWS ) . "</a> (".$arrMatch[0]["LoseID"].")";
			$numCol ++;
			
			// lookahead for all scores
			$runningSum = 0;
			foreach ($arrMatch as $round) {
				$runningSum += $round["WinnerScore"];
			}
			// if winner's score for all the rounds is 0, no info must of been submitted!
			if (!$runningSum)
			{	// cant use width in no info row cause of cellspacin :<
?>
	<tr>
	  <td>
	    <table border="0" cellpadding="0" align="center" cellspacing="1" class="match_list">
		<tr>
		  <td class="header" colspan="3" width="50%"><? echo $WinInfo; ?></td>
		  <td class="header" colspan="3"><? echo $LoseInfo; ?></td>
		</tr>
		<tr align="center">
          <td class="entry" colspan="6">No Info!</td>
        </tr>
        <tr align="center">
		  <td colspan="1" width="100">&nbsp;</td>
		  <td colspan="1" width="20">&nbsp;</td>
		  <td colspan="2" width="50">&nbsp;</td>
          <td colspan="1" width="20">&nbsp;</td>
          <td colspan="1" width="100">&nbsp;</td>
        </tr>
		</table>
	  </td>
	</tr>
<?
				continue;	
			}
	?>
	<tr>
	  <td>
	    <table border="0" cellpadding="0" cellspacing="1" class="match_list" align="center">
		<tr>
		  <td class="header" colspan="2"><? echo $WinInfo; ?></td>
		  <td colspan="2" align="center" class="header">[<a href="?show=match&matchid=<? echo $arrMatch[0]["BracketID"]; ?>">View</a>]</td>
		  <td class="header" colspan="2"><? echo $LoseInfo; ?></td>
		</tr>
		  <? showRounds($arrMatch); ?>
		</table>
	  </td>
	</tr>
	<?
			if ($numCol >= $perRow)
			{
				$numCol = 0;
				echo "</tr><tr>\n";
			}
		} // end of foreach (matches)
		
		if ($numCol < $perRow)
			echo "<td>&nbsp;</td>\n";	// just for sanity
	?>
	
	</table>
  </td>
</tr>
<?
	if (isset ($teamChan) && strlen ($teamChan))
	{
?>
<tr>
  <td>&nbsp;</td>
  <td colspan="2" align="center">You can find this team at: <a class="headerText"><? echo $teamChan; ?></a></td>
</tr>
<?
	}
?>
</table>
<?
		} // if mysql_num_rows(result)
		else
		{
			echo "<br><br><br>No Match Data Available.<br>";
			if (isset ($teamChan))
			{
				 if (strlen ($teamChan))
					echo "<br><br>You can find this team at: <a class=\"headerText1\">$teamChan</a><br>";
				else
					echo "<br><br>No team irc-channel specified.<br>";
			}
		}
	}
?>
  </td>
</tr>
</table>

<?
	} else
	if ($show == "match")
	{
		$ssid = (isset ($ssid)) ? $ssid : 0;
		if (isset ($matchid))
			showMatchDetail ($matchid, $ssid);
	} else
	if ($show == "MapList")
	{
		$query = "select m.ID, m.MapName, m.URL, m.Description, m.MapImage, count(r.ID) as numRounds, "
				."max(abs(WinnerScore - LoserScore)) as maxScoreDiff, "
				."min(abs(WinnerScore - LoserScore)) as minScoreDiff "
				."from pbs_mappool m left join pbs_rounds r on m.ID = r.MapID "
				."group by m.ID";
		$rMapInfo = db_query ($query);
		
		// get the most popular map, only displays the most popular map if there is ONE such map, during a tie nothing is shown
		$query	= "select MapID, count(MapID) as numPlayed from pbs_rounds where MapID group by MapID order by numPlayed desc";
		$rMPMap	= db_query ($query);
		
		$MPMap	= array ("MapID" => 0, "numPlayed" => 0);
		$MPMap1	= array ("MapID" => 0, "numPlayed" => 0);
		$MPMap2	= array ("MapID" => 0, "numPlayed" => 0);
		
		if (mysql_num_rows ($rMPMap) > 1)
		{	// take top 2 and see if first one is bigger, if not its a tie (cant have second bigger than first :)
			$MPMap1 = mysql_fetch_assoc ($rMPMap);
			$MPMap2 = mysql_fetch_assoc ($rMPMap);
			if ($MPMap1["numPlayed"] > $MPMap2["numPlayed"])
				$MPMap = $MPMap1;
		} else
			$MPMap = mysql_fetch_assoc ($rMPMap);
		
		// get the numbre of times the maps were picked as first choice
		$query = "select MapID, count(MapID) as numPlayed from pbs_rounds where SequenceNum = 1 and MapID group by MapID order by numPlayed desc";
		$rFirstChoice = db_query ($query);
		$arr1stChoice = array();
		while( $row = mysql_fetch_assoc( $rFirstChoice ) )
			$arr1stChoice[$row["MapID"]]= $row["numPlayed"];
			
		// most common first choice
		list( $MCFC["MapID"], $MCFC["numPlayed"] ) = each( $arr1stChoice );
		
		$query = "select BracketID, MapID, abs(WinnerScore - LoserScore) as minScoreDiff, WinnerScore, LoserScore "
			."from pbs_rounds where MapID group by minScoreDiff limit 1";
		$result = db_query ($query);
		$ClosestGame = mysql_fetch_assoc ($result);
		
		$query = "select BracketID, MapID, abs(WinnerScore - LoserScore) as maxScoreDiff, WinnerScore, LoserScore "
			."from pbs_rounds where MapID group by maxScoreDiff desc limit 1";
		$result = db_query ($query);
		$FurthestGame = mysql_fetch_assoc ($result);
?>
<br>
<table border="0" cellpadding="2" cellspacing="2" align="center">
<tr><td class="td_sched">Award</td><td class="td_sched">Characteristic</td></tr>
<tr><td class="td_sched">MCFC</td><td class="td_played" style="text-align: left">Most Common First Choice</td></tr>
<tr><td class="td_sched">MPM</td><td class="td_played" style="text-align: left">Most Played Map (not shown during a tie)</td></tr>
<tr><td class="td_sched">CG</td><td class="td_played" style="text-align: left">Closest Game on a Map</td></tr>
<tr><td class="td_sched">BG</td><td class="td_played" style="text-align: left">Biggest Blowout Game on a Map</td></tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" align="center" width="300">
<?
		while ($arrMap = mysql_fetch_assoc ($rMapInfo))
		{
			if (!file_exists("map_pics/".$arrMap["MapImage"]))
				$arrMap["MapImage"] = "none.jpg";
?>
<tr>
  <td colspan="2" class="td_dropin">
<?			echo $arrMap["MapName"];
			if (strlen ($arrMap["URL"]))
					echo " [<a href=\"$arrMap[URL]\">link</a>]";
?>
  </td>
</tr>
<tr>
  <td align="center"><img src="map_pics/<? echo $arrMap["MapImage"]; ?>" border="0" class="map_image"></td>
  <td class="map_statistics">
    <table border="0" cellpadding="0" cellspacing="0" width="160" height="100">
	<tr>
	  <td valign="top">
<?
			$times = ($arrMap["numRounds"] == 1) ? "time" : "times";	// aha i know english
			echo "Map Played: ".$arrMap["numRounds"]." $times<br>\n";
			if( ! isset( $arr1stChoice[$arrMap["ID"]] ) )
				$arr1stChoice[$arrMap["ID"]] = 0;
			$times = ($arr1stChoice[$arrMap["ID"]] == 1) ? "time" : "times";
			echo "First Choice: ".$arr1stChoice[$arrMap["ID"]]." $times<br>\n";
			if ($arrMap["maxScoreDiff"])
				echo "Biggest Score Diff: ".$arrMap["maxScoreDiff"]."<br>\n";
			if ($arrMap["minScoreDiff"])
				echo "Smallest Score Diff: ".$arrMap["minScoreDiff"]."<br>\n";
?>
	  </td>
	</tr>
	<tr>
	  <td valign="bottom">
<?
			if ($MCFC["MapID"] == $arrMap["ID"])
				echo "<a class=\"red\">*</a> MCFC (".$MCFC["numPlayed"].")<br>\n";
			if ($MPMap["MapID"] == $arrMap["ID"])
				echo "<a class=\"red\">*</a> MPM<br>\n";
			if ($ClosestGame["MapID"] == $arrMap["ID"])
				echo "<a class=\"red\">*</a> CG (<a href=\"?show=match&matchid=".$ClosestGame["BracketID"]."\">".$ClosestGame["WinnerScore"]." vs ".$ClosestGame["LoserScore"]."</a>)<br>\n";
			if ($FurthestGame["MapID"] == $arrMap["ID"])
				echo "<a class=\"red\">*</a> BG (<a href=\"?show=match&matchid=".$FurthestGame["BracketID"]."\">".$FurthestGame["WinnerScore"]." vs ".$FurthestGame["LoserScore"]."</a>)<br>\n";
?>
	  </td>
	</tr>
	</table>
  </td>
</tr>
<tr>
  <td colspan="2" class="map_description">
<?
			echo $arrMap["Description"];
?>
  </td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<?
		}
?>
</table>
<?
	} else
	if ($show == "MapInfo")
	{
		if (!isset ($MapID)) $MapID = 1;
		//$sort = explode (";", $sort);
		$sortString = (isset ($sort)) ? " order by $sort" : "";
		$sortDirection = (isset ($dir)) ? " desc" : "";
		$query = "select p1.Nick as p1Nick, p2.Nick as p2Nick, WinnerScore as p1Score, LoserScore as p2Score, MapName, r.BracketID as MatchID "
				."from pbs_rounds r left join pbs_players p1 on (p1.ID = r.WinnerPID) "
								  ."left join pbs_players p2 on (p2.ID = r.LoserPID) "
								  ."left join pbs_mappool m on (m.ID = r.MapID) "
				."where MapID = $MapID";
		$query .= $sortString;
		$query .= $sortDirection;
		
		$curSort = (isset ($sort)) ? $sort : "";
		
		$rMapHistory = db_query ($query);
		if (mysql_error()) showHint("MapInfo: Server Query Error", mysql_error());
		
		$MapName = mysql_ ($rMapHistory,0, "MapName");
?>
<table border="0" cellpadding="1" cellspacing="2" align="center">
<tr>
  <td colspan="5" style="color: #C1D5F5" align="center">Match History for <? echo $MapName; ?></td>
</tr>
<tr>
  <td class="td_unsched"><a href="bracket_view.php?show=MapInfo&MapID=<? echo $MapID; ?>&sort=p1Nick<? if ($curSort == "p1Nick" && !isset ($dir)) echo "&dir=desc"; ?>"><? echo $gameTermS; ?> 1</a></td>
  <td class="td_unsched"><a href="bracket_view.php?show=MapInfo&MapID=<? echo $MapID; ?>&sort=p1Score<? if ($curSort == "p1Score" && !isset ($dir)) echo "&dir=desc"; ?>"><? echo $gameTermS; ?> 1 Score</a></td>
  <td class="td_unsched">Map Name</td>
  <td class="td_unsched"><a href="bracket_view.php?show=MapInfo&MapID=<? echo $MapID; ?>&sort=p2Score<? if ($curSort == "p2Score" && !isset ($dir)) echo "&dir=desc"; ?>"><? echo $gameTermS; ?> 2 Score</a></td>
  <td class="td_unsched"><a href="bracket_view.php?show=MapInfo&MapID=<? echo $MapID; ?>&sort=p2Nick<? if ($curSort == "p2Nick" && !isset ($dir)) echo "&dir=desc"; ?>"><? echo $gameTermS; ?> 2</a></td>
</tr>
<?
		while ($MapRound = mysql_fetch_assoc ($rMapHistory))
		{
			// FIX: for compatability with pre-0.9.0 data padding
			if (!$MapRound["p1Score"] && !$MapRound["p2Score"]) continue;
?>
<tr>
  <td class="td_normal"><? echo $MapRound["p1Nick"]; ?></td>
  <td class="td_normal"><? echo $MapRound["p1Score"]; ?></td>
  <td class="td_normal"><a href="bracket_view.php?show=match&matchid=<? echo $MapRound["MatchID"]; ?>">View</a></td>
  <td class="td_normal"><? echo $MapRound["p2Score"]; ?></td>
  <td class="td_normal"><? echo $MapRound["p2Nick"]; ?></td>
</tr>
<?
		}
?>
</table>
<?
	} else
	if ($show == "History")
	{
		$query = "select p1.Nick as p1Nick, p2.Nick as p2Nick, WinnerScore, LoserScore, MapName, BracketID, p1.ID as p1ID, p2.ID as p2ID, b.Timestamp "
				."from pbs_rounds r left join pbs_players p1 on (p1.ID = r.WinnerPID) "
								  ."left join pbs_players p2 on (p2.ID = r.LoserPID) "
								  ."left join pbs_mappool m on (m.ID = r.MapID) "
								  ."left join pbs_bracket b on (r.BracketID = b.ID) where MapID group by b.Timestamp desc, BracketID, SequenceNum";
		$rRounds = db_query ($query);
		if (mysql_error()) showHint("History: Server Query Error", mysql_error());
?>
<br>
<table border="0" cellpadding="2" cellspacing="0" align="center" class="match_list">
<tr>
  <td colspan="9" class="headerText2" align="center">Known Match History</td>
</tr>
<tr align="center">
  <td class="" width="110"><a><? echo $gameTermS; ?> 1</a></td>
  <td>&nbsp;</td>
  <td class="" width="110"><a><? echo $gameTermS; ?> 2</a></td>
  <td width="30">&nbsp;</td>
  <td class="" colspan="3" width="110"><a>Result</a></td>
  <td>&nbsp;</td>
  <td class=""><a>Map Name</a></td>
</tr>
<?
		$lastIDs = 0;
		$curIDs = 1;
		while ($round = mysql_fetch_assoc ($rRounds))
		{
			$curIDs = $round["p1ID"] . "-" . $round["p2ID"];
			$matchInfo = ($lastIDs != $curIDs) ? true : false;
			
			if ($matchInfo)
			{
?>
<tr>
  <td colspan="9" class="light_header" align="center">
    <a href="?show=match&matchid=<? echo $round["BracketID"]; ?>"><? echo $round["p1Nick"]; ?> vs <? echo $round["p2Nick"]; ?></a>
  </td>
</tr>
<?
				//$className = ($altStyle) ? "td_normal" : "td_normal";
			}

			echo "<tr align=\"center\">"
				."<td class=\"entry\">{$round["p1Nick"]}</td>"
				."<td class=\"\">vs</td>"
				."<td class=\"entry\">{$round["p2Nick"]}</td>"
				."<td class=\"\">:</td>"
				."<td class=\"entry\">{$round["WinnerScore"]}</td>"
				."<td class=\"\">to</td>"
				."<td class=\"entry\">{$round["LoserScore"]}</td>"
				."<td class=\"\">on</td>"
				."<td class=\"entry\">{$round["MapName"]}</td>"
				."</tr>\n";
				
			echo "<tr><td colspan=\"5\" class=\"break\"></td></tr>";
			
			$lastIDs = $curIDs;			
		}
?>
</table>
<?
	} else
	if ($show == "Upcoming")
	{
		$query = "select date_format(Timestamp, '%m/%d/%Y @ %H:%i:%s') as Timestamp, Opponent1, Opponent2, RefName, p1.Nick as p1Name, p2.Nick as p2Name "
				."from pbs_bracket b left join pbs_referees r on (b.RefID = r.ID) "
				."left join pbs_players p1 on (p1.ID = Opponent1) "
				."left join pbs_players p2 on (p2.ID = Opponent2) "
				."where Timestamp and PlayerID = 0";
		$arrdir = array ("desc", "desc", "desc", "desc");
		$thisdir = "desc";
		if (!isset ($dir) || ($dir != "asc" && $dir != "desc")) $dir = "desc";
		if (isset ($sort)) {
			$key = 0;
			switch ($sort)
			{
				case "Opponent1":
					$key = "Opponent1";
					$arrdir[0] = ($dir == "asc") ? "desc" : "asc";
					break;
				case "Opponent2":
					$key = "Opponent2";
					$arrdir[1] = ($dir == "asc") ? "desc" : "asc";
					break;
				case "Time":
					$key = "Timestamp";
					$arrdir[2] = ($dir == "asc") ? "desc" : "asc";
					break;
				case "Referee":
					$key = "RefName";
					$arrdir[3] = ($dir == "asc") ? "desc" : "asc";
					break;
				default: // fuckin with the url?
					$sort = "id";
			}
			if ($key)
			{	// if we got a valid sort key
				$query .= " order by $key $dir";
			}
		} else $sort = "id";
		$rUpcoming = db_query ($query);
?>
<br>
<table border="0" cellpadding="2" cellspacing="2" align="center" class="match_list">
<tr>
  <td colspan="6" class="headerText2" align="center">Upcoming Matches</td>
</tr>
<tr align="center">
  <td></td>
  <td width="130px"><a href="<? echo "?show=$show&sort=Opponent1&dir=$arrdir[0]"; ?>"><? echo $gameTermS; ?> 1</a></td>
  <td width="130px"><a href="<? echo "?show=$show&sort=Opponent2&dir=$arrdir[1]"; ?>"><? echo $gameTermS; ?> 2</a></td>
  <td width="140px"><a href="<? echo "?show=$show&sort=Time&dir=$arrdir[2]"; ?>">Time</a></td>
  <td><a href="<? echo "?show=$show&sort=Referee&dir=$arrdir[3]"; ?>">Referee Assigned</a></td>
  <td></td>
</tr>
<?
		if (!mysql_num_rows ($rUpcoming))
		{
?>
<tr>
  <td class="td_normal" colspan="6">no matches coming up!</td>
</tr>
<?
		} else
		while ($upcoming = mysql_fetch_assoc ($rUpcoming))
		{
			$refName = ($upcoming["RefName"]) ? $upcoming["RefName"] : "None";
?>
<tr>
  <td class="td_normal"></td>
  <td class="header"><? echo "<a href=\"?show=".$gameTermS."s&id={$upcoming["Opponent1"]}\">{$upcoming["p1Name"]}</a>"; ?></td>
  <td class="header"><? echo "<a href=\"?show=".$gameTermS."s&id={$upcoming["Opponent2"]}\">{$upcoming["p2Name"]}</a>"; ?></td>
  <td class="td_normal"><? echo $upcoming["Timestamp"]; ?></td>
  <td class="td_normal"><? echo $refName; ?></td>
  <td class="td_normal"></td>
</tr>
<?
		}
?>
<tr>
  <td colspan="6" align="center"><? echo "sorted by $sort, " . ($dir == "asc" ? "descending" : "ascending") . " order"; ?></td>
</tr>
</table>
<?
	} else
	if ($show == "AvgScoreDiff")
	{
		$query = "select r.MapID, count(r.MapID) as gameCount, m.MapName, m.MapImage, "
				."sum(r.WinnerScore - r.LoserScore)/count(r.MapID) as numScoreDiff "
				."from pbs_rounds r, pbs_mappool m where r.MapID = m.ID group by r.MapID order by r.MapID";
				
		// forces desc/asc incase ppl fuck with it
		if (isset ($Sort)) $query .= ($Sort == "Desc") ? " desc" : " asc";
		
		$result = db_query ($query);
?>
<br>
<table border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
  <td colspan="2" class="headerText" align="center">
    SDI - Score Difference Index<br>
	Takes rounds played into account :]<br><br>
	Sort: <a href="?show=AvgScoreDiff&Sort=Desc">Desc</a> / <a href="?show=AvgScoreDiff&Sort=Asc">Asc</a><br>
	<br>
  </td>
</tr>
<?
		while ($mapInfo = mysql_fetch_assoc ($result))
		{
?>
<tr>
  <td class="headerText2">
    <? echo $mapInfo["MapName"]; ?><br>
    <img src="map_pics/<? echo $mapInfo["MapImage"]; ?>" border="0" class="td_sched">    
  </td>
  <td>
	<table border="0" cellpadding="1" cellspacing="1" align="center" width="100%">
	<tr>
	  <td class="headerText2" align="center" width="100px">Games Played</td>
	  <td class="headerText2" align="center" width="50px">SDI</td>
	</tr>
	<tr>
	  <td class="td_normal"><? echo $mapInfo["gameCount"]; ?></td>
	  <td class="td_normal"><? echo $mapInfo["numScoreDiff"]; ?></td>
	</tr>
	</table>
  </td>
</tr>
<?
		}
?>
</table>
<?
	} else
	if ($show == "About")
	{
		echo "<br><br><br>";
		aboutBox();
	} else
	if ($show == "Ranking")
	{
		$query = "select r.WinnerPID, r.LoserPID, sum(r.WinnerScore-r.LoserScore) as ScoreDiff, p1.Nick as WinnerNick, p2.Nick as LoserNick "
			."from pbs_rounds r "
			."left join pbs_players p1 on (r.WinnerPID = p1.ID) "
			."left join pbs_players p2 on (r.LoserPID = p2.ID) "
			."group by r.WinnerPID, r.LoserPID";
		$res = db_query ($query);
		$arrRanking = array();
		if (mysql_error()) echo mysql_error();
		else
		{
			$arrData = array();
			// queer but this makes the sql query so simple
			while ($row = mysql_fetch_assoc ($res))
				$arrData []= $row;
			foreach ($arrData as $row)
			{
				if (!isset ($arrRanking[$row["WinnerPID"]]))
				{
					//if ($row["WinnerPID"] == 5)
						//echo "setting " . $row["ScoreDiff"] . "<br>\n";
				
					$row["Nick"] = $row["WinnerNick"];
					$row["PID"] = $row["WinnerPID"];
					$row["MWon"] = 1;
					//$row["ScoreDiff"] = 0;
								
					//unset ($row["LoserPID"]);
					$arrRanking[$row["WinnerPID"]] = $row;
				} else
				{
					//if ($row["WinnerPID"] == 5)
						//echo "adding " . $row["ScoreDiff"] . "<br>\n";
						
					$arrRanking[$row["WinnerPID"]]["ScoreDiff"] += $row["ScoreDiff"];
					$arrRanking[$row["WinnerPID"]]["MWon"] ++;
				}
			}
			
			foreach ($arrData as $row)
			{
				if (!isset ($arrRanking[$row["LoserPID"]]))
				{
					//echo "LOSER! " . $row["LoserPID"] . "<br>\n";
					$row["Nick"] = $row["LoserNick"];
					$row["PID"] = $row["LoserPID"];
					$row["MWon"] = -1;
					$row["ScoreDiff"] = -$row["ScoreDiff"];
					$arrRanking[$row["LoserPID"]] = $row;
				} else
				{
					//if ($row["LoserPID"] == 5)
						//echo "subtracting " . $row["ScoreDiff"] . "<br>\n";
					$arrRanking[$row["LoserPID"]]["ScoreDiff"] -= $row["ScoreDiff"];
					$arrRanking[$row["LoserPID"]]["MWon"] --;
				}
			}
		}
		
		function cmpRanks ($a, $b)
		{	
			// incase of a tie check score diffs
			if ($a["MWon"] == $b["MWon"])
				if ($a["ScoreDiff"] > $b["ScoreDiff"])
					return -1;
					if ($a["MWon"] > $b["MWon"])
				return -1;
			return 1;
		}
		
		echo "<pre>";
		usort ($arrRanking, "cmpRanks");
		//print_r ($arrRanking);
		echo "</pre>";
?>
<br>
<table border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
  <td align="right"><hr noshade size="1" width="77"></td>
  <td width="200" align="center" class="td_seed_header">Bracket Path Length Ranking..</td>
  <td align="left"><hr noshade size="1" width="77"></td>
</tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td class="td_seed_header" width="50px">Rank</td>
  <td class="td_seed_header" width="220px">(Seed) Nick</td>
  <td class="td_seed_header" width="30px" title="Net Matches Won-Lost">MW</td>
  <td class="td_seed_header" width="30px" title="Net Score Diff">NSD</td>
</tr>
<?
	$rank = 1;
	foreach ($arrRanking as $ranked)
	{
?>
<tr>
  <td class="td_seed_entry"><? echo $rank++; ?></td>
  <td class="td_seed_entry"><? echo "(".$ranked["PID"].") <a href=\"?show=players&id=".$ranked["PID"]."\">".$ranked["Nick"]."</a>"; ?></td>
  <td class="td_seed_entry" title="Net Rounds Won/Lost"><? echo $ranked["MWon"]; ?></td>
  <td class="td_seed_entry" title="Average Score Diff"><? echo $ranked["ScoreDiff"]; ?></td>
</tr>
<?
	}
?>
</table>
<br>
<hr noshade size="1" width="277" align="center">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="">
<tr>
  <td align="center">
  ranking is computed by number of rounds won.<br>
  the tie-breaker is the net score difference<br>between wins and loses. props to zzjzz for the algo :D
  </td>
</tr>
</table>
<?
	}
	//db_printstats();
?>
</body>
</html>
<?
	//ob_end_flush();
?>