<?
    require_once ("bracket_cfg.php");

	session_start();
	// globals
	$VERSION_ID	= "v0.9.4 pmru remix";
	$VERSION_DATE	= "04/24/2012";
	
	//var $db_numqueries, $db_timestart, $db_timeend;
	
	function db_query ($query)
	{
		global $db_numqueries, $db_timestart, $db_timeend;
		$db_numqueries ++;
		$db_timestart += getMicroTime();
		$result = mysql_query ($query);
		$db_timeend += getMicroTime();
		if( mysql_error() )
		{
			showHint( "Error Running Query", "Bad Query: " . $query . "<br>Error Message: " . mysql_error() );
			exit; // lest not let the errors propogate across the page, stop it dead!
		}
		return $result;
	}
	
	function db_init ()
	{
		global $db_numqueries, $db_timestart, $db_timeend;
		$db_numqueries = 0;
		$db_timestart = 0;
		$db_timeend = 0;
	}
	
	function db_getquerytime ()
	{
		global $db_timestart, $db_timeend;
		return number_format ($db_timeend - $db_timestart, 5);
	}
	
	function db_getnumqueries ()
	{
		global $db_numqueries;
		return $db_numqueries;
	}
	
	function db_printstats ()
	{
		echo "<br>conducted " . db_getnumqueries() . " queries in " . db_getquerytime() . " seconds<br>\n";
	}
  
	function buildList($nSEPlayers, $nColumns, $nRows, $arrGraph)
	{
		$arrDropIns = array();
		$nPlayerID = $nSEPlayers * 2;
		$arrIDtoPlayerID = array();
		for ($i = 0; $i < $nColumns; $i ++)
		{
			$arrDrop = array();
			for ($j = 0; $j < $nRows; $j ++)
			{
				$id = $j * $nColumns + $i;
				
				if ($arrGraph[$id] == 1) {	// normal winner
					$nPlayerID ++;
					$arrIDtoPlayerID[$id] = $nPlayerID;
				} else if ($arrGraph[$id] == 2) {	// drop in from loser bracket
					$nPlayerID ++;
					//echo "nPlayerID = $nPlayerID<br>";
					$arrIDtoPlayerID[$id] = $nPlayerID;
				}
			}
			$arrDropIns[] = $arrDrop;
		}
		return $arrIDtoPlayerID;
	}

	function buildConverter ($nPlayers, $arrGraph, $nRows)
	{
		$nColumns = $nPlayers * 2 + 1;
	
		$nPlayerID = 32;
		for ($i = 0; $i < $nColumns; $i ++)
		{
			for ($j = 0; $j < $nRows; $j ++)
			{
				$id = $j * $nColumns + $i;
				if ($arrGraph[$id] == 1)
				{
					$nPlayerID ++;
					$arrGraph[($j - 2) * $nColumns + $i] = 3;
				} else
				if ($arrGraph[$id] == 2)
				{	// drop in from loser bracket
					$nPlayerID ++;
					echo "nPlayerID = $nPlayerID<br>";
					$arrIDtoPlayerID[$id] = $nPlayerID;
				}
			}
		}
		
		return $arrIDtoPlayerID;
	}

	function buildSeeds ($numPlayers)
	{
		$n_order = array();
		$start = array (1, $numPlayers);
		$n_order[] = $start[0];
		$n_order[] = $start[1];
		$n_order[] = ($start[1] + 1) - ($start[1] / 2);
		$n_order[] = ($start[1] / 2);
		$n_order[] = ($start[1] + 1) - ($start[1] * 3 / 4);
		$n_order[] = ($start[1] * 3 / 4);
		$n_order[] = ($start[1] * 3 / 4) + 1;
		$n_order[] = ($start[1] / 4);
		
		$magic1 = $numPlayers / 8;	// weeeeeeeeeee
		$magic2 = $magic1 / 2;
		$magic3 = $magic2 / 2;
		$incstart = array (0, $magic1);	// 16
		$incincstart = array (0, $magic2);	// 8
		$incincinc = array (0, $magic3, $magic3/2, $magic3 + ($magic3/2));	// 0, 4, 2, 6
		$incinc = array();
		for ($i = 0; $i < $magic3; $i ++)
		{
			$incinc[] = $incincstart[0] + $incincinc[$i];
			$incinc[] = $incincstart[1] + $incincinc[$i];		
		}
		$inc = array();
		for ($i = 0; $i < $magic2; $i ++)
		{
			$inc[] = $incstart[0] + $incinc[$i];
			$inc[] = $incstart[1] + $incinc[$i];
		}
		$seeds = array();
		for ($i = 0; $i < $magic1; $i ++)
		{
			$seeds[] = $n_order[0] + $inc[$i];
			$seeds[] = $n_order[1] - $inc[$i];
			$seeds[] = $n_order[2] + $inc[$i];
			$seeds[] = $n_order[3] - $inc[$i];
			$seeds[] = $n_order[4] + $inc[$i];
			$seeds[] = $n_order[5] - $inc[$i];
			$seeds[] = $n_order[6] + $inc[$i];
			$seeds[] = $n_order[7] - $inc[$i];
		}
		
		return $seeds;
	}
	
	function buildDoubleElim ($nPlayers)
	{
		$arrGraph = array();
		$aStack = array();
		$aNext = array();
		$aBigStack = array();
		$aDropIns = array();
		
		$nColumns = $nPlayers * 2 + 1;
		$nRows = $nPlayers * 2;
		$numRows = 0;

		$aDropRow = array();
		// startup the stack
		for ($i = 1; $i < $nRows; $i += 2)
		{
			$aStack[] = $i;
			$arrGraph[$i] = 2;
			$aDropRow[] = $i;
		}
		
		$aDropIns[] = $aDropRow;
		
		for ($i = 0; $i < $nColumns+1; $i ++)
		{		
			for ($j = 0; $j < $nRows + 1; $j ++)
			{
				$id = $i * $nRows + $j;
				//echo "id ($i/$j) = $id<br>\n";
				if (!isset ($arrGraph[$id]))
					$arrGraph[$id] = 0;
			}
			
			$aDropRow = array();
			
			if ($i % 2 == 0)
			{
				$aNext = array();
				for ($k = 0; $k < sizeof ($aStack); $k += 2)
				{
					if (sizeof ($aStack) == 1) {
						$aStack = 0;
						$aNext = 0;
						if (!$numRows)
							$numRows = $i + 1;
						continue;
					}
						
					$nOffset = ($aStack[$k] + $aStack[$k+1]) / 2;
					$nOffset = $nOffset - ($nRows + 1) * $i;
					if ($nOffset < 0) continue;
					$nNext = (($nRows+1) * ($i + 2)) + ((($aStack[$k] - (($nRows+1) * $i)) + ($aStack[$k+1]) - (($nRows+1) * $i)) / 2);
					$aBigStack[$nNext] = array ($aStack[$k], $aStack[$k+1]);
					$nLNext = $nNext - 2;
					$arrGraph[$nNext] = 1;
					if ($i % 4 == 0) {
							$arrGraph[$nLNext] = 2;
							$aNext[] = $nLNext;
							$aDropRow[] = $nLNext;
					}
					$aNext[] = $nNext;
				}
			}
			$aStack = $aNext;
		
			if (sizeof ($aDropRow))
				$aDropIns[] = $aDropRow;
						
		}
		return array ($arrGraph, $aBigStack, $aDropIns);
	}
	
	function buildDoubleElim2 ($nPlayers)
	{
		if( $nPlayers % 2 ) return 0;
		
		$arrGraph = array();
		$aStack = array();
		$aNext = array();
		$aBigStack = array();
		$aDropIns = array();
		
		$nColumns = $nPlayers * 2 + 1;
		$nRows = $nPlayers * 2;
		$numRows = 0;

		$aDropRow = array();
		for ($i = 1; $i < $nRows; $i += 2)
		{
			$aStack[] = $i;
			$arrGraph[$i] = 2;
			$aDropRow[] = $i;
		}
		
		$aDropIns[] = $aDropRow;
		
		for ($i = 0; $i < $nColumns+1; $i ++)
		{		
			for ($j = 0; $j < $nRows + 1; $j ++)
			{
				$id = $i * $nRows + $j;
				if (!isset ($arrGraph[$id]))
					$arrGraph[$id] = 0;
			}
			
			$aDropRow = array();
			
			if ($i % 2 == 0)
			{
				$aNext = array();
				for ($k = 0; $k < sizeof ($aStack); $k += 2)
				{
					if (sizeof ($aStack) == 1) {
						$aStack = 0;
						$aNext = 0;
						if (!$numRows)
							$numRows = $i + 1;
						continue;
					}
						
					$nOffset = ($aStack[$k] + $aStack[$k+1]) / 2;
					$nOffset = $nOffset - ($nRows + 1) * $i;
					if ($nOffset < 0) continue;
					$nNext = (($nRows+1) * ($i + 2)) + ((($aStack[$k] - (($nRows+1) * $i)) + ($aStack[$k+1]) - (($nRows+1) * $i)) / 2);
					$aBigStack[$nNext] = array ($aStack[$k], $aStack[$k+1]);
					$nLNext = $nNext - 2;
					$arrGraph[$nNext] = 1;
					$arrGraph[(($nRows+1) * ($i)) + ((($aStack[$k] - (($nRows+1) * $i)) + ($aStack[$k+1]) - (($nRows+1) * $i)) / 2)] = 3;
					
					if ($i % 4 == 0) {
							$arrGraph[$nLNext] = 2;
							$aNext[] = $nLNext;
							$aDropRow[] = $nLNext;
					}
					$aNext[] = $nNext;
				}
			}
			$aStack = $aNext;
		
			if (sizeof ($aDropRow))
				$aDropIns[] = $aDropRow;
						
		}

		return array ($arrGraph, $aBigStack, $aDropIns);
	}
	
	function buildDoubleElim2Small ($nPlayers)
	{
		$arrGraph = array();
		$aStack = array();
		$aNext = array();
		
		$nColumns = $nPlayers * 2 + 1;
		$nRows = $nPlayers * 2;
		$numRows = 0;

		$aDropRow = array();
		for ($i = 1; $i < $nRows; $i += 2)
		{
			$aStack[] = $i;
			$arrGraph[$i] = 2;
			$aDropRow[] = $i;
		}
			
		for ($i = 0; $i < $nColumns+1; $i ++)
		{		
			for ($j = 0; $j < $nRows + 1; $j ++)
			{
				$id = $i * $nRows + $j;
				if (!isset ($arrGraph[$id]))
					$arrGraph[$id] = 0;
			}
			
			$aDropRow = array();
			
			if ($i % 2 == 0)
			{
				$aNext = array();
				for ($k = 0; $k < sizeof ($aStack); $k += 2)
				{
					if (sizeof ($aStack) == 1) {
						$aStack = 0;
						$aNext = 0;
						if (!$numRows)
							$numRows = $i + 1;
						continue;
					}
						
					$nOffset = ($aStack[$k] + $aStack[$k+1]) / 2;
					$nOffset = $nOffset - ($nRows + 1) * $i;
					if ($nOffset < 0) continue;
					$nNext = (($nRows+1) * ($i + 2)) + ((($aStack[$k] - (($nRows+1) * $i)) + ($aStack[$k+1]) - (($nRows+1) * $i)) / 2);
					$nLNext = $nNext - 2;
					$arrGraph[$nNext] = 1;
					$arrGraph[(($nRows+1) * ($i)) + ((($aStack[$k] - (($nRows+1) * $i)) + ($aStack[$k+1]) - (($nRows+1) * $i)) / 2)] = 3;
					
					if ($i % 4 == 0) {
							$arrGraph[$nLNext] = 2;
							$aNext[] = $nLNext;
							$aDropRow[] = $nLNext;
					}
					$aNext[] = $nNext;
				}
			}
			$aStack = $aNext;						
		}

		return array ($arrGraph);
	}
	
	function buildSeedList ($nPlayers)
	{
		$n_order = array();
		$start = array (1, $nPlayers);
		$n_order[] = $start[0];
		$n_order[] = $start[1];
		$n_order[] = ($start[1] + 1) - ($start[1] / 2);
		$n_order[] = ($start[1] / 2);
		$n_order[] = ($start[1] + 1) - ($start[1] * 3 / 4);
		$n_order[] = ($start[1] * 3 / 4);
		$n_order[] = ($start[1] * 3 / 4) + 1;
		$n_order[] = ($start[1] / 4);
		
		$magic1 = $nPlayers / 8;	// weeeeeeeeeee
		$magic2 = $magic1 / 2;
		$magic3 = $magic2 / 2;
		$incstart = array (0, $magic1);	// 16
		$incincstart = array (0, $magic2);	// 8
		$incincinc = array (0, $magic3, $magic3/2, $magic3 + ($magic3/2));	// 0, 4, 2, 6
		$incinc = array();
		for ($i = 0; $i < $magic3; $i ++)
		{
			$incinc[] = $incincstart[0] + $incincinc[$i];
			$incinc[] = $incincstart[1] + $incincinc[$i];		
		}
		$inc = array();
		for ($i = 0; $i < $magic2; $i ++)
		{
			$inc[] = $incstart[0] + $incinc[$i];
			$inc[] = $incstart[1] + $incinc[$i];
		}
		$seeds = array();
		for ($i = 0; $i < $magic1; $i ++)
		{
			$seeds[] = $n_order[0] + $inc[$i];
			$seeds[] = $n_order[1] - $inc[$i];
			$seeds[] = $n_order[2] + $inc[$i];
			$seeds[] = $n_order[3] - $inc[$i];
			$seeds[] = $n_order[4] + $inc[$i];
			$seeds[] = $n_order[5] - $inc[$i];
			$seeds[] = $n_order[6] + $inc[$i];
			$seeds[] = $n_order[7] - $inc[$i];
		}
		
		return $seeds;
	}
	
	function buildDoubleEliminationGraph ($rows, $columns)	
	{	
		for ($i = 1; $i <= $rows; $i += 4)
		{
			${"half1"}[] = array ($i, $i + 2);	// the start
		}
		
		$done = 0;
		for ($i = 1; $i < $columns && !$done; $i ++)
		{
			$winarr = array();
			for ($j = 0; $j < sizeof (${"half$i"}) && !$done; $j ++)
			{
				$n = $i + 1;
				$winner1 = ${"half$i"}[$j][0];
				$winner2 = ${"half$i"}[$j][1];
				$winner = ($winner1 + $winner2) / 2;
				
				if ($i % 2 == 1)
				{
					$winarr[] = $winner - 2;
					$id = ($i+1) * $rows + $winner - 1;
					$pink[$id] = 1;
					//echo "\$pink[$id] = 1<br>\n";
				}
				$winarr[] = $winner;
				$id = ($i) * $rows + $winner1 + 1;
				$graph[$id] = 1;
				$id = ($i) * $rows + $winner2 + 1;
				$graph[$id] = 1;
				
				if (sizeof ($winarr) == 2) {
					${"half$n"}[] = $winarr;
					$winarr = array();
				}
				if (sizeof ($winarr) == 1 && sizeof (${"half$i"}) == 1) {
					${"half$n"}[] = $winarr;
					$id = ($i+1) * $rows + $winarr[0] + 1;
					$graph[$id] = 1;
					$last = $n;
					$done = 1;
				}
			}
		}
		return array ($graph, $pink, $last);
	}
	
	function getMicroTime()
	{ 
		list ($usec, $sec) = explode(" ",microtime()); 
		return ((float)$usec + (float)$sec); 
	}
	
	$arrTimer = array();
	function timeCode ($timerID, $timerDesc = NULL)
	{
		global $arrTimer;
		if (!isset($arrTimer[$timerID]))
		{
			$arrTimer[$timerID] = array ("Time" => getMicroTime(), "Desc" => $timerDesc);
			echo "Timer $timerID started<br>";
		} else {
			$time = number_format (getMicroTime() - $arrTimer[$timerID]["Time"], 5);
			echo "Timer $timerID [".$arrTimer[$timerID]["Desc"]."]: $time seconds<br>";
			unset ($arrTimer[$timerID]);
		}
	}
	
	function editTimeData ($nSpotID)
	{
		// sets the defaults
		// get the matches' time info
		$rTimeInfo = db_query ("select date_format(Timestamp, '%Y-%m-%d %H:%i:%s') as Time, RefID from pbs_bracket where ID = $nSpotID");
		$aTimeInfo = mysql_fetch_assoc ($rTimeInfo);
		$sTimeInfo = $aTimeInfo["Time"];
		$nRefID = $aTimeInfo["RefID"];
		preg_match ("/(\d{4})-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})/", $sTimeInfo, $matches);
		if ($matches[3] > 0)
		{	// if a day was selected, the timestamp is valid (meh?)
			//$nDate	= $matches[0];
			$nYear	= $matches[1];
			$nMonth	= $matches[2];
			$nDay	= $matches[3];
			$nHour	= $matches[4];
			$nMin	= $matches[5];
			$nSec	= $matches[6];
		} else
		{	// else we dont have a selected time, make up defaults!
			// get todays month, year for the defaults
			list ($nMonth, $nYear) = split ("-", date ("m-Y"));
			$nDay	= "0";
			$nHour	= "17";
			$nMin	= "00";
			$nSec	= "00";			
		}
		
		return array (
			"nMonth" => $nMonth, "nYear" => $nYear, "nDay" => $nDay,
			"nHour" => $nHour, "nMin" => $nMin, "nSec" => $nSec, "sTimeInfo" => $sTimeInfo, "nRefID" => $nRefID
		);
	}

	function editTime ($nSpotID, $nSpot1, $nSpot2)
	{
		extract (editTimeData($nSpotID));
		
		$rPlayers = db_query ("select Nick from pbs_players where ID = $nSpot1 or ID = $nSpot2");
		$arrPlayers = array ();
		while ($row = mysql_fetch_assoc ($rPlayers))
			$arrPlayers[] = $row["Nick"];
?>
<form action="bracket_admin_post.php" method="post">
<input type="hidden" name="detailAction" value="Set Date">
<input type="hidden" name="iOpponent1" value="<? echo $nSpot1; ?>">
<input type="hidden" name="iOpponent2" value="<? echo $nSpot2; ?>">
<table cellpadding="1" cellspacing="1" align="center">
<tr>
  <td colspan="3" class="td_normal" align="center"><? echo $arrPlayers[0] . " vs " . $arrPlayers[1]; ?></td>
</tr>
<tr>
  <td valign="top">
    <div id="writeroot">Please make sure javascript<br>is enabled on your browser..</div>
  </td>
  <td width="10">&nbsp;
  </td>
  <td width="100" valign="top">
    <table border="0" cellpadding="1" cellspacing="1">
    <tr>
      <td align="center">
        Hour
      </td>
      <td align="center">
        Min
      </td>
    </tr>
    <tr>
      <td>
        <select name="nHour" size="1" onChange="hourSelect(this)">
<?
	  	for ($i = 0; $i < 24; $i ++)
		{
			$paddedhour = sprintf ("%02d", $i);
			echo "<option value=\"$paddedhour\"";
			if ($i == $nHour)
				echo " selected";
			echo ">";
			$usmode = ($i > 12) ? ($i - 12) . " PM" : $i . " AM";
			if ($i == 12) $usmode = "12 Noon";
			if ($i == 00) $usmode = "Midnight";
			echo str_replace (" ", "&nbsp;", sprintf ("%5s", $usmode));
			echo "</option>\n";
		}
?>
        </select> 
      </td>
      <td>
        <select name="nMinute" size="1" onChange="minSelect(this)">
<?
  	for ($i = 0; $i < 60; $i += 10)
	{
		$paddedminute = sprintf ("%02d", $i);
		echo "<option value=\"$paddedminute\"";
		if ($i == $nMin)
			echo " selected";
		echo ">$paddedminute</option>\n";
	}
?>
        </select> 
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <br>
        <u>Timestamp</u>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="text" class="text" id="dateString" name="dateString" maxlength="19" size="25" style="text-align: center;" value="<? echo $sTimeInfo; ?>">
      </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
	  	<input type="hidden" name="nSpotID" value="<? echo $nSpotID; ?>">
      </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <br>
        <u>Referee</u>
      </td>
    </tr>
	<tr>
	  <td colspan="2" align="center">
	    <select name="iReferee" size="1"<? if ($_SESSION["bIsAdmin"] != 'Y') echo " disabled"; ?>>
		  <option value="0">None</option>
<?
		$result = db_query ("select ID, RefName from pbs_referees order by RefName");
		while ($referee = mysql_fetch_assoc ($result))
		{
			$addString = ($nRefID == $referee["ID"]) ? " selected" : "";
?>
		  <option value="<? echo $referee["ID"]; ?>"<? echo $addString; ?>><? echo $referee["RefName"]; ?></option>
<?
		}
?>
		</select>
	  </td>
	</tr>
    </table>
  </td>
</tr>
<tr>
  <td colspan="3" align="center">
    <input type="submit" name="detailAction" value="Set Date" class="button">
	<input type="submit" name="detailAction" value="Cancel" class="button">
	<?
		if( $_SESSION["bIsAdmin"] == 'Y' )
		{
	?>
	<input type="submit" name="detailAction" value="Clear" class="button">
	<?
		}
	?>
  </td>
</tr>
</table>
</form>
<?
	}
	
	function editScores ($editSID)
	{
		// upload through chtv or use local uploader
		global $bUseCHTV;
				
		// get the match info
		$qMatchInfo = "select WinnerScore, DemoID, DropInID, DemoURL, LoserScore, MapID, Screenshot, ID from pbs_rounds where BracketID = $editSID order by SequenceNum";
		$rMatchInfo = db_query ($qMatchInfo);
		$aMatchInfo = array();
		while ($tmpMatchInfo = mysql_fetch_assoc ($rMatchInfo))
			$aMatchInfo[] = $tmpMatchInfo;
		
		// get the players info
		$qPlayersInfo = "select r.WinnerPID, r.LoserPID, p1.Nick as nameWinner, p2.Nick as nameLoser from pbs_rounds r "
				."left join pbs_players p1 on (p1.ID = r.WinnerPID) "
				."left join pbs_players p2 on (p2.ID = r.LoserPID) where r.BracketID = $editSID limit 1";
		$rPlayersInfo = db_query ($qPlayersInfo);
		$aPlayersInfo = mysql_fetch_assoc ($rPlayersInfo);
	
		// get the map list
		$rMapList = db_query ("select ID, MapName from pbs_mappool order by ID");
		$aMapInfo = array();	
		while ($tmpMapInfo = mysql_fetch_assoc ($rMapList))
			$aMapInfo[] = $tmpMapInfo;

		// if there are no rows for this match that means the result wasnt set... cant set scores before we know who won :/
		if (!count($aMatchInfo))
		{			
			showHint ("Cannot Edit Scores", "<br>You must set the result of a match before you can specify scores!<br><br>", 250, "center");
			return 0;
		}
		
		// get amount of maps played per match/game (default hardcoded to 3)
		$rMPGame = db_query ("select Value from pbs_config where Label = 'MPG'");
		$nMapsPerGame = mysql_result ($rMPGame, 0);
		
		// in case # of maps was changed after some matches were already added (hack)			
		if( count( $aMatchInfo ) != $nMapsPerGame )
		{  // meh add the rows that should of been added when match result was first set
			for ($i = count( $aMatchInfo ) + 1; $i <= $nMapsPerGame; $i ++)
				db_query( "insert into pbs_rounds (BracketID, DropInID, WinnerPID, LoserPID, SequenceNum) "
					 ."values ($editSID, {$aMatchInfo[0]["DropInID"]}, {$aPlayersInfo["WinnerPID"]}, {$aPlayersInfo["LoserPID"]}, $i)" );
			// GAH we cant reload the page from HERE reload the page
			showHint ("Score Resize", "<br>The scores were created with a different maps-per-game settings. The number of rounds have been updated, please refresh the page to unlock the results editor.<br><br>", 350, "center");
			exit;
		}

		// LIMITATION: max 10 maps per game
		if ($nMapsPerGame < 1 || $nMapsPerGame > 10)
			$nMapsPerGame = 3;
?>
<form enctype="multipart/form-data" action="bracket_cpost.php" method="post">
<input type="hidden" name="nMPG" value="<? echo $nMapsPerGame; ?>">
<?
	for ($i = 1; $i <= $nMapsPerGame; $i ++)
	{
?>
<input type="hidden" name="nRoundID_<? echo $i; ?>" value="<? echo $aMatchInfo[$i-1]["ID"]; ?>">
<?
	}
?>
  <br>
  <table align="center" class="data_list">
  <tr>
    <td width="100px" class="header" align="center">
      Round
    </td>
    <td width="100px" class="header" align="center">
      <? echo $aPlayersInfo["nameWinner"]; ?>
    </td>
    <td width="100px" class="header" align="center">
      <? echo $aPlayersInfo["nameLoser"]; ?>
    </td>
    <td width="120px" class="header" align="center">
      Map
    </td>
  </tr>
  <?

	// cheaper than 'date ("S", mktime (0, 0, 0, 0, $i, 0))'
	// LIMITATION: max 10 maps per game
	$prefix = array ("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th", "th");

	for ($i = 1; $i <= $nMapsPerGame; $i ++)
	{
?>
  <tr>
    <td align="center" class="entry">
      <? echo ($i) . $prefix[$i]; ?> Map
    </td>
    <td align="center" class="entry">
      <input type="text" name="nWScore_<? echo $i; ?>" value="<? echo $aMatchInfo[$i-1]["WinnerScore"]; ?>" class="text" size="4">
    </td>
    <td align="center" class="entry">
      <input type="text" name="nLScore_<? echo $i; ?>" value="<? echo $aMatchInfo[$i-1]["LoserScore"]; ?>" class="text" size="4">
    </td>
    <td align="center" class="entry">
      <select name="nMap_<? echo $i; ?>">
       <option value="0">None</option>
<?
		foreach ($aMapInfo as $sMapInfo)
		{
?>
        <option value="<? echo $sMapInfo["ID"]; ?>"<? if ($sMapInfo["ID"] == $aMatchInfo[$i-1]["MapID"]) echo " selected"; ?>><? echo $sMapInfo["MapName"]; ?></option>
<?
		}
?>
      </select> 
    </td>
  </tr>
  <?
	}
?>
  <tr>
    <td align="center" class="empty" colspan="2">
      <input type="submit" name="setScores" value="Set Scores" class="button">
    </td>
    <td align="center" class="empty" colspan="2">
      <input type="submit" name="setScores" value="Cancel" class="button">
    </td>
  </tr>
  </table>
  <br><br>
  <table class="data_list" align="center">
  <tr>
    <td width="200" rowspan="<? echo ($nMapsPerGame + 1); ?>" align="center" class="header">
	Once you have set scores,<br>
	you may upload the demos.
    </td>
    <td align="center" class="light_header"><u>Map</u></td>
<?
  	if ($bUseCHTV)
  	{	// display demoids
    	echo <<<EOT
    <td colspan="1" align="center" class="light_header"><u>DemoIDs</u></td>
EOT;
  	} else
  	{	// display demo upload status
  		echo <<<EOT
    <td align="center" class="light_header" width="70"><u>Action</u></td>
    <td align="center" class="light_header" width="70"><u>Status</u></td>
EOT;
  	}
?>
  </tr>
<?
	for ($i = 1; $i <= $nMapsPerGame; $i ++)
	{		
		echo "<tr>";		
		$mapinfo = $i . $prefix[$i];
		echo <<<EOT
		<td align="center" width="60" class="entry">$mapinfo Map</td>
EOT;
  		if ($bUseCHTV)
  		{	// display demoid input field
  			$DemoID = $aMatchInfo[$i-1]["DemoID"];
  			echo <<<EOT
    <td><input type="text" name="iDemo_$i" size="10" class="text" value="$DemoID"></td>
EOT;
  		} else
  		{	// display demo actions
  			$RoundID = $aMatchInfo[$i-1]["ID"];
			if ($aMatchInfo[$i-1]["DemoURL"])
			{	// a demo has already been uploaded
				echo <<<EOT
	<td align="center" class="entry">
      <a href="bracket_demoup.php?RoundID=$RoundID">ReUpload</a>
    </td>
    <td align="center" class="entry">Demo Exists</td>
EOT;
			} else
			if ($aMatchInfo[$i-1]["MapID"])
			{	// no demo has been uploaded but map scores have been submited
				echo <<<EOT
	<td align="center" class="entry">
    <a href="bracket_demoup.php?RoundID=$RoundID">Upload</a>
    </td>
    <td align="center" class="entry">No Demo</td>
EOT;
			} else
			{	// we got nothing to do
				echo <<<EOT
    <td align="center" class="entry">
	  None
    </td>
    <td align="center" class="entry">No Scores</td>
EOT;
			}
  		}
	echo "</tr>";
	} // for i (1 .. mapspergame)
?>
  </table>
  <br><br>
  <table class="message_window" align="center" cellspacing="1">
  <tr>
    <td colspan="2" class="header" align="center"><u>Screenshot Upload</u></td>
    <td width="70" class="header" align="center"><u>Status</u></td>
  </tr>
<?
	for ($i = 1; $i <= $nMapsPerGame; $i ++)
	{  
?>
  <tr>
    <td width="60" class="entry"><? echo ($i) . $prefix[$i]; ?> Map</td>
    <td class="entry"><input type="file" name="iScreenshot_<? echo $i; ?>" size="45" class="file"></td>
    <td align="center" class="entry"><? echo (strlen($aMatchInfo[$i-1]["Screenshot"])) ? "SS Exists" : "No SShot"; ?></td>
  </tr>
<?
	}
?>
  </table>
<?
	$body =  "To upload the screenshots just browse to the files and hit Set Scores. "
			."You can do this while setting scores, or at a later time.<br><br>"
			."Screenshots will be automatically resized.<br><br>";
	if ($bUseCHTV)
		$body .= "Use CHTV's own demo uploader then insert the generated IDs into the Demos fields. [<a href=\"http://www.challenge-tv.com/file_uploader.php\">Demo Uploader</a>]<br><br>";
	else
		$body .= "You <u>must</u> submit the scores for a map before you can upload a Demo for that map.<br><br>";
	$body .= "Like the screenshots, this is optional and can be done at any time.<br>";
	showHint("Screenshot/Demo Upload", $body, 390);
?>
</form>

<?
	}
	
	function checkFilename ($filepath, $filename)
	{	// filepath must include a trailing /
		$checkme = $filepath . $filename;
		if (file_exists ($checkme))
		{
			// split the name off at the extension
			$extension = end(explode(".", $filename));
			$basename = basename ($filename, ".$extension");
			// find an unused vesion of the name
			for ($i = 1; $i < 100; $i ++) {	// incase its a popular name :X
				$filename = $basename."_$i.".$extension;
				$checkme = $filepath . $filename;
				if (!file_exists ("$checkme"))	// ok this one doesnt exist go with it
					return $filename;
			}
			return 0;	// shouldnt happen
		} else return $filename;
	}
	
	function resizeImage ($filename, $new_width, $new_height)
	{
		if (!($gd_ext = get_extension_funcs ("gd"))) {
			echo "The GD Library is not installed!<br>Cannot resize image<br>\n";
			return 0;
		}
		if (function_exists("gd_info"))
		{
			$gd_info = gd_info();
			if ($gd_info["JPG Support"] != 1)
			{
				echo "The GD Library does not support JPG format<br>Cannot resize image<br>\n";
				return 0;
			}
			$gd = 2;
		} // FIXME: check jpeg support through phpinfo
		else $gd = 1;
	
		list( $image_width, $image_height, $image_type, ) = getimagesize( $filename );
		
		if( $image_type != 2 )	// not a jpeg... meh...
			return 1;	// should return 0 but.. for the sake of compatability with shitty games..
		
  		$oldImage = imagecreatefromjpeg ($filename);
  		if (!$oldImage) return 0;
		
		if ($gd == 2) {
			$newImage = imagecreatetruecolor ($new_width, $new_height);
			imagecopyresampled ($newImage, $oldImage, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);	
		} else {
			$newImage = imagecreate ($new_width, $new_height);
			imagecopyresized ($newImage, $oldImage, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
		}

		if (!imagejpeg ($newImage, $filename, 87))
			return 0;
		
		return 1;
	}
	
	function handleScreenshot ($nID, $Screenshot)
	{
		if (!$Screenshot["size"]) return 2;	// no file uploaded

		$destDir	= "screenshots/";
		$fileName	= checkFilename($destDir, $Screenshot["name"]); // returns a free version of the filename
		$destPath	= $destDir . $fileName;
		
		$bIsGoodUpload = is_uploaded_file ($Screenshot["tmp_name"]);
		if (!$bIsGoodUpload) showHint ("Error Uploading", "file does not qualify as a proper rfc1867 upload");
		$bMoved = move_uploaded_file($Screenshot["tmp_name"], $destPath);

		if (!$bMoved) showHint ("Error Uploading", "Screenshot could not be moved:<br>$Screenshot[tmp_name] to $destPath");
		else
		{
			if (!resizeImage($destPath, 640, 480))
			{
				showHint ("Error Resizing", "Screenshot could not be resized");
				return 0;
			} else
			{
				db_query ("update pbs_rounds set Screenshot = '$destPath' where ID = $nID");
				if (mysql_error()) {
					showHint ("Error Updating DB", "Screenshot could not be added to database:<br>".mysql_error());
					return 0;
				}
				return 1;
			}
		}
		return 0;
	}
	
	// i love common code :D this is taken directly out of my chworld common code
	function safeString ($String) {
		return (get_magic_quotes_gpc()) ? $String : addslashes ($String);
	}
	
	function safeUnset ($Var) {
		unset ($_SESSION[$Var], $Var);
	}
	
	function safeNL2Br ($String)
	{
		$String = str_replace ("\r\n", "\n", $String);
		$String = str_replace ("\r", "", $String);
		$String = str_replace ("\n", "<br>", $String);
		return $String;
	}
	
	function safeBr2NL ($String)
	{
		return str_replace ("<br>", "\n", $String);
	}
	
	function safeBreak ($String) {
		if (get_magic_quotes_gpc())
			return $String;
			
		$String = str_replace ("\r", "\\r",$String);
		$String = str_replace ("\n", "\\n",$String);
		return $String;
	}
	
	function redirectTo ($url)
	{
		$insertGoTo = $url;
		if (isset ($_SERVER['QUERY_STRING'])) {
			$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
			$insertGoTo .= $_SERVER['QUERY_STRING'];
		}
		header (sprintf ("Location: %s", $insertGoTo));
	}
	
	function showHint ($title, $body, $width = 400, $align = "left")
	{
?>
  <br>
  <table class="message_window" cellspacing="0" align="center" width="<? echo $width; ?>px">
  <tr>
    <td class="header"><? echo $title; ?></td>
  </tr>
  <tr>
    <td class="body">
	  <? echo $body; ?>
	</td>
  </tr>
  </table>
  <br>
<?
	}
	
	function showRounds ($rounds)
	{
		$alt = false;
		foreach ($rounds as $round)
		{			
			$alt = !$alt;
			$WinScore = $round["WinnerScore"];	// score of the MATCH's winner
			$LoseScore = $round["LoserScore"];	// score of the MATCH's loser
			//$WinRound = ($WinScore > $LoseScore) ? "Won" : "Lost";
			//$LoseRound = ($LoseScore > $WinScore) ? "Won" : "Lost";
			$match = $round["BracketID"];
			
			// skip maps with no results (not played) ? why would this even be inserted? stupid referees
			if (!$WinScore && !$LoseScore) continue;

			//if ($round["Screenshot"] != NULL)
				//$MapText = "<a href=\"?show=matchDetail&match=$match&screenshot=$round[SequenceNum]\">$round[MapName]</a>";
			//else
			$MapText = $round["MapName"];
				
			$class = ($alt) ? "td_sched" : "td_normal";
?>
<tr align="center">
  <td width="100">
<?
			if ($round["Screenshot"])
				echo "[<a href=\"?show=match&matchid=$match&ssid=$round[SequenceNum]\">Screenshot</a>]<br>";
			else
				echo "No Screenshot";
?>
  </td>
  <td class="entry" style="text-align: center"><? echo $WinScore; ?></td>
  <td class="" colspan="2" width="70"><? echo $MapText; ?></td>
  <td class="entry" style="text-align: center"><? echo $LoseScore; ?></td>
  <td width="100">
<?
			global $destinationPath;
			if ($round["DemoID"])
				echo "[<a href=\"http://www.challenge-tv.com/index.php?mode=demodetail&demo=$round[DemoID]\">Demo</a>]<br>";
			else if ($round["DemoURL"])
				echo "[<a href=\"".$destinationPath.$round["DemoURL"]."\">Demo</a>]<br>";
			else
				echo "No Demo";
?>
  </td>
</tr>
<?
		}
	}
	
	function showMatchDetail ($match, $screenshot)
	{
		global $gameTermS, $bShowMeter;
		
		$gm  = getGameMode();
		$gameTermS = strtolower ($gameTermS);
		
	//	if ($bracket == "loser")
//			$check = "r.DropInID = $match";
		//else if ($bracket == "winner")
		$check = "r.BracketID = $match or r.DropInID = $match";
	
		$query = "select p1.Nick as WinNick, p2.Nick as LoseNick, LoserPID, WinnerPID, WinnerScore, LoserScore, BracketID, SequenceNum, MapName, Screenshot, DemoID, DemoURL, RefName "
				."from pbs_players p1, pbs_players p2, pbs_rounds r, pbs_mappool m, pbs_bracket b "
				."left join pbs_referees ref on (ref.ID = b.RefID) "
				."where (b.ID = r.BracketID) and (p1.ID = r.WinnerPID) and (p2.ID = r.LoserPID) and ($check) and (m.ID = r.MapID) "
				."order by SequenceNum";
	
		$result = db_query ($query);
		if (mysql_error()) echo mysql_error();
		$rounds = array();

		if (!mysql_num_rows ($result)) {
			//echo "<br><br><br><br><div align=\"center\">No Match Information Has Been Posted!</div><br>";
			echo "<br><br><br><br><br><br>";
			showHint ("Match Details", "<br>No match information is available.<br>Complain to the tournament admins!<br><br>", 240, "center");
		} else
		{
		
			while ($round = mysql_fetch_assoc ($result))
				$rounds []= $round;
			
			$WinnerPID = $rounds[0]["WinnerPID"];
			$LoserPID = $rounds[0]["LoserPID"];
			$RefName = $rounds[0]["RefName"];
			//var_dump($rounds[0]);
			
			$WinInfo = "($WinnerPID) <a href=\"?show=".$gameTermS."s&id=$WinnerPID\">" . $rounds[0]["WinNick"] . "</a>";
			$LoseInfo = "<a href=\"?show=".$gameTermS."s&id=$LoserPID\">".$rounds[0]["LoseNick"] . "</a> ($LoserPID)";
			
			// compute ownij
			$team1 = 0;
			$team2 = 0;
			foreach ($rounds as $round)
  			{
  				$team1 += $round["WinnerScore"];
  				$team2 += $round["LoserScore"];
  			}
  			$total = $team1 + $team2;
  			$team1 = ($total) ? $team1/$total : 0;
  			$team2 = ($total) ? $team2/$total : 0;
  			$ownscale = ($team1 - .5) * 2;
  			$scalesize = 20;
  			//echo "\$team1 = $team1, \$team2 = $team2, \$total = $total, \$ownscale = $ownscale, \$scalesize = $scalesize<br>";
  			$arrTeam1 = array();
  			$arrTeam2 = array();
  			if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
			  {	// get the team members
				  // first team
			    $rTeam = db_query ("select Name, Captain from pbs_teamdata where TeamID = $WinnerPID order by Name");
			    if (mysql_error()) showHint ("Error Fetching Team 1 Player List", mysql_error());
			    while ($row = mysql_fetch_assoc ($rTeam))
			    	$arrTeam1 []= $row["Name"];
			    mysql_free_result ($rTeam);
			    // second team
			    $rTeam = db_query ("select Name, Captain from pbs_teamdata where TeamID = $LoserPID order by Name");
			    if (mysql_error()) showHint ("Error Fetching Team 2 Player List", mysql_error());
			    while ($row = mysql_fetch_assoc ($rTeam))
			    	$arrTeam2 []= $row["Name"] . (($row["Captain"] == 'Y') ? "<a class=\"red\">*</a>" : "");
			    mysql_free_result ($rTeam);
			  }
  			
  			
?>
<br>
<table align="center" class="match_list">
<tr>
   <td colspan="6" align="center">
    Match Details<br>	
   </td>
</tr>
<tr>
  <td colspan="3" class="header"><? echo $WinInfo; ?></td>
  <td colspan="3" class="header"><? echo $LoseInfo; ?></td>
</tr>
<?
	showRounds($rounds);
	if ($bShowMeter)
	{
?>
<tr>
  <td colspan="6" align="center">
<?
    	echo "Close Game |";
    	for ($i = 0; $i < $ownscale * $scalesize - 2; $i ++)
    		echo "-";
    	echo "<a class=\"headerText\">[--]</a>";
    	for ($i = $ownscale * $scalesize + 2; $i < $scalesize; $i ++)
    		echo "-";
    	echo "| Total Rape";
    	//[-------------------[--]-------------------]
?>
  </td>
</tr>
<?
	}
	if ((count ($arrTeam1) || count ($arrTeam2)) && !$screenshot)
	{
?>
<tr>
  <td colspan="6"><hr width="200" size="1" noshade></td>
</tr>
<tr>
  <td colspan="3" valign="top">
    <table border="0" cellpadding="1" cellspacing="1" align="center" width="120">
    <tr><td align="center" class="td_normal">Members</td></tr>
    <?
		foreach ($arrTeam1 as $member)
			echo "<tr><td class=\"td_unsched\">$member</td></tr>";
		if (!count ($arrTeam1))
			echo "<tr><td class=\"td_normal\">Empty Roster</td></tr>";
    ?>
    </table>
  </td>
  <td colspan="3" valign="top">
    <table border="0" cellpadding="1" cellspacing="1" align="center" width="120">
    <tr><td align="center" class="td_normal">Members</td></tr>
    <?
		foreach ($arrTeam2 as $member)
			echo "<tr><td class=\"td_unsched\">$member</td></tr>";
		if (!count ($arrTeam2))
			echo "<tr><td class=\"td_normal\">Empty Roster</td></tr>";
    ?>
    </table>
  </td>
</tr>
<?
	}
	if (0){
?>
<tr>
  <td colspan="6" align="center" class="headerText2">
  <br>
  <? if ($RefName != null) echo "Refereed by: $RefName"; ?>
  </td>
</tr>
<tr>
  <td colspan="6" align="center">
  <table border="0" cellpadding="1" cellspacing="1" align="center" width="80%">
  <tr>
    <td class="headerText2" align="center"><b>Ownij Meters</b><br><br></td>
  </tr>
  <tr>
    <td class="headerText2" align="center">
    <?
    	echo "|";
    	for ($i = 0; $i < $team2 * 40 - 2; $i ++)
    		echo "-";
    	echo "[--]";
    	for ($i = $team2 * 40 + 2; $i < 40; $i ++)
    		echo "-";
    	echo "|";
    ?>
    </td>
  </tr>
<?
  	foreach ($rounds as $round)
  	{
  		echo "<tr><td align=\"center\">";
  		echo $round["WinNick"] . " vs " . $round["LoseNick"] . " on " . $round["MapName"] . ": " . $round["WinnerScore"] . " to " . $round["LoserScore"];
  		echo "</td></tr>";
  	}
?>
  <tr>
    <td class="headerText2" align="center">
    <?
    	echo "|";
    	for ($i = 0; $i < $team2 * 40 - 2; $i ++)
    		echo "-";
    	echo "[--]";
    	for ($i = $team2 * 40 + 2; $i < 40; $i ++)
    		echo "-";
    	echo "|";
    	//[-------------------[--]-------------------]
    ?>    
    </td>
  </tr>
  <tr>
    <td class="headerText2" align="center">-or-</td>
  </tr>
  <tr>
    <td class="headerText2" align="center">
    <?
    	echo "Rape Meter: ";
    	echo "<a class=\"headerText1\">";
    	for ($i = 0; $i <= $ownscale * $scalesize; $i ++)
    		echo "-";
    	echo "</a>";
    	for ($i = $ownscale * $scalesize + 1; $i < $scalesize; $i ++)
    		echo "-";
    	echo "";
    	//[-------------------[--]-------------------]
    ?>    
    </td>
  </tr>
<?
  	foreach ($rounds as $round)
  	{
  		echo "<tr><td align=\"center\">";
  		echo $round["WinNick"] . " vs " . $round["LoseNick"] . " on " . $round["MapName"] . ": " . $round["WinnerScore"] . " to " . $round["LoserScore"];
  		echo "</td></tr>";
  	}
?>
  <tr>
    <td class="headerText2" align="center">-or-</td>
  </tr>
  <tr>
    <td class="headerText2" align="center">
    <?
    	echo "Close Game |";
    	for ($i = 0; $i < $ownscale * $scalesize - 2; $i ++)
    		echo "-";
    	echo "<a class=\"headerText1\">[--]</a>";
    	for ($i = $ownscale * $scalesize + 2; $i < $scalesize; $i ++)
    		echo "-";
    	echo "| Total Rape";
    	//[-------------------[--]-------------------]
    ?>    
    </td>
  </tr>
<?
  	foreach ($rounds as $round)
  	{
  		echo "<tr><td align=\"center\">";
  		echo $round["WinNick"] . " vs " . $round["LoseNick"] . " on " . $round["MapName"] . ": " . $round["WinnerScore"] . " to " . $round["LoserScore"];
  		echo "</td></tr>";
  	}
?>
  </table>
  </td>
</tr>
<?
	}
?>
</table>
<?
			if ($screenshot && isset ($rounds[$screenshot-1]))
			{
				$screenshoturl = $rounds[$screenshot-1]["Screenshot"];
				if ($screenshoturl == null)
				{
					$screenshot = "<b>No Screenshot Available</b>";
				} else
				{
					$screenshot = "<img border=\"1\" src=\"".$screenshoturl."\">";
				}
?>
<br><br>
<table border="0" cellpadding="1" cellspacing="0" align="center" bgcolor="#6D724C">
<tr>
  <td colspan="6" align="center"><? echo $screenshot; ?></td>
</tr>
</table>
<?
			}
			
			$cres = db_query ("select ID, BracketID, Author, Body, IP, date_format(Timestamp, '%H:%i:%s %m/%d/%Y') as Time from pbs_comments where BracketID = $match order by ID");
			if (mysql_error()) echo mysql_error();
?>
<br><br>
<table class="comments_stream" cellspacing="0" width="360" align="center">
<?
			if (!mysql_num_rows ($cres))
				echo "<tr><td align=\"center\">No comments have been posted yet!<br></td></tr>\n";
			else
			while ($comment = mysql_fetch_assoc ($cres))
			{
				echo "<tr><td class=\"header\" style=\"text-align: left\">by <b>{$comment["Author"]}</b></td>";
				echo "<td class=\"header\">";
				echo $comment["Time"];
				echo "</td><td class=\"header\" width=\"25px\"><a href=\"bracket_view_post.php?delComment=" . $comment["ID"] . "\" >[X]</a></td>";
				echo "</tr><tr><td colspan=\"3\" class=\"entry\">";
				echo $comment["Body"];
				echo "</td></tr><tr><td colspan=\"3\">&nbsp;</td></tr>\n";
			}
?>
</table>
<br>
<form action="bracket_view_post.php" method="post">
<input type="hidden" name="addMatchComment" value="1">
<input type="hidden" name="iBracketID" value="<? echo $match; ?>">
<table cellspacing="0" align="center" class="data_list">
<tr><td colspan="2" class="header">Post Comment</td></tr>
<tr>
  <td class="light_header" width="50px">Name:</td><td class="entry"><input type="text" name="iName" maxlength="18" size="61" class="ltext"></td>
</tr>
<tr>
  <td class="light_header">Body:</td><td class="entry"><textarea wrap="hard" rows="10" cols="60" name="iBody"></textarea></td>
</tr>
<tr>
  <td colspan="2" class="empty" align="center"><input type="button" name="Submit" value="Post" class="button" onclick="submit()"></td>
</tr>
</table>
<?
		} // if mysql_num_rows(result)
	}
	
	function getLBSize ($players)
	{
		if ($players < 2 || $players > 128)
			return 0;
		
		$size = 5;
		$x = log($players)/log(2);
		for ($i = 1; $i < $x; $i ++)
			$size = ($size * 2 + 3);
		return $size;
	}
	
	function checkStrFit ($string)
	{
		if (strlen ($string) > 15)
		{
			$string = substr ($string, 0, 13);
			$string .= "..";
		}
		return $string;
	}
	
	function ShortenString( $string, $length )
	{
		if( strlen( $string ) > $length + 2 )
			return substr( $string, 0, $length ) . "..";
		return $string;
	}
	
	function aboutBox()
	{
		global $VERSION_ID, $VERSION_DATE;
		echo<<<EOT
<table border="0" cellpadding="2" cellspacing="2" align="center" width="400">
<tr>
  <td class="td_normal">
  <table border="0" cellpadding="2" cellspacing="2" align="center" width="100%">
  <tr>
    <td>
    Putty's Bracket System [$VERSION_ID] ($VERSION_DATE)<br><br>
    for the people, by the people<br>
    </td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td align="right">
    [<a href="http://www.challenge-tv.com/putty/bracket/">Website</a>]
  </td>
</tr>
</table>
EOT;
	}
	
?>