<html>
<head>
<title>Bracket: Detail</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<?
	require_once "bracket_connect.php";
    require_once "bracket_cfg.php";

	$nColor1	= "#02132F";	// blank
	$nColor2	= "#838862";	// players
	$nColor3	= "#9D7785";	// drop in players
	$nColor4	= "#C4A879";	// connector
	
	extract ($_GET);
	
	if (!isset ($SID))
		$SID = 2;
?>
<body bgcolor="<? echo $nColor1; ?>">
<?
	$query = "select p1.Nick as p1Nick, p2.Nick as p2Nick, LoserPID, WinnerPID, WinnerScore, LoserScore, SequenceNum, MapName "
			."from pbs_players p1, pbs_players p2, pbs_rounds r, pbs_mappool m "
			."where p1.ID = r.WinnerPID and p2.ID = r.LoserPID and r.BracketID = $SID and m.ID = r.MapID "
			."order by SequenceNum";
	
	$result = db_query ($query);
	$rounds = array();
	
	if (!mysql_num_rows ($result)) {
		echo "No Match Information has been Posted yet!<br>";
	} else {
		
		while ($round = mysql_fetch_assoc ($result))
			$rounds []= $round;
?>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="100px">Round</td>
  <td width="100px"><? echo $rounds[0]["p1Nick"]; ?></td>
  <td width="100px"><? echo $rounds[0]["p2Nick"]; ?></td>
  <td width="100px">Map</td>
</tr>
<?
		$prefix = array ("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th", "th");
		$nRound = 0;
		foreach ($rounds as $round)
		{
			$nRound ++;
?>
<tr>
  <td><? echo $nRound . $prefix[$nRound]; ?></td>
  <td><? echo $rounds[$nRound-1]["WinnerScore"]; ?></td>
  <td><? echo $rounds[$nRound-1]["LoserScore"]; ?></td>
  <td><? echo $rounds[$nRound-1]["MapName"]; ?></td>
</tr>
<?
		} // end of foreach
	}// end of else
?>
</table>
</body>
</html>
