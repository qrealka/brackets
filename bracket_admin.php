<?
	require_once "bracket_connect.php";		// mysql connection
    require_once "bracket_cfg.php";
	require_once "bracket_ccode.php";		// shared bracket code
	require_once "bracket_login.php";		// plug-in security ;)
	
	db_init();
	$debug = 0;

    getGameMode();
    getTourneyName();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
<title>Bracket Admin<? if (isset ($ViewMode)) echo " ($ViewMode)"; ?></title>
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>

<?

$nSEPlayers = mysql_result (db_query ("select Value from pbs_config where Label = 'SEPlayers'"), 0);
$nDEPlayers	= mysql_result (db_query ("select Value from pbs_config where Label = 'DEPlayers'"), 0);
$nMPG				= mysql_result (db_query ("select Value from pbs_config where Label = 'MPG'"), 0);
//$sWML   		= trim( mysql_result (db_query ("select Value from pbs_config where Label = 'WinnersMaps'"), 0) );
//$sLML   		= trim( mysql_result (db_query ("select Value from pbs_config where Label = 'LoosersMaps'"), 0) );
$nDERows		= log10($nDEPlayers) / log10(2) * 4 + 1;
$nSERows		= log10 ($nSEPlayers) / log10 (2) * 2 + 1;
$nDBTableOffset = $nSEPlayers * 2;

$ViewMode = "Standing";

extract ($_GET, EXTR_OVERWRITE);
extract ($_POST, EXTR_OVERWRITE);

$nWName = 190;
$nWSpace = 5;

	if (isset ($editTime))
	{
		extract (editTimeData( array_shift( split( ";", $editTime ) ) ));
?>
<script src="bracket_cjscript.js" language="JavaScript" type="text/javascript"></script>
<body onload="javascript: changeMonth (<? echo "$nMonth, $nYear"; ?>); setMinHour (<? echo "$nMin, $nHour"; ?>); dateSelect (<? echo "$nMonth, $nDay, $nYear"; ?>);">
<?
	} else {
		echo "<body>\n";
	}
?>
<form action="<? echo $_SERVER['PHP_SELF']; ?>" method="get">
<table cellpadding="0" cellspacing="0" align="center" class="sexy_buttons">
<tr>
  <td><a class="sexy_button" href="?ViewMode=Signups">Signups</a></td>
<?
    $gm = getGameMode();
	if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
	{
?>
  <td><a class="sexy_button" href="?ViewMode=Teams">Teams</a></td>
<?
	}
?>
  <td><a class="sexy_button" href="?ViewMode=Clips">Clips</a></td>
  <td><a class="sexy_button" href="?ViewMode=Standing">Standing</a></td>
  <td><a class="sexy_button" href="?ViewMode=Seeder">Seeder</a></td>
  <td><a class="sexy_button" href="?ViewMode=Config">Config</a></td>
  <td><a class="sexy_button" href="?ViewMode=MapPool">MapPool</a></td>
  <td><a class="sexy_button" href="?ViewMode=Referees">Referees</a></td>
  <td><a class="sexy_button" href="?ViewMode=Rules">Rules</a></td>
  <td><a class="sexy_button" href="?ViewMode=News">News</a></td>
  <td><a class="sexy_button" href="?action=Logout">Logout</a></td>
</tr>
</table>
</form>
<?
if (isset ($editTime))
{
	echo "<br><br>";
	list ($nSpotID, $nPlayer1, $nPlayer2) = split (";", $editTime);
	editTime ($nSpotID, $nPlayer1, $nPlayer2);
	showHint ("How To Schedule", "The timestamp input field allows you to override and use your own time. "
								."However please maintain the format used within the timestamp.<br><br>"
								."Clear removes time and referee info.");
} else
if (isset ($editSID))
{
	editScores($editSID);
} else
if ($ViewMode == "Seeder")
{
	$aPlayers = array ();
	
	if (!isset ($Sort)) $Sort = "Seeds";
	$order = ($Sort == "Seeds") ? "p.ID" : "b.ID";
	
	// FIX: this is slipery for when u have bracket data, it might be sql version dependent
	$rBracket = db_query ("select b.ID, p.Nick, b.PlayerID from pbs_players p left join pbs_bracket b on (p.ID = b.PlayerID) group by p.ID order by $order");
	if (mysql_error()) showHint ("Error Getting Seed List", mysql_error());
	
	if (mysql_num_rows ($rBracket))
	while ($rPlayer = mysql_fetch_assoc ($rBracket))
			$aPlayers[] = $rPlayer;
	mysql_free_result ($rBracket);
	
?>
<form action="bracket_admin_post.php" method="post">
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td align="center" width="80"><a href="?ViewMode=Seeder&Sort=Spots">Bracket Spot</a></td>
  <td align="center" width="80"><a href="?ViewMode=Seeder&Sort=Seeds">Seed</a></td>
  <td align="center" width="150">Name</td>
  <td width="20" rowspan="<? echo $nSEPlayers + 2; ?>"></td>
  <td width="300" rowspan="<? echo $nSEPlayers + 2; ?>" valign="top">
  <table border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td>
	Player Names in Seed Order
	</td>
  </tr>
  <tr>
    <td>
	<textarea cols="40" rows="<? echo $nSEPlayers; ?>" name="rawPlayerNames"></textarea>
	</td>
  </tr>
  <tr>
	<td align="center">
	  <input class="button" type="submit" name="seedAction" value="Build List">
	</td>
  </tr>
  <tr>
    <td>
	<br>
	allowed formats:<br>
	seed#. playername (ex: 1. apheleon)<br>
	seed#.playername (ex: 1.apheleon)<br>
	playername (ex: apheleon)<br><br>
	if you use 'from mysql' keyword as first line
	[\W\s+\d+\s+]*\W\s+(.+)\s+\W is valid too<br>
    </td>
  </tr>
  </table>
  </td>
</tr>
<pre>
<?
	//print_r ($aPlayers);
	
	foreach ($aPlayers as $Player)
	{
		$nSID = $Player["ID"];
		$nPlayerID = $Player["PlayerID"];
		$sNick = $Player["Nick"];
?>
<tr>
  <td align="center">
    <? echo "$nSID\n"; ?>
  </td>
  <td align="center">
    <? echo "$nPlayerID\n"; ?>
  </td>
  <td align="center">
    <input class="text" type="text" name="sPlayer_<? echo $nPlayerID; ?>" maxlength="64" size="25" value="<? echo $sNick; ?>">
  </td>
</tr>
<?
	}
?>
<tr>
  <td colspan="2">&nbsp;</td>
  <td align="center">
  <input type="hidden" name="nPlayers" value="<? echo $nSEPlayers; ?>">
  <input class="button" type="submit" name="seedAction" value="Update">
  </td>
</tr>
</table>
</form>
<?
} else
if ($ViewMode == "Signups")
{
	//showHint ("Section Offline", "This section surves no purpose atm.<br>Ill make it into something useful later.", 240);
	$query = "select ID, Nick, date_format(Date, '%m/%d/%y %r') as Date, Location, Email, IP, Roster, SeededNum from pbs_signup";
	if (isset ($Sort))
		$query .= " order by $Sort";
	else $Sort = "Nick";
	$rSignUps = db_query ($query);
	$entries = mysql_num_rows ($rSignUps);
	$query = "select Value from pbs_config where Label = 'MaxSignups'";
	$rMaxEntries = db_query($query);
	if (!mysql_num_rows ($rMaxEntries))
		$maxentries = 0;
	else
		$maxentries = mysql_result ($rMaxEntries, 0);
	$entrynumb = 0;
	$printRaw = (isset ($Special) && $Special == "PrintRaw") ? true : false;
	
	if ($printRaw)
	{
?>
<table border="0" cellpadding="2" cellspacing="2" align="center">
<tr>
  <td class="td_unsched">Raw Signups List</td>
</tr>
<tr>
  <td>
    [<a href="?ViewMode=Signups&Special=PrintRaw&Sort=Nick">Order by <? echo $gameTermS; ?></a>]
    [<a href="?ViewMode=Signups&Special=PrintRaw&Sort=Date">Order by Date</a>]
    [<a href="?ViewMode=Signups&Special=PrintRaw&Sort=IP">Order by IP</a>]
  </td>
</tr>
<tr>
  <td>
<?
	$arrSignups = array();
	while ($entry = mysql_fetch_assoc ($rSignUps))
	{
		if ($Sort == "IP")
		{
			$arr = split ("\.", $entry["IP"]);
			if (count($arr) != 4)
				$entry["IP"] = "<a class=\"red\">Not Available</a>";
			else
			{
				for ($i = 0; $i < 4; $i ++)
					$arr[$i] = sprintf("%03d", $arr[$i]);
				$entry["IP"] = join ($arr, ".");
			}
		}
		$arrSignups [] = $entry;
	}
	
	function sortcmpip ($a, $b)
	{
		return ($a["IP"] < $b["IP"]) ? -1 : 1;
	}
	if ($Sort == "IP")
		usort ($arrSignups, "sortcmpip");

	foreach ($arrSignups as $entry)
	{
		if ($Sort == "IP")
			echo "<a class=\"headerText\">".$entry["IP"]."</a> &nbsp;&nbsp; ".$entry["Nick"]."<br>\n";
		else
		if ($Sort == "Date")
			echo "<a class=\"headerText\">".$entry["Date"]."</a> &nbsp;&nbsp; ".$entry["Nick"]."<br>\n";
		else
			echo $entry["Nick"]."<br>\n";
	}
?>
  </td>
</tr>
</table>
<?
	
	} else
	{
?>
<br><br>
<table border="0" cellpadding="2" cellspacing="2" align="center" width="">
<tr align="center">
  <td><a href="?ViewMode=Signups&Sort=Date">Date</a></td>
  <td><a href="?ViewMode=Signups&Sort=Nick"><? echo $gameTermS; ?></a></td>
  <td><a href="?ViewMode=Signups&Sort=Location">Location</a></td>
  <td><a href="?ViewMode=Signups&Sort=Email">Email</a></td>
  <td><a href="?ViewMode=Signups&Sort=IP">IP</a></td>
  <?
    $gm = getGameMode();
  	if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
  		echo "<td><a>Roster</a></td>";
  ?>
  <td>&nbsp;</td>
</tr>
<?
		if (!$entries)
		{
?>
<tr>
  <td class="td_normal" colspan="6">there are currently none signed up.</td>
</tr>
<?
		} else
		while ($entry = mysql_fetch_assoc ($rSignUps))
		{
			$entrynumb ++;
			$arrRoster = split ("<br>", $entry["Roster"]);
			$count = (!strlen ($arrRoster[0])) ? 0 : count ($arrRoster);
?>
<tr>
  <td class="td_normal"><? echo $entry["Date"]; ?></td>
  <td class="td_normal"><? echo $entry["Nick"]; ?></td>
  <td class="td_normal"><? echo $entry["Location"]; ?></td>
  <td class="td_normal"><? echo $entry["Email"]; ?></td>
  <td class="td_normal"><? echo $entry["IP"]; ?></td>
<?
            $gm = getGameMode();
			if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
			{
			  	if ($count)
			  	{
			  		echo "<td class=\"td_unsched\">";
			  		echo array_pop ($arrRoster);
			  		echo "</td>";
			  	} else
			  		echo "<td bgcolor=\"#990000\" align=\"center\">No Roster</td>";
			}
?>
</tr>
<?
			if ($count)
			{
?>
          <tr>
            <td colspan="2" align="center">[<a href="bracket_admin_post.php?signupDel=<? echo $entry["ID"]; ?>">Delete <? echo $gameTermS; ?></a>]</td>
<?
				if ($entry["SeededNum"])
				{	// ask what seed to swap teams with (also moves roster!)
?>
			<td colspan="3" align="center">
			  <form action="bracket_admin_post.php" method="post">
			  <input type="hidden" name="iTeamName" value="<? echo $entry["Nick"]; ?>">
			  <input type="hidden" name="iTeamLocation" value="<? echo $entry["Location"]; ?>">
			  <input type="hidden" name="iTeamData" value="<? echo safeBr2NL($entry["Roster"]); ?>">
			  <input type="hidden" name="iSignupID" value="<? echo $entry["ID"]; ?>">
			  <input type="hidden" name="iSeedNum" value="<? echo $entry["SeededNum"]; ?>">
			  <input type="hidden" name="doTeam" value="Swap">
			  Swap seed from #<? echo $entry["SeededNum"]; ?> to #<input type="text" name="iTeamID" size="3" maxlength="3" class="text">
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="submit" value="Submit" class="button">
			  </form>
			</td>
<?
				} else
				{	// ask where to insert the signup
?>
			<td colspan="3" align="center">
			  <form action="bracket_admin_post.php" method="post">
			  <input type="hidden" name="iTeamName" value="<? echo $entry["Nick"]; ?>">
			  <input type="hidden" name="iTeamLocation" value="<? echo $entry["Location"]; ?>">
			  <input type="hidden" name="iTeamData" value="<? echo safeBr2NL($entry["Roster"]); ?>">
			  <input type="hidden" name="iSignupID" value="<? echo $entry["ID"]; ?>">
			  <input type="hidden" name="doTeam" value="Submit">
			  Insert this <? echo $gameTermS; ?> into seed #<input type="text" name="iTeamID" size="3" maxlength="3" class="text">
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="submit" value="Submit" class="button">
			  </form>
            </td>
<?
				}

                $gm = getGameMode();
				if ($gm != "1v1" && $gm != "HM" && $gm != "FFA")
				{
					if (count ($arrRoster))
					{
						$Player = array_pop ($arrRoster);
						echo "<td class=\"td_unsched\">$Player</td><td></td></tr>";
					}
					if (count ($arrRoster))
					foreach ($arrRoster as $Player)
					{
						echo "<tr><td colspan=\"5\">&nbsp;</td><td class=\"td_unsched\">$Player</td></tr>";
					}
				}
			} else
			{
?>
		  <tr>
		    <td colspan="2" align="center">[<a href="bracket_admin_post.php?signupDel=<? echo $entry["ID"]; ?>">Delete <? echo $gameTermS; ?></a>]</td>
<?
				if ($entry["SeededNum"])
				{	// ask what seed to swap teams with (also moves roster!)
?>
			<td colspan="3" align="center">
			  <form action="bracket_admin_post.php" method="post">
			  <input type="hidden" name="iTeamName" value="<? echo $entry["Nick"]; ?>">
			  <input type="hidden" name="iTeamLocation" value="<? echo $entry["Location"]; ?>">
			  <input type="hidden" name="iTeamData" value="<? echo safeBr2NL($entry["Roster"]); ?>">
			  <input type="hidden" name="iSignupID" value="<? echo $entry["ID"]; ?>">
			  <input type="hidden" name="iSeedNum" value="<? echo $entry["SeededNum"]; ?>">
			  <input type="hidden" name="doTeam" value="Swap">
			  Swap seed from #<? echo $entry["SeededNum"]; ?> to #<input type="text" name="iTeamID" size="3" maxlength="3" class="text">
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="submit" value="Submit" class="button">
			  </form>
			</td>
<?
				} else
				{	// ask where to insert the signup
?>
			<td colspan="3" align="center">
			  <form action="bracket_admin_post.php" method="post">
			  <input type="hidden" name="iTeamName" value="<? echo $entry["Nick"]; ?>">
			  <input type="hidden" name="iTeamLocation" value="<? echo $entry["Location"]; ?>">
			  <input type="hidden" name="iTeamData" value="<? echo safeBr2NL($entry["Roster"]); ?>">
			  <input type="hidden" name="iSignupID" value="<? echo $entry["ID"]; ?>">
			  <input type="hidden" name="doTeam" value="Submit">
			  Insert this <? echo $gameTermS; ?> into seed #<input type="text" name="iTeamID" size="3" maxlength="3" class="text">
			  &nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="submit" value="Submit" class="button">
			  </form>
            </td>
<?
				}
?>
            <td></td>
          </tr>
<?
			}
?>
          <tr>
            <td colspan="6">&nbsp;</td>
          </tr>
<?
		}
?>
</table>
<?
	}
?>
<br><br>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td>
	<form method="POST" action="bracket_admin_post.php">  
	<table class="message_window" cellspacing="0" align="center" width="180">
	<tr>
	  <td colspan="2" class="header">Signups Config</td>
	</tr>
	<tr>
	  <td class="body">
	  <table border="0" cellpadding="0" cellspacing="0" width="100%">
	  <tr>
	    <td align="center">Maximum Entries:</td>
	    <td align="center"><input type="text" maxlength="3" size="5" class="text" name="iMaxSignups" value="<? echo $maxentries; ?>"></td>
	  </tr>
	  <tr>
	    <td colspan="2">&nbsp;</td>
	  </tr>
	  <tr>
	    <td colspan="2" align="center"><input type="submit" name="setMaxSignups" value="Update" class="button"></td>
	  </table>
	  </td>
	</tr>
	</table>
	</form>
  </td>
  <td valign="middle">
    <table border="0" cellpadding="1" cellspacing="1" align="center">
    <tr>
	  <td align="center">
	    Setting Max Entries to 0<br>disables signup list.<br>
	  </td>
	</tr>
	<tr>
	  <td align="center">
	  <form method="GET">
	    <input type="hidden" name="ViewMode" value="Signups">
	    <?
	    	if (!$printRaw)
	    	{
	    ?>
	    <input type="submit" name="Special" value="PrintRaw" class="button">
	    <?
	    	} else {
	    ?>
	    <input type="submit" value="Print Normal" class="button">
	    <?
	    	}
	    ?>
	  </form>
	  </td>
	</tr>
	<tr>
	  <td align="center">
	  <form method="POST" action="bracket_admin_post.php">
	    <input type="submit" name="setSignups" value="ClearList" class="button">
	  </form>
	  </td>
	</tr>
	</table>
  </td>
</tr>
</table>
<?
} else
if ($ViewMode == "Teams") {
?>
<table border="0" cellpadding="1" cellspacing="1" width="800" align="left">
<tr>
  <td width="240" valign="top">
    <br>
    <table border="0" cellpadding="1" cellspacing="1" width="220" align="right">
    <tr>
      <td class="td_sched"><b>Team List</b></td>
    </tr>
      <td class="td_unsched">
    <?
    // for syn :D
    if( ! isset( $TeamID ) ) $TeamID = 1;
    	$res = db_query ("select ID, Nick from pbs_players order by Nick");
    	if (mysql_error()) showHint ("Error Fetching Team List", mysql_error());
    	else
    	{
    		while ($team = mysql_fetch_assoc ($res))
    		{
    			$nick = ShortenString( $team["Nick"], 20 );
    			if( $TeamID == $team["ID"] )
    				echo "<a href=\"?ViewMode=$ViewMode&TeamID=$team[ID]\">&rarr; $nick &larr;</a><br>";
    			else
    				echo "<a href=\"?ViewMode=$ViewMode&TeamID=$team[ID]\">$nick</a><br>";
    		}
    	}
    ?>
      </td>
    </table>
  </td>
  <td valign="top">
    <br>
    <?
    	$rTeamname = db_query ("select Nick, Location from pbs_players where ID = $TeamID");
	$res = mysql_fetch_assoc ($rTeamname);
	$teamName = $res["Nick"];
	$teamLocation = $res["Location"];
	// get the team members
	$rTeams = db_query ("select ID, TeamID, Name, Captain from pbs_teamdata where TeamID = $TeamID order by Name");
	$teamMembers = "";
	while ($teamEntry = mysql_fetch_assoc ($rTeams))
	{
		$teamMembers .= $teamEntry["Name"];
		if ($teamEntry["Captain"] == 'Y')
		$teamMembers .= "+captain";
		$teamMembers .= "\n";
	}
    ?>
    <form action="bracket_admin_post.php" method="post">
    <input type="hidden" name="iTeamID" value="<? echo $TeamID; ?>">
    <table border="0" cellpadding="0" cellspacing="1" align="center">
    <tr>
      <td width="130px">&nbsp;</td>
      <td align="center" class="headerText1">Team/Roster Data Manager</td>
      <td width="150px">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="td_unsched">Team Name:</td>
      <td align="center" class="td_unsched"><input type="text" maxlength="64" size="40" class="text" name="iTeamName" value="<? echo $teamName; ?>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="right" class="td_unsched">Player Names:</td>
      <td align="center" class="td_unsched"><textarea rows="16" cols="40" name="iTeamData" class="text"><? echo $teamMembers; ?></textarea></td>
      <td class="td_unsched" valign="top">
        <table class="format">
        <tr>
          <td align="left">format:<br>name1<br>name2<br>name3+captain<br>name4<br></td>
        </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="right" class="td_unsched">Team Channel:</td>
      <td align="center" class="td_unsched"><input type="text" maxlength="64" size="40" class="text" name="iTeamLocation" value="<? echo $teamLocation; ?>"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
	  <td align="center"><input type="submit" name="doTeam" value="Submit" class="button"></td>
	  <td align="center"><input type="submit" name="doTeam" value="Cancel" class="button"></td>
	  <td align="center"><input type="submit" name="doTeam" value="Delete" class="button"></td>
	</tr>
	</table>
      </td>
      <td>&nbsp;</td>
    </tr>
    </table>
    </form>
    <br>
  </td>
</tr>
<tr>
</tr>
</table>
<?
} else
if ($ViewMode == "Standing") {
	// build double elimination graph ahead of time (we use arrDropIns for loser spot id prediction)
	//timeCode (3, "check");
	list ($DEarrGraph, $arrBackGraph, $arrDropIns) = buildDoubleElim2 ($nDEPlayers);
	$rBracket = db_query ("select b.ID, b.RefID, b.DropInID, b.Timestamp, p.Nick, b.PlayerID from pbs_bracket b left join pbs_players p on b.PlayerID = p.ID order by b.ID");
	if (!mysql_num_rows ($rBracket))
	{
		showHint ("Section Disabled", "bracket has not been seeded with players yet, section disabled.<br>", 240);
		exit;
	}
	$aPlayers = array();
	//timeCode (3);
	$arrLoserToWinner = array();
	
	$nColumns = $nSEPlayers * 2 - 1;
	$nRows = $nSERows;
	
	if (mysql_num_rows ($rBracket))
	{
		while ($rPlayer = mysql_fetch_assoc ($rBracket))
		{
			$aPlayers[$rPlayer["ID"]] = $rPlayer;
			$arrLoserToWinner[$rPlayer["DropInID"]] = $rPlayer["ID"];
		}
	}
	
	showHint ("Match Scheduling", "(s) = is scheduled (r) = has a referee assigned", 300, "center");
?>
<form action="bracket_admin_post.php" method="post">
<br><br>
<div align="center" class="headerText">Winners Bracket</div>
<table style="table-layout: fixed;" cellpadding="1" cellspacing="1">
<?
	$arrGraph = array();
	
	$bColor = array ();
	for ($i = 0; $i < $nSERows; $i ++)
	$bColor []= false;
	
	$powlookup = array (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768);
	//if (isset ($compact)) $powlookup[0] = 0;
	
	for ($i = 0; $i < $nSEPlayers * 2 - 1; $i ++)
	{
		$m = 0;
		$sub = 0;
		$sub2 = 0;
		for ($j = 0; $j < $nSERows; $j ++)
		{
			$p = $powlookup[$j+1];
			$arrGraph[$i * $nSERows + $j] = 0;
			$bWas = false;
			if ($j % 2 == 0)
			{
				if (($i-$sub) % $powlookup[$j/2+1] == 0)
				$arrGraph[$i * $nSERows + $j] = 1;
				$sub += $powlookup[$j/2];
			} else
			{
				if (($i-$sub2) % $powlookup[($j-1)/2+1] == 0)
				{
					$bWas = $bColor[$j];
					$bColor[$j] = !$bColor[$j];
				}
				if ($bColor[$j] || $bWas)
				$arrGraph[$i * $nSERows + $j] = 2;
				$sub2 += $powlookup[($j-1)/2];
			}
		}
	}
	
	$nLoserID = 0;
	$nPlayerID = 0;
	$WBStandingString = "";

	for ($i = 0; $i < $nColumns; $i ++)
	{
		echo "<tr>";
		for ($j = 0; $j < $nRows; $j ++)
		{
			$bHasText	= false;
			$bHasOption	= false;
			$bHasTime	= false;
			$bHasRef	= false;
			
			if ($arrGraph[$i * $nRows + $j] == 1)
			{
				$nPlayerID ++;
				$sClass = "td_unsched";
				$bHasText = true;
				$width = $nWName;
				
				$nLoserID = $aPlayers[$nPlayerID]["DropInID"];
				
				if ($j && !($aPlayers[$nPlayerID]["PlayerID"]))
				{
					$nPlayer1 = $nPlayerID - pow (2, $j/2 - 1);
					$nPlayer2 = $nPlayerID + pow (2, $j/2 - 1);
					
					if (($aPlayers[$nPlayer1]["PlayerID"]) && ($aPlayers[$nPlayer2]["PlayerID"])) {
						$bHasOption = true;
						if ($nLoserID)
							$WBStandingString .= "$nPlayerID/$nLoserID,";
						else
							$WBStandingString .= "$nPlayerID/0,";
					}
					
					if ($aPlayers[$nPlayerID]["Timestamp"] > 0)
					{
						$bHasTime = true;
						$sClass = "td_sched";
					}
					else
						$bHasTime = false;
					
					if ($aPlayers[$nPlayerID]["RefID"] > 0)
						$bHasRef = true;
					
					if ($bHasRef && $bHasTime)
						$sClass = "td_sched";
					
				} else $sClass = "td_played";
				// special color for first row
				if (!$j) $sClass = "";
			} else if ($arrGraph[$i * $nRows + $j] == 2) {
				$sClass = "td_br";
				$width = $nWSpace;
			} else {
				$sClass = 0;
				$width = ($j % 2) ? $nWSpace : $nWName;
			}
	
			//if (!$j) $width = 0;
		
			//$align = ($j) ? "center" : "left";
			if( ! $j ) $sClass = "td_input";
			echo "<td";
			//echo " class=\"$sClass\" width=\"$width\"";
			if ($sClass) echo " class=\"$sClass\"";
			if ($width) echo " width=\"$width\"";
			//echo " align=\"$align\"";	
			if ($bHasText && $j) echo " class=\"$sClass\"";
			echo ">";

			if ($bHasText && !$j)
			{
				echo "<table cellspacing=\"0\">";
				echo "<tr><td>(".$aPlayers[$nPlayerID]["PlayerID"].")</td><td align=\"right\">";
				echo "<input class=\"text\" type=\"text\" name=\"sNames_".$aPlayers[$nPlayerID]["PlayerID"]."\" maxlength=\"64\" size=\"25\" value=\"";
				echo ($aPlayers[$nPlayerID]["PlayerID"]) ? $aPlayers[$nPlayerID]["Nick"] : "-TBA-";
				echo "\">";
				echo "</td></tr></table>";
			} else
			{
				if ($bHasOption)
				{
					$sNick0 = "(".$nPlayer1.") ". ShortenString( $aPlayers[$nPlayer1]["Nick"], 12 );
					$sNick1 = "(".$nPlayer2.") ". ShortenString( $aPlayers[$nPlayer2]["Nick"], 12 );
					
					echo "<table cellspacing=\"0\">";
					echo "<tr><td>";
					echo "<select name=\"sWBracket_$nPlayerID\" size=\"1\">";
					echo "<option value=\"0\">-None-</option>";
					echo "<option value=\"".$aPlayers[$nPlayer1]["PlayerID"]."/".$aPlayers[$nPlayer2]["PlayerID"]."\">".$sNick0."</option>";
					echo "<option value=\"".$aPlayers[$nPlayer2]["PlayerID"]."/".$aPlayers[$nPlayer1]["PlayerID"]."\">".$sNick1."</option>";
					echo "</select>";
				    echo "</td>";
				    echo "<td class=\"right\">";
			
					if ($bHasTime) echo "(s)";
					if ($bHasRef) echo "(r)";
					
					echo "[<a href=\"bracket_admin.php?editTime=$nPlayerID;".$aPlayers[$nPlayer1]["PlayerID"].";".$aPlayers[$nPlayer2]["PlayerID"]."\">Edit</a>]";
				    echo "</td></tr></table>";
				} else
				if ($bHasText && $aPlayers[$nPlayerID]["PlayerID"])
				{
					echo "<table cellspacing=\"0\">";
					echo "<tr><td class=\"td_played_text\">";
					echo "(".$aPlayers[$nPlayerID]["PlayerID"].") <a title=\"{$aPlayers[$nPlayerID]["Nick"]}\">". ShortenString( $aPlayers[$nPlayerID]["Nick"], 12 ) . "</a>";
				  	echo "</td><td align=\"right\">";
				  	echo "[<a href=\"bracket_admin.php?editSID=$nPlayerID\" title=\"Edit\">E</a>]";
				  	echo "[<a href=\"bracket_admin_post.php?delSID=$nPlayerID/$nLoserID\" title=\"Delete\">Del</a>]";
				  	echo "</td></tr></table>";
				} else
				if ($bHasText)
				{
					if ($debug)
						echo $nPlayerID . "($nPlayer1 vs $nPlayer2) DropTo: $nLoserID";
					else
						echo "&nbsp;";
				} else
				{
					echo "&nbsp;";
				}
			}
			echo "</td>";
		}
		echo "</tr>";
	}
	echo "</table><br>";

	$arrGraph = $DEarrGraph;
	$nColumns = $nDEPlayers * 2 + 1;
	$nRows = $nDERows;//log10($nPlayers)/log10(2) * 4 + 1;//$numRows;//$nPlayers * 2 + 1;
	$abCon = array ();
	$abWas = array ();
	
	for ($i = 0; $i < $nRows; $i ++) {
		$abCon[$i] = false;
		$abWas[$i] = false;
	}
	
	$aDoubleElimID = array();
	
	$nPlayerID = $nDBTableOffset;
	$nWidth = 0;
	$arrIDtoPlayerID = array();
	for ($i = 0; $i < $nColumns; $i ++)
	{
		for ($j = 0; $j < $nRows; $j ++)
		{
			$id = $j * $nColumns + $i;
			
			if ($arrGraph[$id] == 1) {	// normal winner
				$nPlayerID ++;
				$arrIDtoPlayerID[$id] = $nPlayerID;
			} else if ($arrGraph[$id] == 2) {	// drop in from loser bracket
				$nPlayerID ++;
				//echo "nPlayerID = $nPlayerID<br>";
				$arrIDtoPlayerID[$id] = $nPlayerID;
			}
		}
	}
	
	$nPlayerID = $nDBTableOffset;
	$LBStandingString = "";
	if ($nDEPlayers)
	{
?>
<div align="center" class="headerText">Losers Bracket: Losers vs New Losers</div>
<table style="table-layout: fixed;" cellpadding="1" cellspacing="1">
<?
		$nSpotIDLoser = 0;
		//echo "<pre>";
		//print_r ($arrLoserToWinner);
		for ($i = 0; $i < $nColumns; $i ++)
		{
			echo "<tr>";
			for ($j = 0; $j < $nRows; $j ++)
			{
				$id = $j * $nColumns + $i;
				
				$bHasText		= false;
				$bHasOption		= false;
				$bHasNonMovable	= false;
				$bIsTBA			= false;
				$bWas[$j] = $abCon[$j];
				
				if ($arrGraph[$id] == 1)
				{	// normal winner
					$nPlayerID ++;
					$sClass		= "td_unsched";
					$abCon[$j]	= !$abCon[$j];
					$nWidth		= $nWName;
					$bHasText	= true;
					$bHasRef	= false;
					$bHasTime	= false;
					$bHasOption = false;
					
					if (!$aPlayers[$nPlayerID]["PlayerID"]["PlayerID"])
					{
						if (($aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][0]]]["PlayerID"]) &&
						($aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][1]]]["PlayerID"]))
						{
							$bHasOption = true;
							$LBStandingString .= "$nPlayerID,";
							$nPlayerID0 = $aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][0]]]["PlayerID"];
							$nPlayerID1 = $aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][1]]]["PlayerID"];
							$sNick0 = $aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][0]]]["Nick"];
							$sNick1 = $aPlayers[$arrIDtoPlayerID[$arrBackGraph[$id][1]]]["Nick"];
						} else
						{
							if ($debug)
								$Nick = $nPlayerID . "(".$arrIDtoPlayerID[$arrBackGraph[$id][0]]." vs ".$arrIDtoPlayerID[$arrBackGraph[$id][1]].")";
							else
								$Nick = "&nbsp;";
							$bIsTBA = true;
						}
							
						if ($aPlayers[$nPlayerID]["Timestamp"] > 0)
							$bHasTime = true;
						else
							$bHasTime = false;
									
						if ($aPlayers[$nPlayerID]["RefID"])
							$bHasRef = true;
									
						if ($bHasRef && $bHasTime)
							$sClass = "td_sched";
					} else
					{
						$sClass = "td_played";
						$Nick = "(".$aPlayers[$nPlayerID]["PlayerID"].") <a title=\"{$aPlayers[$nPlayerID]["Nick"]}\">". ShortenString( $aPlayers[$nPlayerID]["Nick"], 12 ) . "</a>";
					}
					
					
					if ($j == $nRows - 1)
						$nSpotIDLoser = $nPlayerID;
		
				} else
				if ($arrGraph[$id] == 2)
				{	// drop in from loser bracket
					$nPlayerID ++;
					$sClass = "td_dropin";
					$nWidth = $nWSpace;
					$abCon[$j] = !$abCon[$j];
					$nWidth = $nWName;
					//echo "\$id = $id<br>\n";
					//echo "\$nPlayerID = $nPlayerID<br>\n";
					if ($aPlayers[$nPlayerID]["PlayerID"])
					{
						$Nick = "(".$aPlayers[$nPlayerID]["PlayerID"].") <a title=\"{$aPlayers[$nPlayerID]["Nick"]}\">". ShortenString( $aPlayers[$nPlayerID]["Nick"], 12 ) . "</a>";
					} else
					{
						if ($debug)
							$Nick = "Drop ID: $nPlayerID";
						else
							$Nick = "&nbsp;";
						$bIsTBA = true;
					}
				
					$bHasText = true;
					$bHasNonMovable = true;
				} else
				{
					$sClass = 0;
					$nWidth = ($j % 2) ? $nWSpace : $nWName;
				}
			
				if ($j % 2 == 1)
				{
					$nWidth = $nWSpace;
					if ($bWas[$j - 1] || $abCon[$j - 1])
					{
						$sClass = "td_br";
						$bWas[$j - 1] = false;
					}
				}
				
				echo "<td";
				if ($sClass) echo " class=\"$sClass\"";
				echo " width=\"$nWidth\">";
				if ($bHasOption)
				{
					$sNick0 = ShortenString( $sNick0, 15 );
					$sNick1 = ShortenString( $sNick1, 15 );
					
					echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
					echo "<tr><td>";
					echo "<select name=\"sLBracket_$nPlayerID\" size=\"1\">";
					echo "<option value=\"0\">-None-</option>";
					echo "<option value=\"$nPlayerID0/$nPlayerID1\">($nPlayerID0) $sNick0</option>";
					echo "<option value=\"$nPlayerID1/$nPlayerID0\">($nPlayerID1) $sNick1</option>";
					echo "</select>";
					echo "</td><td align=\"right\">";
					if ($bHasTime) echo "(s)";
					if ($bHasRef) echo "(r)";
					echo "[<a href=\"bracket_admin.php?editTime=$nPlayerID;$nPlayerID0;$nPlayerID1\">Edit</a>]";
					echo "</td></tr></table>";
				} else
				if ($bHasText)
				{
					if ($bIsTBA)
					{
						echo $Nick;
					} else
					if ($bHasNonMovable)	// drop in from winner bracket
					{
						echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
						echo "<tr><td class=\"td_played_text\">";
						echo $Nick;
				  		echo "</td><td align=\"right\" class=\"td_played_text\">";
			        	echo "[<a href=\"bracket_admin.php?editSID=".$arrLoserToWinner[$nPlayerID]."\" title=\"Edit\">E</a>]";
			        	echo "[<a href=\"bracket_admin_post.php?delSID=".$arrLoserToWinner[$nPlayerID]."/$nPlayerID\" title=\"Delete\">Del</a>]";
				 		echo "</td></tr></table>";
					} else
					{
						echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
						echo "<tr><td class=\"td_played_text\">";
						echo $Nick;
				  		echo "</td><td align=\"right\">";
				  		echo "[<a href=\"bracket_admin.php?editSID=$nPlayerID\" title=\"Edit\">E</a>]";
			        	echo "[<a href=\"bracket_admin_post.php?delSID=$nPlayerID\" title=\"Delete\">Del</a>]";
				 		echo "</td></tr></table>";
					}
				} else
				{
					echo "&nbsp;";
				}
				echo "</td>";
			}
			echo "</tr>";
		}
		echo "<tr></tr></table>";
	
		$nSpotIDWinner = $nSEPlayers;
		$nSpotIDFinal = $nPlayerID + 1;
		if ($nSpotIDLoser && $nSpotIDWinner)
		if ($aPlayers[$nSpotIDWinner]["PlayerID"] && $aPlayers[$nSpotIDLoser]["PlayerID"])
		{			
			// im usin loser bracket handling to add final winner to winner bracket
			// in order to avoid creating a loser bracket entry (im lazy)
			$nPlayerID0 = $aPlayers[$nSpotIDWinner]["PlayerID"];
			$nPlayerID1 = $aPlayers[$nSpotIDLoser]["PlayerID"];
			$sNick0		= "<a title=\"{$aPlayers[$nSpotIDWinner]["Nick"]}\">" . ShortenString( $aPlayers[$nSpotIDWinner]["Nick"], 20 ) . "</a>";
			$sNick1		= "<a title=\"{$aPlayers[$nSpotIDLoser]["Nick"]}\">" . ShortenString( $aPlayers[$nSpotIDLoser]["Nick"], 20 ) . "</a>";
			
			if ($aPlayers[$nSpotIDFinal]["Timestamp"] > 0)
				$bHasTime = true;
			else
				$bHasTime = false;
									
			if ($aPlayers[$nSpotIDFinal]["RefID"])
				$bHasRef = true;
			else
				$bHasRef = false;
			
			if ($aPlayers[$nSpotIDFinal]["PlayerID"])
				$sClass = "td_played";
			else
			if ($bHasTime)
				$sClass = "td_sched";
			else
				$sClass = "td_unsched";
?>
<div align="center" class="headerText">Final Match: Winner Bracket vs Loser Bracket</div>
<table style="table-layout: fixed;" cellpadding="1" cellspacing="1">
<tr>
  <td width="<? echo $nWName; ?>" class="td_played">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td class="td_played_text"><? echo "($nPlayerID0) " . $sNick0; ?></td>
  </tr>
  </table>
  </td>
  <td width="<? echo $nWSpace; ?>" class="td_br">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>">&nbsp;
  </td>
</tr>
<tr>
  <td width="<? echo $nWName; ?>">&nbsp;
  </td>
  <td width="<? echo $nWSpace; ?>" class="td_br">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>" class="<? echo $sClass;?>">
  <?
  if ($aPlayers[$nSpotIDFinal]["PlayerID"])
  {
  	?>
  	<table border="0" cellpadding="0" cellspacing="0" width="100%">
  	<tr>
  	  <td class="td_played_text"><? echo "(".$aPlayers[$nSpotIDFinal]["PlayerID"].") <a title=\"{$aPlayers[$nSpotIDFinal]["Nick"]}\">". ShortenString( $aPlayers[$nSpotIDFinal]["Nick"], 12 ) . "</a>"; ?></td>
  	  <td align="right"><? echo " [<a href=\"bracket_admin.php?editSID=$nSpotIDFinal\">E</a>] [<a href=\"bracket_admin_post.php?delSID=$nSpotIDFinal\">Del</a>]"; ?></td>
  	</tr>
  	</table>
  	<?
  }
  else
  if (isset ($sNick0) && isset ($sNick1))
  {
  	$LBStandingString .= "$nSpotIDFinal";
	?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	  <td>
		<select name="sLBracket_<? echo $nSpotIDFinal; ?>" size="1">
		  <option value="0">-None-</option>
		  <option value="<? echo "$nPlayerID0/$nPlayerID1"; ?>"><? echo "$nPlayerID0. $sNick0"; ?></option>
		  <option value="<? echo "$nPlayerID1/$nPlayerID0"; ?>"><? echo "$nPlayerID1. $sNick1"; ?></option>
		</select>
	  </td>
	  <td align="right">
	  	<?
	  		if ($bHasTime) echo "(s)";
			if ($bHasRef) echo "(r)";
		?>
        [<a href="bracket_admin.php?editTime=<? echo "$nSpotIDFinal;$nPlayerID0;$nPlayerID1"; ?>">Edit</a>]
	  </td>
	</tr>
	</table>
	<?
  } else echo "-TBA-";
  ?>
  </td>
</tr>
<tr>
  <td width="<? echo $nWName; ?>" class="td_played">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td class="td_played_text"><? echo "($nPlayerID1) " . $sNick1; ?></td>
  </tr>
  </table>
  </td>
  <td width="<? echo $nWSpace; ?>" class="td_br">&nbsp;
  </td>
  <td width="<? echo $nWName; ?>">&nbsp;
  </td>
</tr>
</table>
<hr>
<?
		}
	}

	$WBStandingString = rtrim ($WBStandingString, ",");
	$LBStandingString = rtrim ($LBStandingString, ",");
    getTourneyName();
?>
<input type="hidden" name="tourneyName" value="<? echo $tourneyName; ?>">
<input type="hidden" name="nSEPlayers" value="<? echo $nSEPlayers; ?>">
<input type="hidden" name="nDEPlayers" value="<? echo $nDEPlayers; ?>">
<input type="hidden" name="nMPG" value="<? echo $nMPG; ?>">
<input type="hidden" name="gameMode" value="<? echo $gameMode; ?>">
<input type="hidden" name="WBStandingString" value="<? echo $WBStandingString; ?>">
<input type="hidden" name="LBStandingString" value="<? echo $LBStandingString; ?>">
<input class="button" type="submit" name="Action" value="Set Names">
<input class="button" type="submit" name="Action" value="Set Standings">
</form>
<hr>
<?
} else if ($ViewMode == "Clips") {
	// get the unplayed matches
	$query = "select b.ID, b.PlayerID, b.Opponent1, b.Opponent2, date_format(b.Timestamp, '%Y-%m-%d %H:%i:%s') as Time, p1.Nick as p1Nick, p2.Nick as p2Nick "
			."from pbs_bracket b, pbs_players p1, pbs_players p2 "
			."where (p1.ID = b.Opponent1) and (p2.ID = b.Opponent2) and b.PlayerID = 0";
	$res = db_query ($query);
	if (mysql_error()) echo mysql_error();
?>
<script language="JavaScript" type="text/javascript">
function clip (announcement)
{
	window.clipboardData.setData("text", announcement);
}
</script>
<br>
<table border="0" cellpadding="2" cellspacing="2" align="center">
<tr>
  <td colspan="6" class="td_unsched">Scheduled Matches List</td>
</tr>
<?
	if (!mysql_num_rows($res))
	{
		echo "<tr><td>there are currently no matches scheduled</td></tr>\n";
	} else
	while ($row = mysql_fetch_assoc ($res))
	{
		$string = $row["p1Nick"] . " vs " . $row["p2Nick"] . " at " . $row["Time"];
		echo "<tr><td>$row[p1Nick]</td><td>&nbsp;vs&nbsp;</td><td>$row[p2Nick]</td><td>&nbsp;at&nbsp;</td><td>$row[Time]</td><td><b><a href=\"javascript:clip('$string')\">Clip!</a></b></td></tr>\n";
	}
?>
</table>
<br>
<?
	showHint ("Clipping", "Clip automatically copies the string to the clipboard so u can paste it in irc or w/e", 300, "center");
} else if ($ViewMode == "Config") {
?>
<form action="bracket_admin_post.php" method="post">
<br>
<table class="message_window" cellspacing="0" align="center" width="250px">
<tr><td colspan="2" class="header">Bracket Size Config</td></tr>
<tr>
  <td align="center">Tourney Name:</td>
  <td><input class="text" type="text" name="tourneyName" value="<? echo $tourneyName; ?>" size="5" maxlength="10"></td>
</tr>
<tr>
  <td align="center">Winner Bracket Size:</td>
  <td><input class="text" type="text" name="nSEPlayers" value="<? echo $nSEPlayers; ?>" size="3" maxlength="3"></td>
</tr>
<tr>
  <td align="center">Loser Bracket Size:</td>
  <td><input class="text" type="text" name="nDEPlayers" value="<? echo $nDEPlayers; ?>" size="3" maxlength="3"></td>
</tr>
<tr>
  <td align="center">Maps Per Game:</td>
  <td><input class="text" type="text" name="nMPG" value="<? echo $nMPG; ?>" size="2" maxlength="2"></td>
</tr>
<tr>
  <td align="center">Game Mode:</td>
  <td><input class="text" type="text" name="gameMode" value="<? echo $gameMode; ?>" size="4" maxlength="4"></td>
</tr>
<tr>
  <td colspan="2" align="center">
    <input class="button" type="submit" name="Action" value="Rename">
    <input class="button" type="submit" name="Action" value="Build Bracket">
    <input class="button" type="submit" name="Action" value="Resize">
  </td>
</tr>
</table>
</form>
<br>
<?
	showHint ("Bracket Config Info", "<u>Build Bracket</u> - formats the bracket, erases all data<br><br><u>Resize</u> - resizes double elimination bracket and relinks it to single elimination bracket without losing single elimination bracket information (do this BEFORE double elimination starts :)");
?>
<br><br><br>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="480">
<tr>
  <td colspan="2">
    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
    <tr>
      <td><hr noshade size="1" align="right" width="120"></td>
      <td class="headerText3" align="center">Automated Bracket Removal Options</td>
      <td><hr noshade size="1" align="left" width="120"></td>
    </tr>
    </table>
  </td>
</tr>
<tr>
  <td colspan="2" class="headerText2"><b>DB Remove:</b> [<a href="bracket_admin_post.php?remove=DBOnly">Proceed</a>]</td>
</tr>
<tr>
  <td width="50">&nbsp;</td>
  <td>
    <b>R</b>emoves all bracket data (SQL tables).<br>
    <b>D</b>oes not delete any demos, screenshot, or bracket files.<br>
    <b>I</b>f there are no tables left in the database, the database is dropped.<br>
    <b>A</b>fter doing this bracket needs to be reinstalled before its usable again.<br><br>
  </td>
</tr>
<tr>
  <td colspan="2" class="headerText2"><b>Full Remove:</b> [<a href="bracket_admin_post.php?remove=Full">Proceed</a>]</td>
</tr>
<tr>
  <td width="50">&nbsp;</td>
  <td>
    <b>R</b>emoves all bracket data (SQL tables).<br>
    <b>D</b>eletes all demos and screenshots.<br>
    <b>I</b>f there are no tables left in the database, the database is dropped.<br>
    <b>O</b>nly files left are the bracket_* .php/.css files for your reinstalling pleasure.
  </td>
</tr>
</table>
<?
} else
if ($ViewMode == "MapPool")
{
	if (isset ($AddMap) || isset ($Edit))
	{
		$mapAction = (isset ($AddMap)) ? "Add Map" : "Update Map";
		
		if (isset ($Edit)) {
			$result = db_query ("select ID, MapName, URL, Description, MapImage from pbs_mappool where ID = $Edit");
			$mapInfo = mysql_fetch_assoc ($result);
		} else
		$mapInfo = array ("ID" => 0, "MapName" => "", "URL" => "", "Description" => "No Description", "MapImage" => "none.jpg");
?>
<br>
<form action="bracket_admin_post.php" method="post">
<input type="hidden" name="mapAction" value="<? echo $mapAction; ?>">
<input type="hidden" name="iMapID" value="<? echo $mapInfo["ID"]; ?>">
<table border="0" cellpadding="1" cellspacing="2" align="center">
<tr>
  <td>Map Name:</td><td><input class="text" type="text" size="24" maxlength="64" name="iMapName" value="<? echo $mapInfo["MapName"]; ?>"></td>
  <td rowspan="3">
    <table border="0" cellpadding="1" cellspacing="1">
	<tr>
	  <td class="td_unsched">Map Description:</td>
	</tr>
	<tr>
      <td><textarea rows="10" cols="50" name="iDescription"><? echo $mapInfo["Description"]; ?></textarea></td>
	</tr>
	</table>
  </td>
</tr>
<tr>
  <td>Map Image:</td><td><input class="text" type="text" size="24" maxlength="64" name="iMapImage" value="<? echo $mapInfo["MapImage"]; ?>"></td>
</tr>
<tr>
  <td>Map URL:</td><td><input class="text" type="text" size="24" maxlength="64" name="iMapURL" value="<? echo $mapInfo["URL"]; ?>"></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  <td colspan="1" align="center"><input class="button" type="submit" name="addMap" value="<? echo $mapAction; ?>"></td>
</tr>
</table>
</form>
<br>
<table border="0" cellpadding="2" cellspacing="1" align="center">
<?
	$fcount = 1;
	$perrow = 5;
	$colours = array_reverse (array ("111111", "222222", "333333", "444444", "555555", "666666")); 
?>
<tr>
  <td colspan="<? echo $perrow; ?>" class="td_unsched" align="center">file contents of map_pics/</td>
</tr>
<tr>
<?
	if ($dirhandle = @opendir ("map_pics"))
	{
		while (($file = readdir ($dirhandle)))
		{
			if (!is_file ("map_pics/".$file)) continue;
			//if ($file == "." || $file == "..")
			echo "<td class=\"td_normal\">$file</td>";
			if ($fcount++ % $perrow == 0) echo "</tr>\n<tr>";
		}
		closedir ($dirhandle);
		if ($fcount++ % $perrow == 0) echo "<td>&nbsp;</td>";
	} else echo "<td colspan=\"$perrow\">error opening map_pics folder</td>";
?>
</tr>
</table>
<?
	} else		// else list the maps and show options
	{
		$query = "select ID, MapName, MapImage, URL, Description from pbs_mappool";
		$result = db_query ($query);
?>
<br>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<?	
while ($mapInfo = mysql_fetch_assoc ($result))
{
	// if the pic doesnt exist make it show none.jpg for now (maybe admin will upload new pic later)
	$note = 0;
	if (!file_exists("map_pics/".$mapInfo["MapImage"])) {
		$mapInfo["MapImage"] = "none.jpg";
		$note = "<br><br><a class=\"red\">map pic was not found!</a>";
	}
?>
<tr>
  <td colspan="1" class="td_unsched" align="center"><? echo $mapInfo["MapName"]; ?></td>
  <td colspan="1" class="td_unsched" align="center"><u>Description</u></td>
  <td colspan="2" class="td_dropin" align="center"><u>Actions</u></td>
</tr>
<tr>
  <td class="td_unsched">
    <img src="map_pics/<? echo $mapInfo["MapImage"]; ?>" border="1" class="td_sched"><br>
  </td>
  <td width="200px">
    <? echo $mapInfo["Description"]; if ($note) echo $note; ?>
  </td>
  <td>[<a href="?ViewMode=MapPool&Edit=<? echo $mapInfo["ID"]; ?>">Edit</a>]</td>
  <td>[<a href="bracket_admin_post.php?delMap=<? echo $mapInfo["ID"]; ?>">Delete</a>]</td>
</tr>
<tr>
  <td colspan="4">&nbsp;</td>
</tr>
<?
} // end of while (mapinfo = fetch())
?>
<tr>
  <td colspan="4" align="center">
    [<a href="?ViewMode=MapPool&AddMap=yep">Add Map</a>]
  </td>
</tr>
</table>
<?
	}
} else
if ($ViewMode == "Referees")
{
	
	$buttonText = (!isset ($EditRef)) ? "Add Referee" : "Update Ref";
	$refAction = (!isset ($EditRef)) ? "addReferee" : "updateReferee";
	$refName = "";
	$refLogin = "";
	//if (isset ($EditRef))
	//$refName = mysql_result (db_query ("select RefName from pbs_referees where ID = $EditRef"), 0);
	
	//$rReferees = db_query ("select ID, RefName from pbs_referees");
	$query = "select r.ID, r.Login, r.RefName, p1.Nick as p1Nick, p2.Nick as p2Nick, b.Opponent1, b.Opponent2, "
			."if(b.ID is null, 0, b.ID) as BID, if(b.Timestamp, date_format(b.Timestamp, '%m/%d/%Y @ %H:%i:%s'), 0) as Time "
			."from pbs_referees r left join pbs_bracket b on (r.ID = b.RefID and b.PlayerID = 0) "
			."left join pbs_players p1 on p1.ID = b.Opponent1 "
			."left join pbs_players p2 on p2.ID = b.Opponent2";
	$rReferees = db_query ($query);
	if (mysql_error()) echo mysql_error();
	
	$arrRefInfo = array();
	while ($refInfo = mysql_fetch_assoc ($rReferees))
		$arrRefInfo[$refInfo["ID"]] []= $refInfo;
	
	mysql_free_result ($rReferees);
	
	//echo "<pre>";
	//print_r ($arrRefInfo);
	
?>
<br><br>
<table border="0" cellpadding="1" cellspacing="1" width="520" align="center">
<tr>
  <td class="td_unsched" align="center">Referee</td>
  <td class="td_unsched" align="center">Assignments</td>
  <td class="td_unsched" align="center">Scheduled</td>
  <td class="td_unsched" align="center">Actions</td>
</tr>
<?
foreach ($arrRefInfo as $referee)
{	// each referee will have atleast one entry, even if no assignments were made to him
	$numAssignments = count ($referee);
	$assignment = array_shift ($referee);
	$refLogin = $assignment["Login"];
	$refName = $assignment["RefName"];
	if ($assignment["p1Nick"] == NULL) {
		$matchTitle = "None";
		$matchSched = "N/A";
	} else {
		$matchTitle = "<a href=\"bracket_admin.php?editTime=$assignment[BID];$assignment[Opponent1];$assignment[Opponent2]\">".$assignment["p1Nick"] . " vs " . $assignment["p2Nick"] . "</a>";
		$matchSched = ($assignment["Time"]) ? $assignment["Time"] : "No";
	}

?>
<tr>
  <td rowspan="<? echo $numAssignments; ?>" valign="top" align="center"><? echo $assignment["RefName"]; ?></td>
  <td align="center"><? echo $matchTitle; ?></td>
  <td align="center"><? echo $matchSched; ?></td>
  <td rowspan="<? echo $numAssignments; ?>" valign="top" align="center">[<a href="?ViewMode=Referees&EditRef=<? echo $assignment["ID"]; ?>">Edit</a>][<a href="bracket_admin_post.php?delReferee=<? echo $assignment["ID"]; ?>">Delete]</a></td>
</tr>
<?
	foreach ($referee as $assignment)
	{
		if ($assignment["p1Nick"] == NULL) {
			$matchTitle = "None";
			$matchSched = "N/A";
		} else {
			$matchTitle = "<a href=\"bracket_admin.php?editTime=$assignment[BID];$assignment[Opponent1];$assignment[Opponent2]\">".$assignment["p1Nick"] . " vs " . $assignment["p2Nick"] . "</a>";
			$matchSched = ($assignment["Time"]) ? $assignment["Time"] : "No";
		}
?>
<tr>
  <td align="center"><? echo $matchTitle; ?></td>
  <td align="center"><? echo $matchSched; ?></td>
</tr>
<?
	}
}

$refName = "";
$refLogin = "";
$refAdmin = 0;

if (isset ($EditRef))
{
	$sqlresult = db_query ("select RefName, Login, bIsAdmin from pbs_referees where ID = $EditRef");
	$refInfo = mysql_fetch_assoc ($sqlresult);
	$refName = $refInfo["RefName"];
	$refLogin = $refInfo["Login"];
	$refAdmin = ($refInfo["bIsAdmin"] == 'Y') ? true : false;
}
?>
<tr>
  <td colspan="4" align="center"><hr noshade size="1" width="100%"></td>
</tr>
<tr>
  <td colspan="4">
	<form action="bracket_admin_post.php" method="post">
	<input type="hidden" name="<? echo $refAction; ?>" value="1">
	<? if (isset ($EditRef)) { ?>
	<input type="hidden" name="iRefID" value="<? echo $EditRef; ?>">
	<? } ?>
	<table border="0" cellpadding="2" cellspacing="1" align="center">
	<tr>
	  <td class="" align="right">Referee Name:</td>
	  <td class="" align="center"><input type="text" name="iRefName" maxlength="64" size="24" class="text" value="<? echo $refName; ?>"></td>
	</tr>
	<tr>
	  <td class="" align="right">Login:</td>
	  <td class="" align="center"><input type="text" name="iRefLogin" maxlength="32" size="24" class="text" value="<? echo $refLogin; ?>"></td>
	</tr>
	<tr>
	  <td class="" align="right">Passwd:</td>
	  <td class="" align="center"><input type="text" name="iRefPasswd" maxlength="32" size="24" class="text"></td>
	</tr>
	<tr>
	  <td class="" align="right">Admin:</td>
	  <td class="" align="center"><input type="checkbox" name="iRefAdmin"<? if ($refAdmin) echo " checked"; ?>></td>
	</tr>
	<tr>
	  <td colspan="2" align="center">
		<input type="submit" name="<? echo $refAction; ?>" value="<? echo $buttonText; ?>" class="button">
		<input type="submit" name="<? echo $refAction; ?>" value="Cancel" class="button">
	  </td>
	</tr>
  </table>
  </form>
  </td>
</tr>
</table>
<?
	showHint("User Management", "When editing leave the password blank, else the password gets changed to whatever you put in", 300);
?>
<br><br>
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td colspan="2" align="center">Login IP Block List</td>
</tr>
<tr>
  <td class="td_unsched" width="100" align="center">Blocked IPs</td>
  <td class="td_unsched" width="80" align="center">Actions</td>
</tr>
<?
$sqlresult = db_query ("select ID, IP, FailedAuths from pbs_blockedips");
if (!mysql_num_rows ($sqlresult))
{
?>
<tr>
  <td colspan="2" align="center">No Blocked IPs Found.</td>
</tr>
<?
}
while ($blockedip = mysql_fetch_assoc ($sqlresult))
{
?>
<tr>
  <td align="center"><? echo $blockedip["IP"]; ?></td><td align="center">[<a href="bracket_admin_post.php?delIpBan='<? echo $blockedip["IP"]; ?>'">Delete</a>]</td>
</tr>
<?
}
?>
</table>
<br><br>
<form action="bracket_admin_post.php" method="post">
<input type="hidden" name="addIpBan" value="Add IP">
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td colspan="2" class="td_unsched" align="center">Add IP Ban</td>
</tr>
<tr>
  <td><input class="text" type="text" name="iIpAddy" maxlength="15" size="19"></td>
  <td><input class="button" type="submit" value="Add IP"></td>
</tr>
</table>
</form>
<?
} else
if ($ViewMode == "About")
{
	echo "<br><br><br>";
	aboutBox();
} else
if( $ViewMode == "Rules" )
{
	$resid = mysql_query( "select Body, Author from pbs_textdata where TextType = 'Rules'" );
	$data = mysql_fetch_assoc( $resid );
	$rules = safeBr2NL( $data["Body"] );
?>
<form action="bracket_admin_post.php" method="post">
<input type="hidden" name="setRules" value="1">
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td colspan="2" class="td_unsched" align="center">Edit Tournament Rules</td>
</tr>
<tr>
  <td colspan="2" class="input_cell">Author &rarr; <input class="text" type="text" name="Author" value="<? echo $data["Author"]; ?>" size="40" maxlength="32"></td>
</tr>
<tr>
  <td>Rules&darr;<br><textarea name="Rules" rows="34" cols="90"><? echo $rules; ?></textarea></td>
  <td valign="top" align="center" class="rules_cell"><br>
    <input class="button" type="submit" value="Save Rules"><br>
    <hr>
    <u>Headers</u><br>
    <h1>&lt;H1&gt;</h1><br>
    <h2>&lt;H2&gt;</h2><br>
    <h3>&lt;H3&gt;</h3><br>
    <h4>&lt;H4&gt;</h4><br>
    <h5>&lt;H5&gt;</h5><br>
    <h6>&lt;H6&gt;</h6><br>
  </td>
</tr>
</table>
</form>
<?
}
else
if( $ViewMode == "News" )
{
	$NewsId = isset( $_GET["NewsId"] ) ? $_GET["NewsId"] : 0;
	if( $NewsId )
	{
		$resid = db_query( "select Body, Author, Title, LastModified from pbs_textdata where ID = $NewsId" );
		$data = mysql_fetch_assoc( $resid );
	} else
		$data = array( "Body" => "", "Author" => "", "Title" => "" );
	
	$rules = safeBr2NL( $data["Body"] );
	
?>
<form action="bracket_admin_post.php" method="post">
<input type="hidden" name="setNews" value="1">
<input type="hidden" name="NewsId" value="<? echo $NewsId; ?>">
<table border="0" cellpadding="1" cellspacing="1" align="center">
<tr>
  <td rowspan="5" width="200px" valign="top">
    <table class="news_list">
<?
	$resid = db_query( "select Title, Created, ID from pbs_textdata where TextType = 'News'" );
	while( $article = mysql_fetch_assoc( $resid ) )
	{
		$article["Title"] = ShortenString( $article["Title"], 28 );
		
		$selected = $article["ID"] == $NewsId ? " class=\"selected\"" : "";
		
		echo "<tr><td$selected><a href=\"?ViewMode=News&NewsId={$article["ID"]}\">{$article["Title"]}</a></td></tr>\n";
		echo "<tr><td$selected>Posted: <i>{$article["Created"]}</i></td></tr>\n";
	}
?>
    </table>
  </td>
</tr>
<tr>
  <td colspan="2" class="td_unsched" align="center"><? echo $NewsId ? "Edit" : "Post"; ?> Tournament News</td>
</tr>
<tr>
  <td colspan="2" class="input_cell">Title &rarr; <input class="text" type="text" name="Title" value="<? echo $data["Title"]; ?>" style="width: 213px" maxlength="32"></td>
</tr>
<tr>
  <td colspan="2" class="input_cell">Author &rarr; <input class="text" type="text" name="Author" value="<? echo $data["Author"]; ?>" style="width: 200px" maxlength="32"></td>
</tr>
<tr>
  <td>Rules&darr;<br><textarea name="News" rows="32" cols="90"><? echo $rules; ?></textarea></td>
  <td valign="top" align="center" class="rules_cell"><br>
    <input class="button" type="submit" value="Save News"><br>
    <input class="button" type="submit" name="newsAction" value="Close"><br>
    <hr>
    <input class="button" type="submit" name="newsAction" value="Delete"><br>
    <hr>
    <u>Headers</u><br>
    <h1>&lt;H1&gt;</h1><br>
    <h2>&lt;H2&gt;</h2><br>
    <h3>&lt;H3&gt;</h3><br>
    <h4>&lt;H4&gt;</h4><br>
    <h5>&lt;H5&gt;</h5><br>
    <h6>&lt;H6&gt;</h6><br>
  </td>
</tr>
</table>
</form>
<?
}
else
if ($ViewMode == "Export")
	include ("bracket_export.php");
?>
</body>
</html>
