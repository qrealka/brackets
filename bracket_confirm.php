<?
	require_once "bracket_ccode.php";
?>
<html>
<head>
<title>Confirmation</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link href="bracket_view.css" rel="stylesheet" type="text/css">
</head>
<br>
<body>
<?
	if ( ! isset ( $_SESSION["Confirm"] ) )
	{
		showHint ("Confirm Dialog Error", "The Confirm Dialog was called without any data!<br>"
		."If you got here by pressing back, its not a problem.<br>If you got here any other way, it was a bug!", 320, "center");
	} else
	{
		extract ($_SESSION["Confirm"], EXTR_PREFIX_ALL, "confirm");
?>
<table class="data_list" width="300" align="center">
<tr>
  <td class="header">
    Confirmation Request 
  </td>
</tr>
<tr>
  <td align="center" class="entry">
  <? echo $confirm_body; ?><br><br>
  <form action="<? echo $confirm_return; ?>" method="POST">
  <input type="hidden" name="<? echo $confirm_variable; ?>" value="<? echo $confirm_value; ?>">
  <input type="submit" class="button" name="<? echo $confirm_cfname; ?>" value="Yes">
  <input type="submit" class="button" name="<? echo $confirm_cfname; ?>" value="No">
  </form>
  </td>
</tr>
</table>
<?
	}
?>
</body>
</html>