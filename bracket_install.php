<?
	require_once "bracket_ccode.php";
	
	define	("MODE_CONFIGURE",			0);
	define	("MODE_ERRORBOX",			1);
	define	("MODE_CREATETABLES",		2);
	define	("MODE_DOPOST",				3);
	define	("MODE_EXIT",				4);
	
	$Mode = MODE_CONFIGURE;

	$adminPage = "bracket_admin.php";
	
	$cfgTable = "pbs_config";	// table into which configuration will be inserted into
	$defTable = "pbs_referees";	// where default accounts will go into

	require_once "bracket_install.inc";
	
	extract ($_GET);
	extract ($_POST, EXTR_OVERWRITE);
	
	/*
	// to keep require_once from sexoring itself and prevent multiple reconnections
	if ($Mode != MODE_DOPOST && file_exists ("bracket_connect.php"))
	{	// bracket config is already made
		require_once "bracket_connect.php";
		// see if we have any admins
		$res = @mysql_query ("select count(ID) as count from pbs_referees where bIsAdmin = 'Y'");
		if ($res)
		{	// if the tables and databases required to perform this query exist..
			$count = mysql_result ($res, 0);
			if ($count)
			{	// an admin already exists, lock install functions
				echo "Bracket is already installed.<br>Access to this file is denied.<br>";
				exit;
			}
		}
	}
	*/
	
	// i dont wanna support uninstalling, deleting tables can only cause problems
	if (isset ($special) && $special == "uninstall")
	{
		require_once ("bracket_connect.php");
		foreach ($CTables as $Table)
		{
			mysql_query ("drop table ".$Table["Name"]);
		}
		header ("Location: bracket_install.php?special=removed");
	}
?>
<html>
<head>
<title>PBS Installation</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="bracket_view.css" type="text/css">
</head>
<body bgcolor="#354662" text="#FFFFFF">
<br><br>
<?

	if (isset ($special) && $special == "removed")
	{
		showHint ("Bracket Uninstalled", "<br>Bracket tables were removed from database,<br>demos and screenshots have been deleted.<br>Thank you, come again.<br><br>", 300, "center");
		$Mode = MODE_EXIT;
	}

	if ($Mode == MODE_DOPOST)
	{
		//iDownloadLogiDownloadPathiMapsPerGamebUseCHTViDEPlayersiSEPlayers
		//iBRPasswordiBRUsernameiSQLHostnameiSQLDatabaseiSQLPasswordiSQLUsername
		//echo "<pre>";
		//print_r ($_POST);
		//echo "</pre>";
		$iMapsPerGame = intval($iMapsPerGame);
		$iDEPlayers	= intval($iDEPlayers);
		$iSEPlayers	= intval($iSEPlayers);
		
		//var_dump($iMapsPerGame);
		//var_dump($iDEPlayers);
		//var_dump($iSEPlayers);
		
		$depower = ($iDEPlayers) ? log($iDEPlayers)/log(2) : 0;
		$sepower = ($iSEPlayers) ? log($iSEPlayers)/log(2) : 0;
		
		if ($iMapsPerGame < 1 || $iMapsPerGame > 10) {
			showHint ("Invalid Input", "Maps Per Game is not a valid number (1-10)");
			$Mode = MODE_EXIT;
		} else
		if( $depower != (int) $depower || $iDEPlayers == 1 || $sepower != (int) $sepower ) {
			showHint( "Invalid Input", "Invalid size, must be 2^n (8, 16, 32, 64..)" );
			$Mode = MODE_EXIT;
		} else
		if ($iSEPlayers < 2 || $iSEPlayers > 128) {
			showHint ("Invalid Input", "Invalid number of single elimination players (2-128)");
			$Mode = MODE_EXIT;
		} else
		if ($iDEPlayers < 0 || $iDEPlayers > $iSEPlayers / 2) {
			showHint ("Invalid Input", "Invalid number of double elimination players (0-".($iSEPlayers/2).")");
			$Mode = MODE_EXIT;
		}
		if ($Mode != MODE_EXIT)
		{	// if all input is good
			// check mysql access
			$connect = @mysql_connect ("$iSQLHostname", "$iSQLUsername", "$iSQLPassword");
			if (!$connect)
			{
				showHint("Error Connecting to SQL", mysql_error()."<br>Press Back and check inputed sql information.");
				$Mode = MODE_EXIT;
			} else
			if (!@mysql_select_db("$iSQLDatabase"))
			{
				if ($bCreateDB == "Yes")
				{	// user wants the database created
					if (!mysql_query ("create database $iSQLDatabase"))
					{	// couldnt create database, lack of permissions?
						showHint ("Error Creating Database", mysql_error() . "<br>Could not create database, perhaps the account lacks permissions to do create databases.<br>Install aborted cleanly.");
						$Mode = MODE_EXIT;
					}
				} else
				{	// database doesnt exist and shouldnt be created so poop
					showHint("Error Connecting to SQL","<br>". mysql_error()."<br>Press Back and check the sql info.<br><br>", 230, "center");
					$Mode = MODE_EXIT;
				}
			}
		}
		
		if ($Mode != MODE_EXIT)
		{
            // save $gameMode $tourneyName
            $br_config = <<<XML
<?xml version='1.0' encoding="ISO-8859-1"?>
<config>
    <tourney>$iTourneyName</tourney>
    <game>$iGameMode</game>
</config>
XML;
            $fconfig = @fopen ("bracket_cfg.xml", "wb");	// prevents \r\n translations and shit
			if ($fconfig)
			{
				fwrite ($fconfig, $br_config, strlen ($br_config));	// specifying size bypasses magic quotes
				fclose ($fconfig);
				chmod ("bracket_cfg.xml", 0666);
			} else
			{
				showHint("Error Creating Config", "Could not write config to bracket_cfg.php<br>Check file permissions. Install aborted cleanly.", 280);
				$Mode = MODE_EXIT;
			}

      // try to continue and create bracket_connect.php
			$br_config = <<<EOT
<?
\  require_once "bracket_cfg.inc";		// tourney & mode name
  
\$bUseCHTV = $bUseCHTV;			// [boolean] changing this changes download link (old demos wont show up)
\$destinationPath = "$iDownloadPath";		// also makes old demo links wrong
\$demoLogName = "$iDownloadLog";		// its just a log heh
\$bShowMeter = $bShowMeter;			// show close-game meter
\$gameMode = "1v1";
\$tourneyName = "CPMA";

if ( !@mysql_pconnect("$iSQLHostname", "$iSQLUsername", "$iSQLPassword") )
{
	echo "bracket_connect.php: Error Connecting to MySQL Server<br>\\n";
	exit;
}
if ( !@mysql_select_db("$iSQLDatabase") )
{
	echo "bracket_connect.php: Error Selecting Database<br>\\n";
	exit;
}
?>
EOT;
			$fconfig = @fopen ("bracket_connect.php", "wb");	// prevents \r\n translations and shit
			if ($fconfig)
			{
				//$br_config = safeString ($br_config);
				fwrite ($fconfig, $br_config, strlen ($br_config));	// specifying size bypasses magic quotes
				fclose ($fconfig);
				chmod ("bracket_connect.php", 0666);
			} else
			{
				showHint("Error Creating Config", "Could not write config to bracket_config.php<br>Check file permissions. Install aborted cleanly.", 280);
				$Mode = MODE_EXIT;
			}
		}
		if ($Mode != MODE_EXIT)
			$Mode = MODE_CREATETABLES;
	}
	
	if ($Mode != MODE_EXIT)
	{
?>
<br>
<table border="0" cellpadding="0" cellspacing="1" width="700" align="center">
<tr>
<td>
  <table border="0" cellpadding="1" cellspacing="1" width="100%" class="td_unsched" bgcolor="#9FAFCA">
  <tr>
	<td align="center">Putty Bracket System Installation</td>
  </tr>
  </table>
</td>
</tr>
<tr>
<td>
  <table border="0" cellpadding="1" cellspacing="1" width="100%">
  <tr>
	<td align="center">
	<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="td_unsched">
	<tr>
	  <td>
	  <br>
<?
		if ($Mode == MODE_CREATETABLES)
		{
			require_once "bracket_connect.php";
			
			$problems = 0;
			
			foreach ($CTables as $Table)
			{
				$table_name = $Table["Name"];
				$check_exists = mysql_num_rows (mysql_query ("show tables like '$table_name'"));
				if ($check_exists) {
					$entries = mysql_result (mysql_query ("select count(*) from $table_name"), 0);
					
					echo "Table <b>$table_name</b> already exists in the database.<br>\n";
					echo "-> it contains $entries entries.<br><br>\n";
					//$entries = mysql_result (mysql_query ("drop table $table_name"), 0);
					//echo "drop table result is $entries.<br><br>\n";
					$problems ++;
				}
			}
			
			if ($problems) {
				echo "There are $problems tables that are already using our table names.<br>\n"
					."Please drop/rename the tables in the database and try again.<br>\n";
				echo "For safety reasons those tables will not be dropped through this install.<br><br>\n";
			} else
			{
				$inst_errors = 0;
				foreach ($CTables as $Table)
				{
					$table_name = $Table["Name"];
					mysql_query ($Table["Code"]);
					if (mysql_error()) {
						echo "> Error creating <b>$table_name</b>:<br>\n> " . mysql_error() . "<br>\n";
						$inst_errors ++;
					}
					else echo "Table <b>$table_name</b> created.<br>\n";
				}
								
				if ($inst_errors) {	// ERROR CODE 501
					echo "There were $inst_errors errors while creating tables.<br>\n";
					foreach ($CTables as $Table) {
						$table_name = $Table["Name"];
						mysql_query ("drop table $table_name");
					}
					echo "Table creation undone, however installation has failed.<br>\n";
					echo "Contact putty with error code 501, sorry about the inconvenience.<br>\n";
				} else
				{
					// admin account creation
					$check = mysql_query ("insert into pbs_referees (RefName, Login, Passwd, bIsAdmin) values ('The Admin', '$iBRUsername', md5('$iBRPassword'), 'Y')");
					if (!$check) {
						showHint ("Error Configuring Bracket", mysql_error() . "<br>Could not create admin account for bracket.<br>Install failed dirty.");
						exit;
					}
					// maps per game
					mysql_query ("insert into $cfgTable (Label, Value) values ('MPG', $iMapsPerGame)");
					if (!$check) {
						showHint ("Error Configuring Bracket", mysql_error() . "<br>Install failed dirty.");
						exit;
					}
					// player/team info
					mysql_query ("insert into $cfgTable (Label, Value) values ('SEPlayers', $iSEPlayers)");
					mysql_query ("insert into $cfgTable (Label, Value) values ('DEPlayers', $iDEPlayers)");
					// signup info
					mysql_query ("insert into $cfgTable (Label, Value) values ('MaxSignups', $iSEPlayers)");

					$nSEPlayers = $iSEPlayers;
					$nDEPlayers = $iDEPlayers;
					
					$numWB = $nSEPlayers * 2;
					$numLB = getLBSize($nDEPlayers);
					if ($numLB) $numLB += 1;			// 1 more for final round
					$numRows = $numWB + $numLB;
					
					// proper filler :)
					for ($i = 1; $i <= $numRows; $i ++)
						mysql_query ("insert into pbs_bracket (ID, PlayerID, Timestamp) values ($i, 0, 0)");
					// player list
					for ($i = 1; $i <= $nSEPlayers; $i ++)
						mysql_query ("insert into pbs_players (ID, Nick) values ($i, '-TBA-')");
					
					require( "bracket_link.php" );
					
					$aSeeds = buildSeedList ($nSEPlayers);
					for ($i = 0; $i < $nSEPlayers; $i ++)
					{	// insert first round player data into bracket (incase admin doesnt wanna use seeder)
						$nID = $i * 2 + 1;
						$nSeededID = $aSeeds[$i];
						mysql_query ("update pbs_bracket set PlayerID = $nSeededID, Timestamp = Timestamp where ID = $nID");
					}
					
					echo "<br>Tables created successfully.<br>"
						."Your account has been activated.<br>"
						."Ready to use the <a href=\"$adminPage\">admin section</a>.<br><br>\n";
				}
			}
		} else if ($Mode == MODE_CONFIGURE) {
?>
		<div align="center">
		All of the settings you set here are changable within the bracket.<br><br>
		</div>
		<form name="instpoll" method="post" action="">
		<input type="hidden" name="Mode" value="<? echo MODE_DOPOST; ?>">
		<table border="0" cellpadding="1" cellspacing="0" align="center" width="95%">
		<tr>
		  <td colspan="2">&nbsp;</td>
		  <td align="center">SQL Login Info</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">SQL User Name:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iSQLUsername" maxlength="32" class="text"></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">SQL Password:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iSQLPassword" maxlength="32" class="text"></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">SQL Database:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iSQLDatabase" maxlength="32" class="text"></td>
		  <td>name of the database the bracket will store data in<br></td>
		</tr>
		<tr>
		  <td align="right" width="150px">SQL Create DB:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="radio" name="bCreateDB" value="Yes">Yes<input type="radio" name="bCreateDB" value="No" checked>No</td>
		  <td>this will create the database if it does not already exist.<br></td>
		</tr>
		<tr>
		  <td align="right" width="150px">SQL Hostname:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iSQLHostname" maxlength="32" class="text" value="localhost"></td>
		  <td>most commonly the sql server is on same box as http server.. bust just incase</td>
		</tr>
		<tr>
		  <td colspan="2">&nbsp;</td>
		  <td align="center">Bracket Admin Info</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Admin User Name:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iBRUsername" maxlength="32" class="text"></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Admin Password:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iBRPassword" maxlength="32" class="text"></td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td colspan="2">&nbsp;</td>
		  <td align="center">Bracket Setup</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Single Elim:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iSEPlayers" maxlength="3" class="text"></td>
		  <td>number of players/teams in the tourney (must be 2^n)</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Double Elim:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iDEPlayers" maxlength="3" class="text"></td>
		  <td>number of players/teams you want to have in loser bracket<br>(0 = disable double elim)</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Maps Per Game:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iMapsPerGame" maxlength="1" class="text"></td>
		  <td>number of maps a match consists of</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Game Mode:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center">
		    <select name="iGameMode" size="1">
		      <option value="1v1" selected>1v1</option>
		      <option value="tdm">tdm</option>
		      <option value="ctf">ctf</option>
		      <option value="ntf">ntf</option>
		      <option value="ctfs">ctfs</option>
		      <option value="2v2">2v2</option>
		      <option value="HM">HM</option>
		      <option value="CA">CA</option>
		      <option value="FFA">FFA</option>
		      <option value="FTAG">FTAG</option>
		    </select>
		  </td>
		  <td>game mode for the tourney</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Use CHTV:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="radio" name="bUseCHTV" value="true">Yes<input type="radio" name="bUseCHTV" value="false" checked>No</td>
		  <td>weather you want to upload demos to <a href="http://www.challenge-tv.com" title="a huge online demo database">CHTV</a> or store them locally</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Show Meter:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="radio" name="bShowMeter" value="true">Yes<input type="radio" name="bShowMeter" value="false" checked>No</td>
		  <td>a close - rape game meter (just for shits and giggles)</td>
		</tr>
		<tr>
		  <td colspan="2">&nbsp;</td>
		  <td align="center">If Answered No Above..</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Demo Storage Path:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iDownloadPath" maxlength="128" class="text" value="demos/"></td>
		  <td>default demo storage path is ..bracket_path/demos/</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Demo Log Name:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iDownloadLog" maxlength="128" class="text" value="demoupload.log"></td>
		  <td>all demo uploads are logged to this file</td>
		</tr>
		<tr>
		  <td colspan="2">&nbsp;</td>
		  <td align="center">Most Importantly..</td>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="right" width="150px">Tourney Name:</td>
		  <td width="10">&nbsp;</td>
		  <td align="center"><input type="text" name="iTourneyName" maxlength="64" class="text" value="Challenges 4!"></td>
		  <td>no fancy characters please</td>
		</tr>
		<tr>
		  <td colspan="3">&nbsp;</td>
		</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
		<td width="150" align="center">
		  <input type="submit" value="Ok" class="button">
		</td>
		<td>&nbsp;</td>
		</tr>
		</table>
		<br>
		</form>		
<?		} ?>
	  </td>
	</tr>
	</table>
	</td>
  </tr>
  </table>
</td>
</tr>
</table>
<?
	}
?>
</body>
</html>
