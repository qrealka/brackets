<?
	// 100% horse glue

	require_once "bracket_connect.php";			// mysql connection
	require_once "bracket_ccode.php";	// shared bracket code
	
	$nSEPlayers = mysql_result (db_query ("select Value from pbs_config where Label = 'SEPlayers'"), 0);
	$nDEPlayers	= mysql_result (db_query ("select Value from pbs_config where Label = 'DEPlayers'"), 0);
	$nMPG				= mysql_result (db_query ("select Value from pbs_config where Label = 'MPG'"), 0);
	$nDERows	= log10 ($nDEPlayers) / log10 (2) * 4 + 1;
	$nSERows	= log10 ($nSEPlayers) / log10 (2) * 2 + 1;
	
	list ($DEarrGraph, $arrBackGraph, $arrDropIns) = buildDoubleElim2 ($nDEPlayers);
	$arrConverter = buildList ($nSEPlayers, $nDEPlayers * 2 + 1, $nDERows, $DEarrGraph);

	$nColumns = $nSEPlayers * 2 - 1;
	$nRows = $nSERows;

	$arrGraph = array();

	for ($i = 0; $i < $nColumns; $i ++)
	{
		$sub = 0;
		for ($j = 0; $j < $nRows; $j ++)
		{
			$p = pow (2, $j+1);
			
			$arrGraph[$i * $nRows + $j] = 0;
			
			if ($j % 2 == 0 && ($i-$sub) % pow(2,$j/2+1) == 0) {
				$arrGraph[$i * $nRows + $j] = 1;
			}
			if ($j % 2 == 0)
				$sub += pow (2, $j/2);			
		}
	}
	
	//echo "<pre>";
	//print_r ($arrDropIns);
	//echo "</pre>";
	
	$nLoserID = 0;
	$nPlayerID = 0;
	$WBStandingString = "";
	$arrLoserToWinner = array();
	
	$firstLoserID = 0;
	$lastLoserID = 0;
	
	
	$numRounds = ($nDERows - 1) / 2;
	//echo "numRounds = $numRounds<br>";
	$numBackPowers = $numRounds / 2 + 1;
	//echo "numBackPowers = $numBackPowers<br>";
	
	for ($i = 0; $i < $nColumns; $i ++)
	{

		for ($j = 0; $j < $nRows; $j ++)
		{
			if ($arrGraph[$i * $nRows + $j] == 1)
			{
				$nPlayerID ++;
				$nLoserID = 0;
				$nRoundsPlayed = $j/2;
				$nBackPower = ($nRows + 1)/2 - $nRoundsPlayed;
				$nPlayersLevel = pow (2, $nBackPower);
				$nLoserID = 0;
				if ($nPlayersLevel <= $nDEPlayers * 2)
				{	// fall into first normal loser bracket round
					$nRowID = ($nPlayerID / pow (2, $j/2) + 1) / 2 - 1;
					$nFlipSize = $nPlayersLevel / 2 - 1;
					//echo "nRowID = $nRowID<br>";
					$flipedRowID = ($nBackPower % 2 == 0) ? ($nFlipSize - $nRowID) : $nRowID;
					$nLoserID = $arrConverter[$arrDropIns[$numBackPowers - $nBackPower][$flipedRowID]];
					$arrLoserToWinner[$nLoserID] = $nPlayerID;
					//echo "----------------------> $nPlayerID/$nLoserID | check = " .( $nPlayersLevel/2 ). "<br>";
					//echo "index = ".($nRowID)." | new index = ".($nFlipSize - $nRowID)."<br>";
					//echo "nBackPower = $nBackPower | nLoserID = $nLoserID<br>";
					//if ($nBackPower % 2 == 1) echo "-> flip flop<br>";
				}
				
				if ($j)
				{
					$bHasOption = true;
					if ($nLoserID)
						$WBStandingString .= "$nPlayerID/$nLoserID<br>";
					
					if ($nLoserID)
					{
						if ($nLoserID < $lastLoserID) $firstLoserID = $nLoserID;
						if ($nLoserID > $lastLoserID) $lastLoserID = $nLoserID;
					}
				}
			}
		}
	}
	
	//echo "<pre>";
	//print_r ($arrConverter);

	
	$WBStandingString = rtrim ($WBStandingString, "<br>");
	$dropins = split ("<br>", $WBStandingString);
	if (count ($dropins) > 1)
	foreach ($dropins as $dropin)
	{
		list ($BracketID, $DropIn) = split ("/", $dropin);
		//echo "BracketID: $BracketID DropIn: $DropIn<br>\n";
		db_query ("update pbs_bracket set DropInID = $DropIn, Timestamp = Timestamp where ID = $BracketID");
		if (mysql_error()) echo mysql_error();
	}
	
	//echo "winner bracket ids: 1 - $nSEPlayers<br>\n";
	//echo "loser bracket ids: $firstLoserID - $lastLoserID<br>\n";
	
	//timeCode(3);
	//echo "Winner Bracket Spot/Loser Bracket Spot LINKS:<br>$WBStandingString<br>\n";
	//timeCode (1);
?>
