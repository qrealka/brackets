Putty's Bracket System (PBS) version 0.9.4-chily

*This Documentation Has Been Outdated For Ever*

Installation
------------
1) run bracket_install.php on the server
2) have fun
3) repeat 2


Details
-------
bracket_login.php is a universal login for both referees and admins
bracket_admin.php is what admins use
bracket_referee.php is what referees use


! [NO] default admin account is made during install, you pick your own login/pw :D
! login allows for up to 5 mistakes, then your ip gets banned :)

admins can unban ips and add ip bans for login page
admins can schedule and assign referees to matches
admins can also set scores and upload demos/screenshots
admins can delete matches (incase wrong result was put in)
admins can change player's names at any time of the tournament
admins can reseed bracket, clear and resize bracket,
           resize losers bracket without losing information about winners bracket
admins can add referees and other admins, admin status can be assigned/removed at any time
admins can add maps with screenshots and a description
admins can add rules and news

referees can schedule, set scores, and upload demos and screenshots
referees can change match results, scores, demos, screenshots

viewers can see upcoming scheduled matches and who the referee for it is
viewers can see past history of all matches
viewers can see each player's match history
viewers can see map statistics (number of times map played, closest fights on the map, etc)

Functions
---------
admin mode:
	standing - shows a bracket view in which you can edit player names/match results
	seeder	 - plug in a seed list and it builds a bracket, edit seed placement
	config	 - resize/rebuild the bracket
	mappool	 - add/edit/delete maps
	referees - add/edit/delete referees/admins
	export	 - exports sql data for misc purposes
	logout	 - urf
referee mode:
	...
viewer mode:
	...

Contact Info
------------
irc: enterthegame.com, #challenge, putty
aim: ciphernx
email: aleckz@gmx.net